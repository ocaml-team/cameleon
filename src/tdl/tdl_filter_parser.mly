/*********************************************************************************/
/*                Cameleon                                                       */
/*                                                                               */
/*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     */
/*    et en Automatique. All rights reserved.                                    */
/*                                                                               */
/*    This program is free software; you can redistribute it and/or modify       */
/*    it under the terms of the GNU Library General Public License as            */
/*    published by the Free Software Foundation; either version 2 of the         */
/*    License, or any later version.                                             */
/*                                                                               */
/*    This program is distributed in the hope that it will be useful,            */
/*    but WITHOUT ANY WARRANTY; without even the implied warranty of             */
/*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              */
/*    GNU Library General Public License for more details.                       */
/*                                                                               */
/*    You should have received a copy of the GNU Library General Public          */
/*    License along with this program; if not, write to the Free Software        */
/*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   */
/*    02111-1307  USA                                                            */
/*                                                                               */
/*    Contact: Maxence.Guesdon@inria.fr                                          */
/*                                                                               */
/*********************************************************************************/

%{
open Tdl_filter
%}
%token GROUP ITEM EMPTY STATE DESC BEFORE
%token EOF
%token <string> String
%token <Tdl_date.t> Date
%token <Tdl_types.state> State
%type <Tdl_filter.filter> filter
%token LPAR RPAR
%token OR AND
%token NOT
%left OR
%left AND
%nonassoc NOT
%start filter
%%
filter:
| GROUP String {
  Tdl_filter.Group $2
  }
| ITEM String {
  Tdl_filter.Item $2
  }
| EMPTY {
  Tdl_filter.Empty
  }
| STATE State {
  Tdl_filter.State $2
  }
| DESC String {
  Tdl_filter.Desc $2
  }
| BEFORE Date {
  Tdl_filter.Before $2
  }
| NOT filter {
  Tdl_filter.Not $2
  }
| filter AND filter {
  Tdl_filter.And ($1, $3)
  }
| LPAR filter RPAR {
  $2
  }
| filter OR filter {
  Tdl_filter.Or ($1, $3)
  }
;

%%