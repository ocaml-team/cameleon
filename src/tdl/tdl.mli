(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(* $Id: tdl.mli,v 1.6 2004/04/13 07:23:53 zoggy Exp $ *)

(** The OCaml-TDL library. *)

(** {2 Types} *)

type date = {
  year : int;		(** complete year *)
  month : int;		(** 1..12 *)
  day : int;		(** 1..31 *)
  hour : int;
  minute : int;
  second : int;
  zone : int;		(** in minutes; 60 = UTC+0100 *)
  week_day : int	(** 0 = sunday; -1 if not given *)
}

val since_epoch : date -> float
val float_to_date : float -> date


type state =
    Done
  | Suspended
  | Priority_low
  | Priority_normal
  | Priority_high


(** All available item states. *)
val states : state list

val string_of_state : state -> string

type item = {
    mutable item_title : string;
    mutable item_date : date;
    mutable item_enddate : date option;
      (** milestone date of date when the item state changed to [Done]. *)
    mutable item_desc : string option;
    mutable item_state : state;
    mutable item_id : int option;
  }

type group = {
    mutable group_title : string;
    mutable group_items : item list;
    mutable group_groups : group list;
    mutable group_id : int option;
  }

(** {2 Building and manipulating items and groups} *)

val item :
  ?id: int ->
  title:string ->
  state:state ->
  ?date:date ->
  ?enddate:date -> ?desc:string -> unit -> item;;

val group :
  ?id: int ->
  ?title:string ->
  ?items:item list -> ?groups:group list -> unit -> group;;

(** Return a copy of the given item. *)
val copy_item : item -> item

(** Return a copy of the given group, where items and subgroups
   are also copies. *)
val copy_group : group -> group

(** Remove the given item from the given group. *)
val remove_item : group -> item -> unit

(** Remove the given group [son] from the given group [father] . *)
val remove_group : father: group -> son: group -> unit;;

(** {2 Reading groups} *)

val group_of_file : string -> group
val group_of_string : string -> group
val group_of_channel : in_channel -> group;;

(** {2 Writing groups} *)

val print_group : ?encoding: string -> Format.formatter -> group -> unit

(** @param encoding is the name of the encoding of the values of the fields
   of items and groups. By default it is ["ISO-8859-1"] but you should
   give your own if you use another encoding to store the title, description, ...
   of items and groups. This is used when generating the ["<?xml ...>"]
   first line of the document. *)
val print_file : ?encoding: string -> string -> group -> unit

(** {2 Merging} *)

(** [insert_group g1 g2] inserts group g2 into g1, that is g1 becomes
     a subgroup of g1. It is a new subgroup if no group of the same title exists
     in g1, orelse all subgroups of g2 are recursively inserted into the subgroup
     of g1 matching the title of g2.
     @param path can be used to insert g2 in a subgroup of g1, creating
     subgroups if needed.
     *)
val insert_group : ?path: string list -> group -> group -> unit

val merge_top_groups : group -> group -> group;;

(** {2 Filtering} *)

type filter =
  Group of string
  | Item of string
  | Empty
  | State of Tdl_types.state
  | Desc of string
  | Before of date
  | Or of filter * filter
  | And of filter * filter
  | Not of filter
;;

val filter_of_string : string -> filter
val filter_of_channel : in_channel -> filter

val filter_group : filter -> group -> group

(** {2 Splitting Todo list by day} *)

(** [split_by_day f tdl] splits the given todo list, calling [f] on each
     different day and the corresponding todo list. A day is given by
     a tuple (year, month (1..12), day (1..31)). *)
val split_by_day : ((int * int * int) * group -> unit) -> group -> unit

(** {2 Sorting} *)

(** Same as {!Pervasives.compare} but for item state (and enddate and date).
  When the states are the same, the item with the smallest date if first.
  When the states are both [Done], the item with the smallest enddate is last.
  When the states are different, the order is the following:
  [Priority_high < Priority_normal < Priority_low < Suspended < Done].
*)
val compare_item_state : item -> item -> int

(** Sort a list of items using the {!compare_item_state} function. *)
val sort_items_by_state : item list -> item list