(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

{

(** The lexer for filters. *)

open Lexing
open Tdl_filter_parser

let print_DEBUG s = () (*prerr_endline s*)

let string_buf = Buffer.create 256

let line = ref 0
}

let newline = ('\010' | '\013' | "\013\010")
let blank = [' ' '\009' '\012']
let any = [^'\n']*
let simple_string = [^'\n' '"' '(' ')' ' ' '\009' '\012' ':' '/' '<' '>']+
let backslash_escapes = ['\\' '"' '\'' 'n' 't' 'b' 'r']
let date = ['0'-'9']+'-'['0'-'9']+'-'['0'-'9']+
let datetime = date blank['0'-'9']+':'['0'-'9']+':'['0'-'9']+

rule main = parse
| "\"" { print_DEBUG "\"";
	 Buffer.reset string_buf;
	 string lexbuf }
| "group:" { print_DEBUG "group:"; GROUP }
| "item:" { print_DEBUG "item:"; GROUP }
| "empty" { print_DEBUG "empty"; EMPTY }
| "state:" { print_DEBUG "state:"; STATE }
| "descr:" { print_DEBUG "descr:"; DESC }
| "before" { print_DEBUG "before"; BEFORE }
| "(" { print_DEBUG "("; LPAR }
| ")" { print_DEBUG ")"; RPAR }
| datetime {
      let s = Lexing.lexeme lexbuf in
      print_DEBUG ("datetime: "^s);
      let d =
        try Tdl_date.parse s
        with Invalid_argument _ ->
            failwith (Printf.sprintf "Invalid date: %s" s)
      in
      Date d
    }
| date {
      let s = Lexing.lexeme lexbuf in
      print_DEBUG ("date: "^s);
      let d =
        try Tdl_date.parse s
        with Invalid_argument _ ->
            failwith (Printf.sprintf "Invalid date: %s" s)
      in
      Date d
    }
| simple_string { let s = Lexing.lexeme lexbuf in
      print_DEBUG s;
      match String.lowercase s with
        "and" -> AND
      | "not" -> NOT
      | "or" -> OR
      | "done" -> State Tdl_types.Done
      | "suspended" -> State Tdl_types.Suspended
      | "low" -> State Tdl_types.Priority_low
      | "normal" -> State Tdl_types.Priority_normal
      | "high" -> State Tdl_types.Priority_high
      | _ -> print_DEBUG s; String s }
| blank { main lexbuf }
| newline { incr line ; main lexbuf}
| eof { EOF }
| _
    {print_DEBUG (Lexing.lexeme lexbuf);
      main lexbuf
    }

and string = parse
| "\\\"" { Buffer.add_string string_buf "\"";
           string lexbuf
         }
| "\"" { String (Buffer.contents string_buf) }
| newline { incr line;
	    Buffer.add_string string_buf (Lexing.lexeme lexbuf);
	    string lexbuf
          }
| _  { Buffer.add_string string_buf (Lexing.lexeme lexbuf);
       string lexbuf }
