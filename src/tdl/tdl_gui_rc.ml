(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

module CF = Config_file

let rc_file = Filename.concat Cam_installation.rc_dir "ocamltdl"

let group = new CF.group

let default_encoding = "ISO8859-1"
let encoding =
  new CF.string_cp ~group
    ["default_encoding"] default_encoding
    "Default charset used for encoding/decoding utf8"

let create_pix name def help =
  new CF.filename_cp ~group ["pixmaps";name] def
    ("Pixmap file used to represent "^help)

let pix_group = create_pix "group"
    (Filename.concat Cam_installation.pixmaps_dir "tdl-group.png")
    "groups"
let pix_item = create_pix "normal-item"
    (Filename.concat Cam_installation.pixmaps_dir "tdl-item.png")
    "normal priority items"
let pix_low = create_pix "low-item"
    (Filename.concat Cam_installation.pixmaps_dir "tdl-item-low.png")
    "low priority items"
let pix_high = create_pix "high-item"
    (Filename.concat Cam_installation.pixmaps_dir "tdl-item-high.png")
    "high priority items"
let pix_done = create_pix "done-item"
    (Filename.concat Cam_installation.pixmaps_dir "tdl-item-done.png")
    "done items"
let pix_susp = create_pix "suspended-item"
    (Filename.concat Cam_installation.pixmaps_dir "tdl-item-suspended.png")
    "suspended items"


let read () = group#read rc_file
let write () = group#write rc_file
