(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(* $Id: test.ml,v 1.2 2004/03/27 13:24:09 zoggy Exp $ *)

open Tdl

let tdl_items = 
  [
    Tdl.item ~title: "package TDL" ~state: Priority_normal ();
    Tdl.item ~title: "distribute TDL" ~state: Priority_normal ();
  ] 

let misc_items =
  [
    Tdl.item ~title: "Do the dishes" ~state: Priority_normal ();
    Tdl.item ~title: "Do the washing" ~state: Priority_high 
      ~desc: "Faut que j'fasse la lessive, bon sang!" ();
  ] 

let group = 
  Tdl.group ~title: "My TODO list"
    ~groups: 
    [ Tdl.group ~title: "Development" ~items: tdl_items () ;
      Tdl.group ~title: "Misc" ~items: misc_items () ;
    ] 
    ()


let _ = Tdl.print_file "foo.xml" group


let main () =
  if Array.length Sys.argv < 2 then
    (
     Printf.eprintf "usage: %s <file to parse>\n" Sys.argv.(0);
     exit 1
    );
  try
    let ch = Tdl.group_of_file Sys.argv.(1) in
    Tdl.print_group Format.std_formatter ch;
    print_newline ()
  with
    Xml.Error e ->
      prerr_endline (Xml.error e);
      exit 1
  | Dtd.Parse_error e ->
      prerr_endline (Dtd.parse_error e);
      exit 2

let _ = main ()
