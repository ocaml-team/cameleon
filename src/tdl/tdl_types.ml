(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(* $Id: tdl_types.ml,v 1.2 2004/03/20 23:05:57 zoggy Exp $ *)

type state =
    Done
  | Suspended
  | Priority_low
  | Priority_normal
  | Priority_high

type item = {
   mutable item_title : string;
   mutable item_date : Tdl_date.t ;
   mutable item_enddate : Tdl_date.t option;
      (** milestone date of date when the item state changed to [Done]. *)
   mutable item_desc : string option ;
   mutable item_state : state ;
   mutable item_id : int option;
  }

let item () =
  {
    item_title = "";
    item_date = Tdl_date.parse (Tdl_date.mk_mail_date (Unix.time ()));
    item_enddate = None ;
    item_desc = None ;
    item_state = Priority_normal ;
    item_id = None ;
  }


type group = {
   mutable group_title : string;
   mutable group_items : item list;
   mutable group_groups : group list;
   mutable group_id : int option;
  }

let group () = 
  {
    group_title = "";
    group_items = [];
    group_groups = [] ;
    group_id = None ;
  } 
