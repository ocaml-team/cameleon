(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

GMain.Main.init ();;
let w = GWindow.window ~title:"test" ~width: 600 ~height: 400 ();;
let nb_waits = ref 0;;

let wait () =
  incr nb_waits;
  prerr_endline (Printf.sprintf "nb_waits is now %d" !nb_waits);
  GMain.Main.main ();
  if !nb_waits > 0 then
    begin
      decr nb_waits;
      GMain.Main.quit ()
    end 

let cb ev =
  let key = GdkEvent.Key.keyval ev in
  if key = GdkKeysyms._Return then
    begin
      wait ();
      prerr_endline "pop!"
    end
  else
    (
     if key = GdkKeysyms._Escape then
       (
        decr nb_waits;
        GMain.Main.quit ();
       )
    );
  true
;;
ignore (w#event#connect#key_press ~callback: cb);;
w#show ();;
GMain.Main.main ()