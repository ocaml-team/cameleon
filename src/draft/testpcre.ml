(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open Pcre;;

let rex = regexp  "(?<=ou)";;
let string = "blablacoucoublacoublacouboul";;


let array_substr = exec_all ~rex ~pos: 0 string;;

let print_s s =
  let (b,e) = get_substring_ofs s 0 in
  let s = (get_substrings s).(0) in
  let s = Printf.sprintf "%s (%d-%d)" s b e in
  prerr_endline s;;
prerr_endline (Printf.sprintf "results: %d" (Array.length array_substr));;
let a_l = Array.map get_substrings array_substr;;
Array.iter (Array.iter prerr_endline) a_l;;
Array.iter print_s array_substr;;

(** query replace test *)

let rex_tmpl = [ "ab", "cd"; "a(b+)", "c$1"];;
let rex_tmpl = List.map (fun (re,t) -> (regexp re, t));;

let callout substrings match_start current_position capture_top capture_last callout_number =
  (* utiliser substitute_substrings et (replace dans le corps de la fonction en param�tre) *)