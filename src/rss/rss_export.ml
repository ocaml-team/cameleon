(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open Format;;
open Tdl;;

(** Command line tool to manipulate rss channels. *)

open Format;;
open Tdl;;


(** {2 Options and main} *)

type output_type = Rss;;
let output_type = ref Rss;;

let filters = ref [];;

let add_filter f = filters := !filters @ [f];;

let remaining = ref [];;

let options = [
    "-", Arg.Unit (fun () -> remaining := "-" :: !remaining),
    "\t\tadd standard input as source of a todo list to read" ;
    "--rss", Arg.Unit (fun () -> output_type := Rss), "\toutput RSS (default)";

    "--max-items", Arg.Int (fun n -> add_filter (Rss.keep_n_items n)),
    "n\n\t\tkeep only n items in each input channel";

];;

let rss_of_file f =
  let ch =
    match f with
      "-" -> Rss.channel_of_channel stdin
    | _ -> Rss.channel_of_file f
  in
  List.fold_left
    (fun acc f -> f acc)
    ch
    !filters
;;

let main () =
  Arg.parse options
    (fun s -> remaining := s :: !remaining)
    (Printf.sprintf "Usage: %s [options] <files>\nThe '-' file is the standard input.\nOptions are:" Sys.argv.(0));

  try
    let rss =
      match List.rev !remaining with
        [] ->
          prerr_endline (Printf.sprintf "Usage: %s [options] <files>" Sys.argv.(0));
          exit 1
      | [f] -> rss_of_file f
      | f :: q ->
          List.fold_left
            (fun acc f -> Rss.merge_channels (rss_of_file f) acc)
            (rss_of_file f)
            q
    in
    match !output_type with
      Rss ->
        Rss.print_channel (formatter_of_out_channel stdout) rss
  with
    Sys_error s
  | Failure s ->
      prerr_endline s ; exit 1
;;
main ()