(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Interface module for OCamlcvs library. *)

module Types = Ocvs_types
module Revisions = Ocvs_revision

module Diffs =
  struct
    type diffs = Odiff.diffs
    let display_diffs ?on_close ~title ~file diffs =
      let w = Odiff_gui.diffs_window ~title ~file diffs in
      (match on_close with
	None -> ()
      | Some f -> ignore (w#window#connect#destroy f)
      );
      w#window#show ()

    let manual_merge ~title ~file =
      let info = Odiff_gui.build_merge_info file in
      Odiff_gui.manual_merge_window ~title ~file info
  end


module Commands = Ocvs_commands
module Behav = Ocvs_behav
module List = Ocvs_list
module Tree = Ocvs_tree
module Misc = Ocvs_misc
