(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Miscelaneous functions . *)

open Ocvs_types

let get_n_first_ele max l =
  let rec iter n l = 
    if n < max then
      match l with
        [] ->
          ([], [])
      | h :: q ->
          let (l1, l2) = iter (n+1) q in
          (h :: l1, l2)
    else
      ([], l)
  in
  iter 0 l


(** Get subdirectories of a directory, except the CVS directory. 
   Return only the subdirectories handled by CVS, that is,
   the ones with a CVS subdirectory. The returned names are not
   concatenated to the given directory name.*)
let get_cvs_directories dir =
  try
    let dir_desc = Unix.opendir dir in
    let rec iter () =
      try 
	let f = Unix.readdir dir_desc in
	let complete_f = Filename.concat dir f in
	try
	  let st = Unix.stat complete_f in
	  if (f <> "CVS") && 
	    (f <> Filename.current_dir_name) && 
	    (f <> Filename.parent_dir_name) && 
	    (st.Unix.st_kind = Unix.S_DIR) &&
	    (try (Unix.stat (Filename.concat complete_f "CVS")).Unix.st_kind = Unix.S_DIR
	    with _ -> false)
	  then
	    f :: (iter ())
	  else
	    iter ()
	with
	| e ->
	    prerr_endline (Printf.sprintf "get_cvs_directories %s" 
			     (Printexc.to_string e));
	    iter ()
      with
	End_of_file -> 
	  []
      |	e ->
	  prerr_endline (Printf.sprintf "get_cvs_directories %s" 
			   (Printexc.to_string e));
	  []
    in
    let l = iter () in
    Unix.closedir dir_desc;
    l
  with
    Unix.Unix_error (e, s1, s2) ->
      prerr_endline ((Unix.error_message e)^": "^s1^" "^s2);
      []

(** Return the given string without the blanks from the given pos. *)
let chop_whitespace str from =
  let i = ref (String.length str) in
  while !i > from &&
        (let c = str.[!i-1] in c = '\000' || c = ' ' || c = '\t')
  do decr i done;
  String.sub str from (!i - from)

