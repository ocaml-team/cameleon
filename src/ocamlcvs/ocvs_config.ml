(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Configuration options and name of config files.*)

(** The home directory. *)
let home = 
  try Sys.getenv "HOME"
  with Not_found -> ""


(** Create the given empty file. *)
let create_file f =
  try let oc = open_out f in close_out oc
  with Sys_error s -> prerr_endline s

(** The configuration file for OCamlCVS. *)
let rc_file = 
  let f = Filename.concat home ".ocamlcvsrc" in
  (
   try ignore (Unix.stat f)
   with Unix.Unix_error _ -> create_file f
  );
  ref f


(** [cvs add] options. *)
let add_options = ref ""

(** [cvs commit] options. *)
let commit_options = ref ""

(** [cvs remove] options. *)
let remove_options = ref ""

(** [cvs status] options. *)
let status_options = ref ""

(** [cvs update] options. *)
let update_options = ref "-d"

(** Read the given configuration file. *)
let read_rc_file file =
  try
    let chanin = open_in file in
    let line = ref 0 in
    let rec f () =
      let s_opt = 
        try Some (input_line chanin)
        with End_of_file -> None
      in
      match s_opt with
        None -> ()
      | Some s -> 
	  (
	   try
	     let n = String.index s ':' in
	     let before = String.sub s 0 n in
	     let len = String.length s in
	     let after = 
	       if n + 1 >= len 
	       then "" 
	       else String.sub s (n+1) (len - n - 1)
	     in
	     let r = 
	       match Ocvs_misc.chop_whitespace before 0 with
		 "add" -> add_options
	       | "commit" -> commit_options
	       | "remove" -> remove_options
	       | "status" -> status_options
	       | "update" -> update_options
	       | _ -> ref ""
	     in
	     r := after
	   with
	     Not_found ->
	       prerr_endline (Ocvs_messages.error_invalid_syntax file !line s)
	  );
	  incr line ;
	  f ()
    in
    f () ;
    close_in chanin
  with
    Sys_error s -> 
      prerr_endline s ;
      ()

(* Read the default configuration file. *)
let _ = read_rc_file !rc_file
