(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Interface module of the Ocamlcvs library. *)

module Types :
  sig
    type cvs_status =
      | Up_to_date (** The file is identical with the latest revision in the
		      repository for the branch in use. *)
      | Locally_modified (** You have edited the file, and not yet committed
			    your changes. *)
      | Locally_added (** You have added the file with add, and not yet
			 committed your changes. *)
      | Locally_removed (** You have removed the file with remove, and not
			   yet committed your changes. *)
      | Needs_checkout (** Someone else has committed a newer revision to the
			  repository. The name is slightly misleading;
			  you will ordinarily use update rather than checkout
			  to get that newer revision. *)
      | Needs_Patch (** Like Needs Checkout, but the CVS server will send a
		       patch rather than the entire file.
		       Sending a patch or sending an entire file accomplishes
		       the same thing. *)
      | Needs_Merge (** Someone else has committed a newer revision to the
		       repository, and you have also made modifications to the
		       file. *)
      | Conflicts_on_merge
                (** This is like Locally Modified, except that a previous
		   update command gave a conflict. If you have not already
		   done so, you need to resolve the conflict as described
		   in 10.3 Conflicts example. *)
      | Unknown (** CVS doesn't know anything about this file. For example,
		   you have created a new file and have not run add. *)

    type update_action =
	U (** The file was brought up to date with respect to the repository.
	     This is done for any file that exists in the repository but not
	     in your source, and for files that you haven't changed but are
	     not the most recent versions available in the repository. *)
      | P (** Like `U', but the CVS server sends a patch instead of an entire file.
	     These two things accomplish the same thing. *)
      | A (** The file has been added to your private copy of the sources,
	     and will be added to the source repository when you run commit on
	     the file. This is a reminder to you that the file needs to be committed. *)
      | R (** The file has been removed from your private copy of the sources, and will
	     be removed from the source repository when you run commit on the file.
	     This is a reminder to you that the file needs to be committed. *)
      | M (** The file is modified in your working directory. *)
      | C (** A conflict was detected while trying to merge your changes to file
	     with changes from the source repository. *)
      | QM (** file is in your working directory, but does not correspond to anything
	      in the source repository. *)

    (** Information on a file handled by CVS. *)
    type cvs_info = {
	cvs_file : string ; (** absolute file name *)
	cvs_status : cvs_status ;
	cvs_work_rev : string ; (** working revision *)
	cvs_rep_rev : string ; (** repository revision *)
	cvs_date_string : string ; (** the date as displayed by cvs status *)
	cvs_date : float ; (** the date of the last time we got the information on this file *)
      }

    type cvs_revision = {
	rev_number : int list ;
	rev_author : string ;
	rev_date : string ;
	rev_comment : string ;
      }

    (** To report failure while executing a cvs command. *)
    exception CvsFailure of string

    (** To report partial failure (e.g. for one file among various files)
       while executing a cvs command. *)
    exception CvsPartFailure of string

    val string_of_status : cvs_status -> string
    val status_of_string : string -> cvs_status
    val update_action_of_string : string -> update_action

    (** Dump of a [cvs_info] structure. *)
    val dump_cvs_info : cvs_info -> unit
  end

(** Manipulating revisions numbers *)
module Revisions :
    sig
      val string_of_revision_number : int list -> string
      val string_of_revision : Types.cvs_revision -> string
      val children_revisions :
	  Types.cvs_revision list ->
	    Types.cvs_revision -> Types.cvs_revision list
      val first_revision :
	  Types.cvs_revision list -> Types.cvs_revision option
    end

(** Manipulating differences between two files. *)
module Diffs :
  sig
    type diffs = Odiff.diffs
    val display_diffs : ?on_close: (unit -> unit) ->
      title: string -> file: string -> diffs -> unit

    val manual_merge : title: string -> file: string -> unit
  end


(** Functions executing cvs commands. *)
module Commands :
  sig
    val delete_file : string -> unit
    val absolute_name : string -> string

    (** This function returns a file in the form of one string.*)
    val input_file_as_string : string -> string

    (** Return a list of [cvs_info] for the files handled by cvs in
       the given directory, not recursively.*)
    val status_dir : string -> Types.cvs_info list

    (** Return a list of [cvs_info] for the files handled by cvs in
       the given list of files.*)
    val status_files : string list -> Types.cvs_info list

    val commit_files : ?comment:string -> string list -> unit

    val commit_dir : ?comment: string -> string -> unit

    val update_dir :
      ?recursive:bool -> string -> (string * Types.update_action) list

    val add_dir : string -> unit

    val create_and_add_dir : string -> unit

    (** Return the list of added files, which must then have
       the "Locally Added" status, and the list of files
       for which an error occured. *)
    val add_files : ?binary: bool -> string list -> string list * string list

    val remove_files : string list -> string list * string list

    (** Get the last modifications of the given file,
       or between two revisions, or between a revision
       and the working file. Also gives the archive filename.
       Warning : Never give [rev2] without [rev].*)
    val diff_file :
	?rev: Types.cvs_revision ->
	  ?rev2: Types.cvs_revision ->
	    string -> Diffs.diffs * string

    (** Get the content of a revision from the RCS archive in a file
       and return the name of the file. *)
    val rcs_revision : Types.cvs_revision -> string -> string

    (** Get the list of the revisions of a file.
       @raise Ocvs_types.CvsFailure if an error occurs.*)
    val revisions_file : string -> Types.cvs_revision list

    (** Functions for tags *)

    (** Get the list of tags in the given file.
       @return a list of (tag, revision number) (both are strings).*)
    val tags_file : string -> (string * string) list

    (** Add a tag to a list of files. The tag syntax is checked before
       any CVS command is performed.
       @param f_confirm is a function taking a string and returning true or
       false if the user confirm or not the message.
       @raise Tag_error if the given tag is incorrect.
       @raise CvsFailure if the CVS command fails.*)
    val tag_files : (string -> bool) -> string -> string list -> unit

    (** Add a tag to a directory. The tag syntax is checked before
       any CVS command is performed.
       @param f_confirm is a function taking a string and returning true or
       false if the user confirm or not the message.
       @raise Tag_error if the given tag is incorrect.
       @raise CvsFailure if the CVS command fails.*)
    val tag_dir : ?recursive: bool -> (string -> bool) -> string -> string -> unit

    (** Return the result string of the [cvs log] command for the given
       file handled by cvs.*)
    val log : string -> string

  end

(** Types and objects to specify the behaviour of the list and tree
   displaying cvs-handled files. *)
module Behav :
  sig
    type autorization_response =
	Continue (** ok *)
      | Skip (** Skip this file *)
      | Stop (** Stop the command *)

    class type ['a] data =
      object
        method cvs_info_of_t : 'a -> Types.cvs_info

        (** get elements from a key *)
        method elements : string -> 'a list

        method remove_element : string -> unit

        method t_of_cvs_info : Types.cvs_info -> 'a

        (** add or update element *)
        method update_element : 'a -> unit
      end
    class type ct_cvs =
      object
        method cvs_add_dir : string -> unit
        method cvs_add_files : ?binary: bool -> string list -> string list * string list
        method cvs_commit_files : ?comment:string -> string list -> unit
	method cvs_commit_dir : ?comment: string -> string -> unit
        method cvs_create_and_add_dir : string -> unit
        method cvs_remove_files : string list -> string list * string list
        method cvs_status_dir : string -> unit
        method cvs_status_files : string list -> unit
        method cvs_update_dir :
          string -> (string * Types.update_action) list
	method cvs_diff_file :
	    ?rev: Types.cvs_revision ->
	      ?rev2: Types.cvs_revision -> string -> Diffs.diffs * string
	method rcs_revision : Types.cvs_revision -> string -> string
	method cvs_revisions_file : string -> Types.cvs_revision list

        method cvs_tags_file : string -> (string * string) list
	method cvs_tag_files : (string -> bool) -> string -> string list -> unit
	method cvs_tag_dir : ?recursive: bool -> (string -> bool) -> string -> string -> unit

	method cvs_log_file : string -> string
      end
    class type ['b] list_behaviour =
      object
	inherit ct_cvs

        (** to call after an operation on a file,
	   for example, to reload a file after an update.*)
	method after_action : 'b -> unit

        (** to call for each file before performing an operation on it,
	   for example to save a file before a commit. *)
	method autorize_file : 'b -> autorization_response

	method cvs_info_of_t : 'b -> Types.cvs_info

        (** the function giving the optional color and the
	  strings to display for a file in a list *)
	method display_strings : 'b -> string option * string list

       (** return a comparison function in function of a number (0-based) of column.
	  The comparison function is used to sort a list of elements. *)
	method comparison_function : int -> ('b -> 'b -> int)

        (** get the elements from a directory *)
        method elements : string -> 'b list

        (** to get the contextual menu, depending on the selected elements *)
	method menu_ctx : 'b list -> GToolbox.menu_entry list

        method remove_element : string -> unit

        (** to call when an element is selected *)
	method select : 'b -> unit

        method t_of_cvs_info : Types.cvs_info -> 'b

        (** the titles of columns *)
        method titles : string list

        (** to call when an element is unselected *)
	method unselect : 'b -> unit

        (** to call when an element is doubel clicked *)
	method double_click : 'b -> unit

        (** add or update element *)
        method update_element : 'b -> unit

        (** indicate whether the update of the list needs a 'cvs status'.*)
	method needs_cvs_status : bool
      end
    class type ['c] tree_behaviour =
      object
	inherit ct_cvs

        (** expand the given directory or not *)
	method expand_dir : string -> bool

        (** set the given directory as expanded *)
	method add_expanded_dir : string -> unit

        (** remove the given directory as expanded *)
	method remove_expanded_dir : string -> unit

        (** to get the contextual menu, depending on the selected directory *)
	method menu_ctx : string option -> GToolbox.menu_entry list

	(** root directories *)
        method roots : string list

        (** to call when a directory is selected *)
	method select : string -> unit

        method t_of_cvs_info : Types.cvs_info -> 'c

        (** to call when a directory is unselected *)
	method unselect : string -> unit

        (** add or update element *)
        method update_element : 'c -> unit
      end
    class ['d] cvs :
	'd data ->
	  object
            method cvs_add_dir : string -> unit
            method cvs_add_files : ?binary: bool -> string list -> string list * string list
            method cvs_commit_files : ?comment:string -> string list -> unit
	    method cvs_commit_dir : ?comment: string -> string -> unit
            method cvs_create_and_add_dir : string -> unit
            method cvs_remove_files : string list -> string list * string list
            method cvs_status_dir : string -> unit
            method cvs_status_files : string list -> unit
            method cvs_update_dir :
		string -> (string * Types.update_action) list
	    method cvs_diff_file :
		?rev: Types.cvs_revision ->
		  ?rev2: Types.cvs_revision ->
		    string -> Diffs.diffs * string
	    method rcs_revision : Types.cvs_revision -> string -> string
	    method cvs_revisions_file : string -> Types.cvs_revision list
	    method cvs_log_file : string -> string

            (** Functions for tags *)

            method cvs_tags_file : string -> (string * string) list
	    method cvs_tag_files : (string -> bool) -> string -> string list -> unit
	    method cvs_tag_dir : ?recursive: bool -> (string -> bool) -> string -> string -> unit
	  end
  end

(** The list widget to display files. *)
module List :
  sig
    val display_string_list_list :
	?modal: bool ->
	  ?width: int -> ?height: int ->
	    string -> string list -> string list list -> unit

    val display_log : ?modal: bool -> ?width:int -> ?height: int ->
      title: string -> log: string -> unit -> unit

    class ['e] box :
	?display_dir: bool ->
	  'e Behav.list_behaviour ->
      object
        val mutable dir : string option
        val mutable elements : 'e list
        val mutable selection : 'e list
        method box : GPack.box
        method cvs_commit_selected_files : unit
        method cvs_remove_selected_files : unit
	method cvs_tag_selected_files : unit
	method cvs_tags_of_file : unit
	method cvs_lastdiff_file : unit
	method cvs_revisions_file : 'e -> Types.cvs_revision list
	method cvs_select_revision : 'e -> Types.cvs_revision option
	method cvs_differences_with : unit
	method cvs_differences_between : unit
	method cvs_resolve_conflicts : unit
	method cvs_log_file : unit
        method display_dir : ?force: bool -> string option -> unit
        method selection : 'e list
        method update : bool -> unit
      end
  end

(** The tree widget to display directories. *)
module Tree :
  sig
    val file_exists : string -> bool
    val is_prefix : string -> string -> bool
    class ['f] box :
      'f Behav.tree_behaviour ->
      object
        val mutable selection : string option
        method box : GPack.box
        method cvs_add_dir : unit
        method cvs_add_files : unit
        method cvs_add_binary_files : unit
        method cvs_update_dir : unit
	method cvs_commit_dir : unit
	method cvs_tag_dir : unit

        method insert_node : ?parent: Gtk.tree_iter -> string -> string -> unit
	method selection : string option
        method select_dir : string -> unit
        method unselect_dir : string -> unit

        method update : unit
        method update_selected_dir : unit
      end
  end
