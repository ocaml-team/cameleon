(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Messages for OCamlCVS. *)

let software = "OCamlCVS"
let software_version = "1.3"

let mYes = "Yes"
let mNo = "No"
let mOk = "Ok"
let mCancel = "Cancel"
let mConfirm = "Confirmation"

let error = "Error"
let error_exec com = "Error while executing the following command:\n"^com
let error_status_string s = "Unknon status string: "^s
let error_update_action_string s = "Unknon update action string: "^s
let error_mkdir d s = "Could not create directory "^d^":\n"^s
let error_add_files l =
  List.fold_left
    (fun acc -> fun f -> acc^"\n"^f)
    "The following files could not be added:"
    l
let error_remove_files l =
  List.fold_left
    (fun acc -> fun f -> acc^"\n"^f)
    "The following files could not be removed:"
    l
let error_analyze_revision s =
  "Error while analyzing the following revision info :\n"^s

let error_rcs_archive = "Could not get RCS archive name from CVS output."
let error_tag_char tag n = "Character "^(string_of_int n)^" ("^(String.make 1 tag.[n])^") is incorrect in tag '"^tag^"'."
let error_invalid_syntax f line s = "File "^f^": syntax error line "^(string_of_int line)^": "^s


let directory = "Directory"
let enter_comment = "Enter a comment"
let enter_comment_commit = "Please enter a comment for commit"
let add_dir = "Add directory"
let should_create_dir d = "Directory "^d^" does not exist.\nCreate it ?"
let add_files = "Add file"
let remove_files = "Remove files"
let confirm_remove_files l =
  List.fold_left
    (fun acc -> fun f -> acc^"\n"^f)
    "The following files will be removed from the repository and locally deleted :"
    l
let revision = "Revision"
let author = "Author"
let date = "Date"
let working_rev = "Working file"

let enter_tag_for_files l =
  List.fold_left
    (fun acc -> fun f -> acc^"\n"^f)
    "Enter a tag for the working revision of the following files :"
    l
let enter_tag_for_dir d ="Enter a tag for the working revision of files in "^d^" :"
let tags_of file = "Tags of "^file
let tag = "Tag"
let files_already_has_tag files tag =
  (List.fold_left
     (fun acc -> fun f -> acc^f^"\n")
     ("The following files already have a tag '"^tag^"' :\n")
     files
  )^
  "\nTagging them again with this tag will change \n"^
  "the revision number associated to this tag.\n\nContinue ?"

let resolve_conflicts_first = "Resolve conflicts first."
let close = "Close"
let no_diff_to_display = "No differences to display."

(** Menu labels *)
let m_file = "File"
let m_cvs = "CVS"
let m_add_dir = "Add directory"
let m_update_dir = "Update selected directory"
let m_commit_dir = "Commit selected directory"
let m_add_files = "Add file"
let m_add_binary_files = "Add binary file"
let m_commit_files = "Commit selected files"
let m_remove_files = "Remove selected files"
let m_quit = "Quit"
let m_last_diff = "Last differences"
let m_diff_with = "Differences with ..."
let m_diff_between = "Differences between ..."
let m_tag_dir = "Tag selected directory"
let m_tag_files = "Tag selected files"
let m_tags_of_file = "List tags of the selected file"
let m_resolve_conflicts = "Resolve merge conflicts"
let m_log = "Log"
