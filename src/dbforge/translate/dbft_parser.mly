/**************************************************************************/
/*                   Cameleon                                             */
/*                                                                        */
/*      Copyright (C) 2002 Institut National de Recherche en Informatique et   */
/*      en Automatique. All rights reserved.                              */
/*                                                                        */
/*      This program is free software; you can redistribute it and/or modify  */
/*      it under the terms of the GNU General Public License as published by  */
/*      the Free Software Foundation; either version 2 of the License, or  */
/*      any later version.                                                */
/*                                                                        */
/*      This program is distributed in the hope that it will be useful,   */
/*      but WITHOUT ANY WARRANTY; without even the implied warranty of    */
/*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the     */
/*      GNU General Public License for more details.                      */
/*                                                                        */
/*      You should have received a copy of the GNU General Public License  */
/*      along with this program; if not, write to the Free Software       */
/*      Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA          */
/*      02111-1307  USA                                                   */
/*                                                                        */
/*      Contact: Maxence.Guesdon@inria.fr                                */
/**************************************************************************/

%{
(***********************************************************************)
(*                             DBForge                                 *)
(*                                                                     *)
(*            Maxence Guesdon, projet Cristal, INRIA Rocquencourt      *)
(*                                                                     *)
(*  Copyright 2001 Institut National de Recherche en Informatique et   *)
(*  en Automatique.  All rights reserved.  This file is distributed    *)
(*  under the terms of the GNU General Public License version 2.       *)
(*                                                                     *)
(***********************************************************************)

open Dbft_documentIO

let unmatched_mark  m1 m2 = m1^" ended by "^m2^"."

%}

/* Token */
%token <string> START_MARK
%token <string> END_MARK
%token <string> VALUE

%token EOF

/* Start Symbols */
%start main 
%type <Dbft_documentIO.item>   main

%%
main:
  item EOF      { $1 }
;

item:
  START_MARK s_value END_MARK
  { 
    (*DEBUG*)print_string "item"; print_newline();
    if $1 = $3 then
      match $2 with
	Simple_item (_, v) ->
	  Simple_item ($1, v)
      |	Subitems (_, l) ->
	  Subitems ($1, l)
    else
      raise (Failure (unmatched_mark  $1 $3))
  } 
| START_MARK END_MARK
    {
      if $1 = $2 then
	Simple_item ($1, "")
      else
	raise (Failure (unmatched_mark $1 $2))
    } 
;

s_value:
  VALUE
  { 
    (*DEBUG*)print_string ("s_value VALUE = "^$1); print_newline(); 
    Simple_item ("", $1) 
  }
| item_list
    {
      (*DEBUG*)print_string "s_value item_list"; print_newline();
      Subitems ("", $1)
    }
;

item_list:
  item 
  { 
    (*DEBUG*)print_string "item_list item"; print_newline();
    [$1]
  }
| item item_list
    {
      (*DEBUG*)print_string "item_list item item_list"; print_newline();
      $2 @ [$1]
    }
%%
