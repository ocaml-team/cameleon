(**************************************************************************)
(*                   Cameleon                                             *)
(*                                                                        *)
(*      Copyright (C) 2002 Institut National de Recherche en Informatique et   *)
(*      en Automatique. All rights reserved.                              *)
(*                                                                        *)
(*      This program is free software; you can redistribute it and/or modify  *)
(*      it under the terms of the GNU General Public License as published by  *)
(*      the Free Software Foundation; either version 2 of the License, or  *)
(*      any later version.                                                *)
(*                                                                        *)
(*      This program is distributed in the hope that it will be useful,   *)
(*      but WITHOUT ANY WARRANTY; without even the implied warranty of    *)
(*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the     *)
(*      GNU General Public License for more details.                      *)
(*                                                                        *)
(*      You should have received a copy of the GNU General Public License  *)
(*      along with this program; if not, write to the Free Software       *)
(*      Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA          *)
(*      02111-1307  USA                                                   *)
(*                                                                        *)
(*      Contact: Maxence.Guesdon@inria.fr                                *)
(**************************************************************************)

{
(***********************************************************************)
(*                             DBForge                                 *)
(*                                                                     *)
(*            Maxence Guesdon, projet Cristal, INRIA Rocquencourt      *)
(*                                                                     *)
(*  Copyright 2001 Institut National de Recherche en Informatique et   *)
(*  en Automatique.  All rights reserved.  This file is distributed    *)
(*  under the terms of the GNU General Public License version 2.       *)
(*                                                                     *)
(***********************************************************************)

open Dbft_parser

let line_number = ref 0

(*[Fonc] This function takes a char and a string, an return the
   number of occurences of c in s.[Fonc]*)
let occ_number c s =
  let rec iter cpt_acc n =
    if n >= String.length s then
      cpt_acc
    else
      if s.[n] = c then
	iter (cpt_acc + 1) (n+1)
      else
	iter cpt_acc (n+1)
  in
  iter 0 0

let blank = "[' ' '\013' '\009' '\012' '\010']"

(* remove the blank from a string *)
let remove_blank s = 
  let s2 = Str.global_replace (Str.regexp (blank^"+")) "" s in 
  s2
}

rule main = parse
    [' ' '\013' '\009' '\012']
      { main lexbuf }

  | [ '\010' ]
      { incr line_number;
         main lexbuf }

  | '<'['a'-'z' 'A'-'Z'] ['_' 'A'-'Z' '-' 'a'-'z' '0'-'9' ]*'>'
      { 
	let s = String.uppercase (Lexing.lexeme lexbuf) in
	START_MARK (String.sub s 1 ((String.length s) - 2))
      }

  | "</"['a'-'z' 'A'-'Z'] ['_' 'A'-'Z' '-' 'a'-'z' '0'-'9' ]*'>'
      { 
	let s = String.uppercase (Lexing.lexeme lexbuf) in
	END_MARK (String.sub s 2 ((String.length s) - 3))
      }

  | eof  { EOF }

  | [^'<']+
      { (* add the number of \n to the line number *)
	let s = Lexing.lexeme lexbuf in
	let n = occ_number '\n' s in
	line_number := !line_number + n;
	if remove_blank s = "" then
	  main lexbuf
	else
	  (* return the value *)
	  VALUE (s) 
      }
  | _
      {
	let s = Lexing.lexeme lexbuf in
        (*DEBUG*)print_string ("Empty token #"^s^"# line "^(string_of_int !line_number)); print_newline ();
	raise (Failure "Empty token")
      }	

{

module Parser =
  struct
    let parse_error n = "Parse error line "^(string_of_int n)
    let init () = line_number := 0
    let parse lexbuf = 
      try
	(* DEBUG *) print_string "Dbft_parser.main main ..." ; print_newline();
	Dbft_parser.main main lexbuf
      with
	Parsing.Parse_error ->
	  raise (Failure (parse_error !line_number))
  end
} 
