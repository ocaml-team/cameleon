(**************************************************************************)
(*                   Cameleon                                             *)
(*                                                                        *)
(*      Copyright (C) 2002 Institut National de Recherche en Informatique et   *)
(*      en Automatique. All rights reserved.                              *)
(*                                                                        *)
(*      This program is free software; you can redistribute it and/or modify  *)
(*      it under the terms of the GNU General Public License as published by  *)
(*      the Free Software Foundation; either version 2 of the License, or  *)
(*      any later version.                                                *)
(*                                                                        *)
(*      This program is distributed in the hope that it will be useful,   *)
(*      but WITHOUT ANY WARRANTY; without even the implied warranty of    *)
(*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the     *)
(*      GNU General Public License for more details.                      *)
(*                                                                        *)
(*      You should have received a copy of the GNU General Public License  *)
(*      along with this program; if not, write to the Free Software       *)
(*      Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA          *)
(*      02111-1307  USA                                                   *)
(*                                                                        *)
(*      Contact: Maxence.Guesdon@inria.fr                                *)
(**************************************************************************)

let _ =
  try
    let file = Sys.argv.(1) in
    
    let input_string nom =
      let chanin = open_in nom in
      let rec iter stracc =
	try
	  let chaine_lue = input_line chanin in
	  iter (stracc^chaine_lue^"\n") 
	with
	  End_of_file -> stracc
      in
      let v = iter "" in
      close_in chanin;
      let len = String.length v in
      if len <= 1 then
	v
      else
	(String.sub v 0 (len - 1))
	  
    in
    let s = input_string file in
    
    let queries = Str.split (Str.regexp ";\n") s in

    let exec_query q =
      try
	let direct_exec_stmt = Ocamlsql.parse_sql_stmt (q^";") in
	output_string stdout (q^":OK:\n");
	let stest = Ocamlsql.String.string_of_direct_sql_stmt direct_exec_stmt in
	let direct_exec_stmt2 = Ocamlsql.parse_sql_stmt stest in
	if direct_exec_stmt = direct_exec_stmt2 then
	  output_string stdout ("Pareil !\n")
	else
	    (
	     output_string stdout "Pas pareil :\n";
	     output_string stdout stest;
	     output_string stdout "\n";
	     output_string stdout (Ocamlsql.String.string_of_direct_sql_stmt direct_exec_stmt2);
	     output_string stdout "\n";
	    )
      with
	Failure s ->
	  output_string stdout (q^":\n"^s^"\n")
    in
    List.iter exec_query queries
  with
    _ ->
      try
	let s = read_line () in
	let direct_exec_stmt = Ocamlsql.parse_sql_stmt s in
	output_string stdout (s^":OK\n")
      with
	Failure s ->
	  output_string stdout (s^":\n")
