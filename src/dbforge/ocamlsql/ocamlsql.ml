(**************************************************************************)
(*                   Cameleon                                             *)
(*                                                                        *)
(*      Copyright (C) 2002 Institut National de Recherche en Informatique et   *)
(*      en Automatique. All rights reserved.                              *)
(*                                                                        *)
(*      This program is free software; you can redistribute it and/or modify  *)
(*      it under the terms of the GNU General Public License as published by  *)
(*      the Free Software Foundation; either version 2 of the License, or  *)
(*      any later version.                                                *)
(*                                                                        *)
(*      This program is distributed in the hope that it will be useful,   *)
(*      but WITHOUT ANY WARRANTY; without even the implied warranty of    *)
(*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the     *)
(*      GNU General Public License for more details.                      *)
(*                                                                        *)
(*      You should have received a copy of the GNU General Public License  *)
(*      along with this program; if not, write to the Free Software       *)
(*      Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA          *)
(*      02111-1307  USA                                                   *)
(*                                                                        *)
(*      Contact: Maxence.Guesdon@inria.fr                                *)
(**************************************************************************)

(*[Mo] This module is the top module of the OCamlSQL library. [Mo]*)


(*[Fonc] This function takes a string (sql statement) and parses it. It returns
   a direct_sql_stmt value. Raises Failure if an error occurs.
   Raises End_of_file if nothing is encountered before End_of_file.[Fonc]*)
let parse_sql_stmt s =
  Ocamlsql_lexer.line_number := 0;
  let lexbuf = Lexing.from_string s in
  try
    let dt = Ocamlsql_parser.main Ocamlsql_lexer.main lexbuf in
    dt
  with
    Parsing.Parse_error ->
      raise (Failure (Ocamlsql_messages.parse_error !Ocamlsql_lexer.line_number))

(*[Fonc] This function takes a channel and try to parse it as a SQL statement.
   It returns a direct_sql_stmt value. Raises Failure if an error occurs.
   Raises End_of_file if nothing is encountered before End_of_file.
   [Fonc]*)
let parse_sql_stmt_from_channel channel =
  Ocamlsql_lexer.line_number := 0;
  let lexbuf = Lexing.from_channel channel in
  try
    let dt = Ocamlsql_parser.main Ocamlsql_lexer.main lexbuf in
    dt
  with
    Parsing.Parse_error ->
      raise (Failure (Ocamlsql_messages.parse_error !Ocamlsql_lexer.line_number))
      

(*[Fonc] This function takes a string (column definition) and parses it. It returns
   a column_def value. Raises Failure if an error occurs.[Fonc]*)
let parse_column_def s =
  Ocamlsql_lexer.line_number := 0;
  let lexbuf = Lexing.from_string s in
  try
    let dt = Ocamlsql_parser.column_def Ocamlsql_lexer.main lexbuf in
    dt
  with
    Parsing.Parse_error ->
      raise (Failure (Ocamlsql_messages.parse_error !Ocamlsql_lexer.line_number))

(*[Mo] This module contains the functions used to get string representation of OCamlSQL types.[Mo]*)
module String = Ocamlsql_print

