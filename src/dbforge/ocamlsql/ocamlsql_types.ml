(**************************************************************************)
(*                   Cameleon                                             *)
(*                                                                        *)
(*      Copyright (C) 2002 Institut National de Recherche en Informatique et   *)
(*      en Automatique. All rights reserved.                              *)
(*                                                                        *)
(*      This program is free software; you can redistribute it and/or modify  *)
(*      it under the terms of the GNU General Public License as published by  *)
(*      the Free Software Foundation; either version 2 of the License, or  *)
(*      any later version.                                                *)
(*                                                                        *)
(*      This program is distributed in the hope that it will be useful,   *)
(*      but WITHOUT ANY WARRANTY; without even the implied warranty of    *)
(*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the     *)
(*      GNU General Public License for more details.                      *)
(*                                                                        *)
(*      You should have received a copy of the GNU General Public License  *)
(*      along with this program; if not, write to the Free Software       *)
(*      Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA          *)
(*      02111-1307  USA                                                   *)
(*                                                                        *)
(*      Contact: Maxence.Guesdon@inria.fr                                *)
(**************************************************************************)

(*[Mo] The types to represent SQL queries.[Mo]*)

(* action *)
type action = 
    Action_select
  | Action_delete
  | Action_insert of privilege_column list
  | Action_update of privilege_column list
  | Action_references of privilege_column list
  | Action_usage

(* alter column action *)
and alter_column_action =
    Set_column_default_clause of default_clause
  | Drop_column_default_clause

(* alter domain action *)
and alter_domain_action =
    Add_domain_constraint of domain_constraint
  | Drop_domain_constraint of constraint_name
  | Set_domain_default_clause of default_clause
  | Drop_domain_default_clause

(* alter table action *)
and alter_table_action =
    Add_column of column_def
  | Alter_column of column_name * alter_column_action
  | Add_table_constraint of table_constraint_def
  | Drop_column of column_name * drop_behavior
  | Drop_table_constraint of constraint_name * drop_behavior

(* approximate_num_lit *)
and approximate_num_lit = float * int

(*[Ty] The approximate numeric types.[Ty]*)
and approx_num_type =
    Float of precision option
	(* for
	   "float" [ "(" precision ")" ] 
	*)

  | Real
      (* for
	 "real"
      *)

  | Double of precision
	(* for
	   "double" precision
	*)

(* author id *)
and author_id = id

(* boolean factor *)
and boolean_factor =
    Boolean_test of boolean_test
  | Not_boolean_test of boolean_test

(* boolean primary *)
and boolean_primary =
    Boolean_predicate of predicate
  | Boolean_search_condition of search_condition

(* boolean term *)
and boolean_term =
    Boolean_factor of boolean_factor
  | Boolean_and of boolean_term * boolean_factor

(* boolean test *)
and boolean_test =
    Boolean_primary of boolean_primary
  | Boolean_primary_is of (boolean_primary * truth_value)
  | Boolean_primary_is_not of (boolean_primary * truth_value)

(* cast operand *)
and cast_operand =
    Cast_op_value of value_exp
  | Cast_op_null

(* cast spec *)
and cast_spec = cast_operand * cast_target
    
(* cast target *)
and cast_target =
    Cast_domain of domain_name
  | Cast_data_type of data_type

and catalog_name = id

(* char factor *)
and char_factor = char_primary * (collation_name option)

(* char primary *)
and char_primary =
    Char_primary_value_exp of value_exp_primary
  | Char_primary_string_value_fct of string_value_fct

(* char string literal *)
and char_string_lit = string

(* char value expressions *)
and char_value_exp =
    Concatenation of concatenation
  | Char_factor of char_factor

(* char value functions *)
and char_value_fct =
    Char_substring_fct of char_value_exp * start_position * (string_length option)
  | Char_translation of char_value_exp * translation_name
  | Char_fold of fold_style * char_value_exp
  | Char_form_conversion of char_value_exp * form_conversion_name
  | Char_trim_fct of trim_operands

and check_constraint = search_condition

and collate_clause = collation_name

and collation_def = string (* A VOIR *)

(* collation name *)
and collation_name = qualified_name

(* column_constraint *)
and column_constraint =
    Column_not_null
  | Column_unique of unique_spec
  | Column_refs of refs_spec
  | Column_check_constraint of search_condition 

(* column constraint definition *)
and column_constraint_def = (constraint_name_def option) * column_constraint * constraint_attribute list

(* type of a column *)
and column_type =
    Data_type of data_type
  | Domain of domain_name

(* column definition *)
and column_def = {
    col_name : column_name ;
    col_type : column_type ;
    col_default_clause : default_clause option ;
    col_constaints : column_constraint_def list ;
    col_collate_clause : collation_name option
  } 

(* column name *)
and column_name = id

and column_name_list = column_name list

(* column_ref *)
and column_ref = (qualifier option) * column_name

(* comparison operators *)
and comp_op =
    Equals
  | Not_equals
  | Strict_less
  | Strict_greater
  | Less_or_equals
  | Greater_or_equals

(* comparison predicate *)
and comp_predicate = row_value_cons * comp_op * row_value_cons

(* concatenation *)
and concatenation = (char_value_exp * char_factor)

(* constraint attribute *)
and constraint_attribute =
  | Deferrrable
  | Not_deferrable
  | Initially_deferred
  | Initially_immediate

(* constraint name *)
and constraint_name = qualified_name

and constraint_name_def = constraint_name

(* correlation name *)
and correlation_name = id

and corresponding_column_list = column_name list

and corresponding_spec = corresponding_column_list

(*[Ty] The standard SQL types. [Ty]*)
and data_type =
  | Char of length option
	(* for
	   "character" [ "(" length ")" ]
	   "char" [ "(" length ")" ]
	*)

  | VarChar of length
	(* for
	   "varchar" "(" length ")"
	   "char varying" "(" length ")"
	   "character varying" "(" length ")"
	*)
	(* A VOIR : "character set ..." *)

  | NationalChar of length option
	(* for 
	   "national" "character" [ "(" length ")" ] 
	   | "national" "char" [ "(" length ")" ] 
	   | "nchar" [ "(" length ")" ] 
	*)

  | NationalVarChar of length
	(* for
	   "national" "character" "varying" "(" length ")" 
	   | "national" "char" "varying" "(" length ")" 
	   | "nchar" "varying" "(" length ")" 
	*)

  | Bit of length option
        (* for 
	   "bit" [ "(" length ")" ] 
	*)

  | VarBit of length
	(* for
	   "bit" "varying" "(" length ")" 
	*)

  | Num of num_type

  | Date
      (* for 
	 "date"
      *)

  | Time of (time_precision option * bool)
	(* bool is true = "with time zone" *)
	(* for
	   | "time" [ "(" time_precision ")" ] 
	   [ "with" "time" "zone" ]
	*)
	
  | Timestamp of (timestamp_precision option * bool)
	(* bool is true = "with time zone" *)
	(* for
	   | "timestamp" [ "(" time_precision ")" ] 
	   [ "with" "time" "zone" ]
	*)

  | Interval of interval_qualifier

(* datetime_factor *)
and datetime_factor = datetime_primary * (time_zone option)

(* datetiem_field *)
and datetime_field = 
    Datetime_field_second
  | Datetime_field_minute
  | Datetime_field_hour
  | Datetime_field_day
  | Datetime_field_month
  | Datetime_field_year

(* datetime literal *)
and datetime_lit =
    Date_lit of date_value
  | Time_lit of time_lit
  | Timestamp_lit of timestamp_lit

(* datetime_primary *)
and datetime_primary =
    Datetime_value_exp of value_exp_primary
  | Datetime_value_fct of datetime_value_fct

and datetime_term = datetime_factor

and datetime_value = int

(* datetime_value_exp *)
and datetime_value_exp =
    Datetime_term of datetime_term
  | Datetime_interval_value_plus_term of interval_value_exp * datetime_term
  | Datetime_value_plus_interval_term of datetime_value_exp * interval_term
  | Datetime_value_minus_interval_term of datetime_value_exp * interval_term

(* datetime_value_fct *)
and datetime_value_fct =
    Current_timestamp of timestamp_precision option
  | Current_date
  | Current_time of time_precision option

(* date value *)
and date_value = {
    date_year : int;
    date_month : int;
    date_day : int
  } 

(* default clause *)
and default_clause =
    Default_lit of lit
  | Default_datetime_value_fct of datetime_value_fct
  | Default_user
  | Default_current_user
  | Default_session_user
  | Default_system_user
  | Default_null
    
and delete_rule = ref_action

(* delete_stmt_searched *)
and delete_stmt_searched = table_name * (search_condition option)

(* derived column *)
and derived_column = value_exp * (column_name option)

and derived_column_list = column_name list

and derived_table = table_subquery

and direct_sql_stmt = directly_exec_stmt

(* directly_exec_stmt *)
and directly_exec_stmt =
    Direct_sql_data_stmt of direct_sql_data_stmt
  | Sql_schema_stmt of sql_schema_stmt
  | Sql_transaction_stmt of sql_transaction_stmt
  | Sql_connection_stmt of sql_connection_stmt
  | Sql_session_stmt of sql_session_stmt
(*A VOIR   | Direct_implt_def_stmt of direct_implt_def_stmt*)

(* direct_sql_data_stmt *)
and direct_sql_data_stmt =
    Direct_delete_stmt_searched of delete_stmt_searched
  | Direct_select_stmt_n_rows of direct_select_stmt_n_rows
  | Direct_insert_stmt of insert_stmt
  | Direct_update_stmt_searched of update_stmt_searched
  | Direct_temporary_table_decl of temporary_table_decl

(* direct_select_stmt_n_rows *)
and direct_select_stmt_n_rows = query_exp * (order_by_clause option)

(* domain constraint *)
and domain_constraint = (constraint_name_def option) * check_constraint * constraint_attribute list

(* domain definition *)
and domain_def = {
    dom_name : domain_name;
    dom_data_type : data_type;
    dom_default_clause : default_clause option;
    dom_constaints : domain_constraint list ;
    dom_collate_clause : collation_name option
  } 

and domain_name = qualified_name

(* drop behavior *)
and drop_behavior =
    Cascade
  | Restrict

(* end field *)
and end_field = datetime_field * (int option)

and escape_char = char_value_exp

(* exact numeric literal *)
and exact_num_lit =
    Exact_float of float
  | Exact_int of int

(*[Ty] The exact numeric types.[Ty]*)
and exact_num_type =
  | Numeric of (precision  * scale option) option 
	(* for
	   "numeric" [ "(" precision [ "," scale ] ")" ] 
	*)

  | Decimal of (precision  * scale option) option 
	(* for 
	   "decimal" [ "(" precision [ "," scale ] ")" ] 
	   | "dec" [ "(" precision [ "," scale ] ")" ] 
	*)

  | Integer
      (* for
	 "integer"
	 "int"
      *)

  | SmallInteger
      (* for 
	 "smallint"
      *)

and explicit_table = table_name

(* extract field *)
and extract_field =
    Extract_datetime of datetime_field
  | Extract_time_zone of time_zone_field

(* extract source *)
and extract_source = 
    Extract_source_datetime of datetime_value_exp
  | Extract_source_interval of interval_value_exp

(* factor *)
and factor = (sign option) * num_primary
    
(* fold style *)
and fold_style = 
    Fold_upper
  | Fold_lower

(* form conversion name *)
and form_conversion_name = qualified_name

(* from clause *)
and from_clause = table_ref list

(* general_lit *)
and general_lit =
    Char_string_lit of char_string_lit 
  | National_char_string_lit  of national_char_string_lit 
(* A VOIR  | Bit_string_lit of bit_string_lit *)
(* A VOIR  | Hex_string_lit of hex_string_lit *)
  | Datetime_lit of datetime_lit 
  | Interval_lit of interval_lit 

(* general_set_fct *)
and general_set_fct = set_fct_type * (set_quantifier option) * value_exp

(* general_value_spec *)
and general_value_spec =
(* A VOIR    Gen_val_paramter_spec of parameter_spec*)
(* A VOIR  | Gen_val_dyn_paramter_spec of dyn_parameter_spec *)
(* A VOIR  | Gen_val_variable_spec of variable_spec *)
  | Gen_val_user
  | Gen_val_current_user
  | Gen_val_session_user
  | Gen_val_system_user
  | Gen_val_value

(* grantee (someone who's granted) *)
and grantee =
    Grantee_public
  | Grantee_author_id of author_id

(* grant statement *)
and grant_stmt = {
    grant_privileges : privileges ;
    grant_object : object_name ;
    grant_grantees : grantee list ;
    grant_with_grant_option : bool
  } 

(* grouping_column_ref *)
and grouping_column_ref = column_ref * (collation_name option)

and group_by_clause = grouping_column_ref list

and having_clause = search_condition

(* id *)
and id = string

(* insert_columns_and_source *)
and insert_columns_and_source =
    Insert_query_exp of insert_column_list * query_exp
  | Insert_default_values

and insert_column_list = column_name list

(* insert statement *)
and insert_stmt = table_name * insert_columns_and_source

(* interval factor *)
and interval_factor = (sign option) * interval_primary

(* interval literal *)
and interval_lit = (sign option) * interval_string * interval_qualifier

(* interval primary *)
and interval_primary = value_exp_primary * (interval_qualifier option)

(* interval qualifier *)
and interval_qualifier = 
    Interval_start_end of start_field * end_field
  | Interval_single_datetime_field of single_datetime_field

(* interval string *)
and interval_string = string (* A VOIR *)

(* interval term *)
and interval_term =
    Interval_term_factor of interval_factor
  | Interval_term_div of interval_term * factor
  | Interval_term_mult of interval_term * factor
  | Interval_term_mult_interval of term * interval_factor

(* interval value exp *)
and interval_value_exp =
    Interval_value_term of interval_term
  | Interval_value_plus of interval_value_exp * interval_term
  | Interval_value_minus of interval_value_exp * interval_term
  | Interval_value_date of datetime_value_exp * datetime_term * interval_qualifier

(* in predicate value *)
and in_predicate_value =
    In_subquery of table_subquery
  | In_values of in_value_list

and in_value_list = value_exp list

(* joined tables *)
and joined_table =
    Cross_join of (table_ref * table_ref)
  | Qualified_join of qualified_join

and join_column_list = column_name list

and join_condition = search_condition

(* join specification *)
and join_spec =
    Join_condition of join_condition
  | Join_named_columns of named_columns_join

(* join type *)
and join_type =
    Inner_join
  | Union
  | Left_outer_join
  | Right_outer_join
  | Full_outer_join

(*[Ty] The type for lengths.[Ty]*)
and length = int

(* length exp *)
and length_exp =
    Char_length_exp of string_value_exp
  | Octet_length_exp of string_value_exp
  | Bit_length_exp of string_value_exp

(* levels clause *)
and levels_clause =
    Levels_cascaded
  | Levels_local


(* literal *)
and lit = 
    Signed_num_lit of signed_num_lit
  | General_lit of general_lit

and local_table_name = qualified_id

(* match rredicate *)
and match_predicate = row_value_cons * bool (*unique or not*) * match_type * table_subquery

and match_type = 
    Match_partial
  | Match_full 
      
and match_value = char_value_exp

and named_columns_join = join_column_list

(* national char string literal *)
and national_char_string_lit = string (* A VOIR : lexeur ?*)
      
(* non_join_query_exp *)
and non_join_query_exp =
    Non_join_query_term of non_join_query_term
  | Non_join_query_union of query_exp * bool (* true = with "all" *) * (corresponding_spec option) * query_term
  | Non_join_query_except of query_exp * bool (* true = with "all" *) * (corresponding_spec option) * query_term
	
(* non_join_query_primary *)
and non_join_query_primary =
    Non_join_simple_table of simple_table
  | Non_join_query_exp of non_join_query_exp
	
(* non_join_query_term *)
and non_join_query_term =
    Non_join_query_primary of non_join_query_primary
  | Intersect of query_term * bool (* true = "all"*) * (corresponding_spec option) * query_primary

(* num primary *)
and num_primary = 
    Num_value of value_exp_primary
  | Num_fct of num_value_fct
				      
(*[Ty] The numeric types.[Ty]*)
and num_type =
    ExactNumType of exact_num_type
  | Approximate_num_type of approx_num_type

(* num_value_exp *)
and num_value_exp =
    Num_term of term
  | Num_plus of num_value_exp * term
  | Num_minus of num_value_exp * term

(* num_value_fct *)
and num_value_fct =
    Fct_extract of (extract_field * extract_source)
  | Fct_position of (char_value_exp * char_value_exp)
  | Fct_length of length_exp

and object_column = column_name

(* object name *)
and object_name =
    Object_table of table_name
  | Object_domain of domain_name
  | Object_collation of collation_name
(* A VOIR  | Object_character_set of char_set_name*)
  | Object_translation of translation_name

and order_by_clause = sort_spec_list 

(* ordering_spec *)
and ordering_spec =
    Asc 
  | Desc

and pattern = char_value_exp

(*[Ty] The type for precisions. [Ty]*)
and precision = int

(* predicate *)
and predicate =
  | Predicate_between of row_value_cons * row_value_cons * row_value_cons
  | Predicate_not_between of row_value_cons * row_value_cons * row_value_cons
  | Predicate_exists of table_subquery
  | Predicate_in of row_value_cons * in_predicate_value
  | Predicate_not_in of row_value_cons * in_predicate_value
  | Predicate_like of match_value * pattern * (escape_char option)
  | Predicate_not_like of match_value * pattern * (escape_char option)
  | Predicate_match of match_predicate
  | Predicate_is_null of row_value_cons
  | Predicate_is_not_null of row_value_cons
  | Predicate_overlaps of row_value_cons * row_value_cons
  | Predicate_comp of row_value_cons * comp_op * row_value_cons
  | Predicate_quantified_comp of row_value_cons * comp_op * quantifier * table_subquery
  | Predicate_unique of table_subquery

(* privilege column *)
and privilege_column = column_name

(* privileges *)
and privileges =
    All_privileges
  | Actions of action list

and qualified_id = id

and qualified_join = 
    table_ref
      * bool (* true = "natural" *)
      * (join_type option)
      * table_ref
      * (join_spec option)
      
and qualified_local_table_name = local_table_name

(*  qualified name *)
and qualified_name = (schema_name option) * qualified_id

(* qualifier *)
and qualifier =
    Qualifier_table of table_name
  | Qualifier_correlation of correlation_name

(* quantified_comp_predicate *)
and quantified_comp_predicate = row_value_cons * comp_op * quantifier * table_subquery

(* quantifier *)
and quantifier =
    Quantifier_all
  | Quantifier_some

(* query exp *)
and query_exp = 
  | Query_exp_non_join of non_join_query_exp
  | Query_exp_joined_table of joined_table

(* query primary *)
and query_primary = 
  | Query_prim_non_join of non_join_query_primary
  | Query_prim_joined_table of joined_table

(* query specification *)
and query_spec = (set_quantifier option) * select_list * table_exp

(* query term *)
and query_term = 
  | Query_term_non_join of non_join_query_term
  | Query_term_joined_table of joined_table

(* refd_table_and_columns *)
and refd_table_and_columns = table_name * ref_column_list

and referencing_columns = ref_column_list

(* refs_spec *)
and refs_spec = refd_table_and_columns * (match_type option) * (ref_triggered_action option)

(* ref action *)
and ref_action =
  | Action_cascade
  | Action_set_null
  | Action_set_default
  | Action_no_action

and ref_column_list = column_name list

(* ref_constraint_def *)
and ref_constraint_def = referencing_columns * refs_spec

(* ref_triggered_action *)
and ref_triggered_action = (update_rule option) * (delete_rule option)
    
and result_exp = value_exp

and row_subquery = subquery

(* row_value_cons *)
and row_value_cons =
    Row_value_elem of row_value_cons_elem
  | Row_value_const_list of row_value_const_list
  | Row_value_subquery of row_subquery

(* row_value_cons_elem *)
and row_value_cons_elem =
    Row_value_exp of value_exp
  | Row_value_null
  | Row_value_default

and row_value_const_list = row_value_cons_elem list

and scalar_subquery = subquery

(*[Ty] The type for scales. [Ty]*)
and scale = int

and schema_author_id = author_id

(* A VOIR and schema_char_set_name = char_set_name *)

and schema_collation_name = collation_name

and schema_def = string

(* schema name *)
and schema_name = (catalog_name option) * unqualified_schema_name

and schema_translation_name = translation_name

(* search condition *)
and search_condition = 
    Search_cond_boolean of boolean_term
  | Search_cond_or of search_condition * boolean_term

(* seconds_value *)
and seconds_value = float

(* select list *)
and select_list =
    Select_all
  | Select_list of select_sublist list

(* select_stmt_single_row *)
(* A VOIR and select_stmt_single_row = (set_quantifier option) * select_list * select_target_list * table_exp *)
(* A VOIR and select_target_list = target_spec list *)

(* select_sublist *)
and select_sublist =
    Select_column of derived_column
  | Select_qualifier of qualifier

(* set_clause *)
and set_clause = object_column * update_source

(* set clause list *)
and set_clause_list = set_clause list

and set_column_default_clause = default_clause

(* set_fct_spec *)
and set_fct_spec = 
    Set_fct_spec_count
  | Set_fct_spec_general of general_set_fct

(* set function type *)
and set_fct_type = 
    Set_fct_avg
  | Set_fct_max
  | Set_fct_min
  | Set_fct_sum
  | Set_fct_count

(* set quantifier *)
and set_quantifier = 
    Distinct
  | All

(* sign *)
and sign = 
    Sign_plus
  | Sign_minus

(* signed_num_lit *)
and signed_num_lit = (sign option) * unsigned_num_lit

(* simple_table *)
and simple_table =
    Simple_table_query of query_spec
  | Simple_table_value of table_value_constructor
  | Simple_table_explicit of explicit_table
	
(* single datetime field *)
and single_datetime_field =
    Single_datetime_non_second of datetime_field * (int option (* interval_leading_fieldprec*) )
  | Single_datetime_second of (int (* interval_leading_fieldprec *) * (int option (* interval_frac_seconds_prec *))) option
	
(* sort_key *)
and sort_key =
  Sort_key_column of column_name
  | Sort_key_int of int

(* sort_spec *)
and sort_spec = sort_key * (collate_clause option) * (ordering_spec option)

and sort_spec_list = sort_spec list

(* sql connection statement *)
and sql_connection_stmt = string (* A VOIR *)

(* sql_data_change_stmt *)
and sql_data_change_stmt =
    (* A VOIR Delete_stmt_pos *)
  | Delete_stmt_searched of delete_stmt_searched 
  | Insert_stmt of (table_name * insert_columns_and_source)
  (* A VOIR | Update_stmt_pos *)
  | Update_stmt_searched of update_stmt_searched 

(* schema definition statement *)
and sql_schema_def_stmt =
  | Schema_def of schema_def
  | Table_def of table_def 
  | View_def of view_def 
  | Grant of grant_stmt 
  | Domain_def of domain_def 
(* A VOIR   | Char_set_def of char_set_def *)
  | Collation_def of collation_def 
(* A VOIR   | Translation_def of translation_def *)
(* A VOIR | Assertion_def of assertion_def *)

(* schema manipulation statement *)
and sql_schema_manipulat_stmt =
    Alter_domain_stmt of domain_name * alter_domain_action
  | Alter_table_stmt of table_name * alter_table_action
  | Drop_assertion_stmt of constraint_name
  | Drop_translation_stmt of translation_name
  | Drop_collation_stmt of collation_name
  | Drop_domain_stmt of domain_name * drop_behavior
  | Drop_schema_stmt of schema_name * drop_behavior
  | Drop_table_stmt of table_name * drop_behavior
  | Drop_view_stmt of table_name * drop_behavior
  | Revoke_stmt of bool (* true = grant option *) * privileges * object_name * grantee list * drop_behavior
(* A VOIR  | Drop_char_set_stmt of char_set_name*)

(* sql schema statement *)
and sql_schema_stmt = 
  | Schema_def_stmt of sql_schema_def_stmt
  | Schema_manipulat_stmt of sql_schema_manipulat_stmt

(* sql session statement *)
and sql_session_stmt = string (* A VOIR *)

(* sql transaction statement *)
and sql_transaction_stmt = string (* A VOIR *)

(* start_field *)
and start_field = datetime_field * (int option (*interval_leading_fieldprec*) )

(* start position *)
and start_position = num_value_exp

and std_collation_name = collation_name

and std_translation_name = translation_name

(* string length *)
and string_length = num_value_exp

(* string value exp *)
and string_value_exp = 
  | String_value_char of char_value_exp
(* A VOIR  | String_value_bit of bit_value_exp *)

(* string value fct *)
and string_value_fct = 
  | String_fct_char of char_value_fct
(* A VOR  | String_fct_bit of bit_value_fct *)

and subquery = query_exp

(* table constraint *)
and table_constraint =
  | Table_unique_constraint of unique_constraint_def
  | Table_ref_constraint of ref_constraint_def
  | Table_check_constraint of check_constraint

(* table_constraint_def *)
and table_constraint_def = (constraint_name_def option) * table_constraint * constraint_attribute list

(* table definition *)
and table_def = {
  tab_name : table_name ;
  tab_temporary : temporary_scope option;
  tab_elements : table_element_list ;
  tab_on_commit_action : on_commit_action option
} 

(* temporary_scope *)
and temporary_scope =
    Global
  | Local

(* on commit action *)
and on_commit_action =
    Delete_rows
  | Preserve_rows

(* table element *)
and table_element =
    Column_def of column_def
  | Table_constraint_def of table_constraint_def

and table_element_list = table_element list

(* table exp *)
and table_exp = from_clause * (where_clause option) * (group_by_clause option) * (having_clause option)

(* table name *)
and table_name =
    Qualified_table_name of qualified_name
  | Qualified_local_table_name of qualified_local_table_name

(* table reference *)
and table_ref = 
  | Table_ref_name of table_name * ((correlation_name * derived_column_list) option)
  | Table_ref_derived of derived_table * correlation_name * derived_column_list
  | Table_ref_joined of joined_table

(* table subquery *)
and table_subquery = subquery

and table_value_constructor = table_value_const_list

and table_value_const_list = row_value_cons list

(* A VOIR and target_spec = string *)

(* temporary table declaration *)
and temporary_table_decl = string (* A VOIR *)

(* term *)
and term = 
    Factor of factor
  | Mult of term * factor
  | Div of term * factor

and timestamp_lit = timestamp_string

(*[Ty] The type for timestamp precision. [Ty]*)
and timestamp_precision = int

and timestamp_string = string (* A VOIR *)

(* time literal *)
and time_lit = time_string

(*[Ty] The type for time precision. [Ty]*)
and time_precision = int

(* time string *)
and time_string = time_value * (time_zone_interval option)

(* time value *)
and time_value = {
    time_hour : int;
    time_minute : int;
    time_second : seconds_value
  } 

and time_zone = time_zone_specifier

(* time zone field *)
and time_zone_field =
    Time_zone_hour
  | Time_zone_minute

and time_zone_interval = string (* A VOIR *)

(* time zone specifier *)
and time_zone_specifier =
  | Local_time_zone
  | Interval_time_zone of interval_value_exp

(* translation_name *)
and translation_name = qualified_name

and trim_char = char_value_exp

(* trim operands *)
and trim_operands = (trim_spec option) * (trim_char option) * trim_source

and trim_source = char_value_exp

(* trim specification *)
and trim_spec = 
    Trim_leading
  | Trim_trailing
  | Trim_both

(* truth value *)
and truth_value = 
    True
  | False
  | Unknown

and unique_column_list = column_name_list 

(* unique_constraint_def *)
and unique_constraint_def = unique_spec * unique_column_list

(* unique_spec *)
and unique_spec =
  | Unique
  | Primary_key

and unqualified_schema_name = id

(* unsigned_lit *)
and unsigned_lit = 
    Unsigned_num_lit of unsigned_num_lit
  | Unsigned_general_lit of general_lit

(* unsigned_num_lit *)
and unsigned_num_lit = 
    Exact_num_lit of exact_num_lit
  | Approximate_num_lit of approximate_num_lit

(* unsigned_value_spec *)
and unsigned_value_spec =
    Unsigned_lit of unsigned_lit
  | General_value of general_value_spec

and update_rule = ref_action

(* update_source *)
and update_source = 
    Update_value of value_exp
  | Update_null
  | Update_default

(* update_stmt_searched *)
and update_stmt_searched = table_name * set_clause_list * search_condition option

(* value exp *)
and value_exp = 
    Value_exp_num of num_value_exp
  | Value_exp_string of string_value_exp
  | Value_exp_datetime of datetime_value_exp
  | Value_exp_interval of interval_value_exp

(* value exp primary *)
and value_exp_primary = 
    Unsigned_value_exp of unsigned_value_spec
  | Column_ref of column_ref
  | Set_fct_spec of set_fct_spec
  | Scalar_subquery of scalar_subquery
(* A VOIR Case_exp of case_exp *)
  | Value_exp of value_exp
  | Cast_spec of cast_spec 

(* value_spec *)
and value_spec =
    Lit of lit
  | General_value_spec of general_value_spec

and view_column_list = column_name_list

(* view definition *)
and view_def = {
    view_name : table_name;
    view_columns : view_column_list;
    view_query : query_exp;
    view_levels : levels_clause option;
    view_with_check_option : bool
  } 

and where_clause = search_condition











