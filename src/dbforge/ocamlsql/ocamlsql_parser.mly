/**************************************************************************/
/*                   Cameleon                                             */
/*                                                                        */
/*      Copyright (C) 2002 Institut National de Recherche en Informatique et   */
/*      en Automatique. All rights reserved.                              */
/*                                                                        */
/*      This program is free software; you can redistribute it and/or modify  */
/*      it under the terms of the GNU General Public License as published by  */
/*      the Free Software Foundation; either version 2 of the License, or  */
/*      any later version.                                                */
/*                                                                        */
/*      This program is distributed in the hope that it will be useful,   */
/*      but WITHOUT ANY WARRANTY; without even the implied warranty of    */
/*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the     */
/*      GNU General Public License for more details.                      */
/*                                                                        */
/*      You should have received a copy of the GNU General Public License  */
/*      along with this program; if not, write to the Free Software       */
/*      Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA          */
/*      02111-1307  USA                                                   */
/*                                                                        */
/*      Contact: Maxence.Guesdon@inria.fr                                */
/**************************************************************************/

/*
(***********************************************************************)
(*                             DBForge                                 *)
(*                                                                     *)
(*            Maxence Guesdon, projet Cristal, INRIA Rocquencourt      *)
(*                                                                     *)
(*  Copyright 2001 Institut National de Recherche en Informatique et   *)
(*  en Automatique.  All rights reserved.  This file is distributed    *)
(*  under the terms of the GNU General Public License version 2.       *)
(*                                                                     *)
(***********************************************************************)

(*[Mo] The parser for SQL statements. [Mo]*)

*/


%{
open Ocamlsql_types

%}

/* Token */
%token <string> IDENT
%token <int> INT
%token <float> SIMPLE_FLOAT
%token <float * int> APPROXIMATE_NUM
%token <string> STRING
%token <string> DELIMITED_ID

/* Mots cles du langage SQL */
%token T_ACTION
%token T_ADD
%token T_ALL
%token T_ALTER
%token T_AND
%token T_ANY
%token T_AS
%token T_ASC
%token T_ASSERTION
%token T_AT
%token T_AVG

%token T_BETWEEN
%token T_BIT
%token T_BOTH
%token T_BY

%token T_CASCADE
%token T_CASCADED
%token T_CAST
%token T_CHAR 
%token T_CHAR_LENGTH
%token T_CHECK
%token T_COLLATE
%token T_COLLATION
%token T_COLUMN
%token T_COMMIT
%token T_CONSTRAINT
%token T_CONVERT
%token T_CORRESPONDING
%token T_COUNT
%token T_CREATE
%token T_CROSS
%token T_CURRENT_DATE
%token T_CURRENT_TIME
%token T_CURRENT_TIMESTAMP
%token T_CURRENT_USER

%token T_DATE
%token T_DAY
%token T_DECIMAL
%token T_DEFAULT
%token T_DEFERRABLE
%token T_DEFERRED
%token T_DELETE
%token T_DESC
%token T_DISTINCT
%token T_DOMAIN
%token T_DOUBLE
%token T_DROP

%token T_ESCAPE
%token T_EXCEPT
%token T_EXISTS
%token T_EXTERNAL
%token T_EXTRACT

%token T_FALSE
%token T_FLOAT
%token T_FOR
%token T_FOREIGN
%token T_FROM
%token T_FULL

%token T_GLOBAL
%token T_GRANT
%token T_GROUP

%token T_HAVING
%token T_HOUR

%token T_IMMEDIATE
%token T_IN
%token T_INITIALLY
%token T_INNER
%token T_INSERT
%token T_INTEGER
%token T_INTERSECT
%token T_INTERVAL
%token T_INTO
%token T_IS

%token T_JOIN

%token T_KEY

%token T_LEADING
%token T_LEFT
%token T_LIKE
%token T_LOCAL
%token T_LOWER

%token T_MATCH
%token T_MAX
%token T_MIN
%token T_MINUTE
%token T_MODULE
%token T_MONTH

%token T_NATIONAL
%token T_NATURAL
%token T_NCHAR
%token T_NO
%token T_NOT
%token T_NULL
%token T_NUMERIC

%token T_OCTET_LENGTH
%token T_ON
%token T_OPTION
%token T_OR
%token T_ORDER
%token T_OUTER
%token T_OVERLAPS

%token T_PARTIAL
%token T_POSITION
%token T_PRESERVE
%token T_PRIMARY
%token T_PRIVILEGES
%token T_PUBLIC

%token T_REAL
%token T_REFERENCES
%token T_RESTRICT
%token T_REVOKE
%token T_RIGHT
%token T_ROWS

%token T_SCHEMA
%token T_SECOND
%token T_SELECT
%token T_SESSION_USER
%token T_SET
%token T_SMALLINT
%token T_SOME
%token T_SUBSTRING
%token T_SUM
%token T_SYSTEM_USER

%token T_TABLE
%token T_TEMPORARY
%token T_TIME
%token T_TIMESTAMP
%token T_TIMEZONE_HOUR
%token T_TIMEZONE_MINUTE
%token T_TO
%token T_TRAILING
%token T_TRANSLATE
%token T_TRANSLATION
%token T_TRIM
%token T_TRUE

%token T_UNKNOWN
%token T_UNION
%token T_UNIQUE
%token T_UPDATE
%token T_UPPER
%token T_USAGE
%token T_USER
%token T_USING

%token T_VALUE
%token T_VALUES
%token T_VARCHAR
%token T_VARYING
%token T_VIEW

%token T_WHERE
%token T_WITH

%token T_YEAR

%token T_ZONE

%token EOF

/* Parenthesis */
%token LPAR
%token RPAR

/* brackets */
%token LBRA
%token RBRA

/* other symbols */
%token T_COMMA
%token T_AMPERSAND
%token T_ASTERISK
%token T_DBQUOTE
%token T_QUOTE
%token T_COLON
%token T_DOT
%token T_EQUALS
%token T_LESS
%token T_GREATER
%token T_BANG
%token T_PLUS
%token T_MINUS
%token T_SEMICOLON
%token T_DIV
%token T_UNDERSCORE
%token T_AROBAS
%token T_PERCENT

/* Start Symbols */
%start main column_def
%type <Ocamlsql_types.direct_sql_stmt>   main
%type <Ocamlsql_types.column_def> column_def

%%
main:
    direct_sql_stmt      { $1 }
| EOF { raise End_of_file }
;

/* action */
action:
  T_SELECT { Action_select }
| T_DELETE { Action_delete }
| T_INSERT { Action_insert [] }
| T_INSERT LPAR privilege_column_list RPAR { Action_insert $3 }
| T_UPDATE { Action_update [] }
| T_UPDATE LPAR privilege_column_list RPAR { Action_update $3 }
| T_REFERENCES { Action_references [] }
| T_REFERENCES LPAR privilege_column_list RPAR { Action_references $3 }
| T_USAGE { Action_usage }
;

/* action list */
action_list:
    action { [ $1 ] }
| action T_COMMA action_list { $1 :: $3 }
;

/* actual id A VOIR */

/* ada_... A VOIR */

/* add_column_def of type alter_table_action */
add_column_def:
    T_ADD T_COLUMN column_def { Add_column $3 }
| T_ADD column_def { Add_column $2 }
;

/* add_domain_constraint_def */
add_domain_constraint_def:
    T_ADD domain_constraint { Add_domain_constraint $2 }
;

/* add_table_constraint_def */
add_table_constraint_def:
    T_ADD table_constraint_def { Add_table_constraint $2 }
;


/* allocate_... A VOIR */

/* alter_column_action */
alter_column_action:
    set_column_default_clause { Set_column_default_clause $1}
| drop_column_default_clause { Drop_column_default_clause }
;

/* alter_column_def of type alter_table_action */
alter_column_def:
      T_ALTER T_COLUMN column_name alter_column_action { Alter_column ($3, $4) }
  | T_ALTER column_name alter_column_action { Alter_column ($2, $3) }
;

/* alter_domain_action */
alter_domain_action :
    set_domain_default_clause { $1 }
| drop_domain_default_clause { $1 }
| add_domain_constraint_def { $1 }
| drop_domain_constraint_def { $1 }
;

/* alter_domain_stmt */
alter_domain_stmt:
      T_ALTER T_DOMAIN domain_name alter_domain_action { Alter_domain_stmt ($3, $4) }
;

/* alter_table_action */
alter_table_action:
    add_column_def { $1 }
| alter_column_def { $1 }
| drop_column_def { $1 }
| add_table_constraint_def { $1 }
| drop_table_constraint_def { $1 }
;

/* alter_table_stmt */
alter_table_stmt:
    T_ALTER T_TABLE table_name alter_table_action { Alter_table_stmt ($3, $4) }
;

/* approximate_num_lit of type approximate_num_lit */
approximate_num_lit: APPROXIMATE_NUM { $1 }
;

/* approximate_num_type */
approximate_num_type:
    T_FLOAT { Float None }
| T_FLOAT LPAR INT RPAR { Float (Some $3) }
| T_FLOAT LPAR INT T_COMMA INT RPAR { Float (Some $3) (* A VOIR : it's for MySQL *)}
| T_REAL { Real }
| T_DOUBLE INT { Double $2 }
;

/* arc... A VOIR */

/* argument A VOIR (page) */

/* assertion_check */
/* A VOIR : not used
assertion_check:
    T_CHECK LPAR search_condition RPAR { $3 (* of type search_condition *) }
;
*/

/* as_clause of type column_name */
as_clause:
    column_name { $1 }
|  T_AS column_name { $2 }
;

/* author_id (added, not in the bnf) of type author_id = id */
author_id: id { $1 }
;

/* between_predicate */
between_predicate:
      row_value_constructor T_NOT T_BETWEEN row_value_constructor T_AND row_value_constructor 
      { Predicate_not_between ($1, $4, $6) }
  | row_value_constructor T_BETWEEN row_value_constructor T_AND row_value_constructor 
      { Predicate_between ($1, $3, $5) }
;

bit_string_type:
  T_BIT { Bit None }
| T_BIT LPAR INT RPAR { Bit (Some $3) }
| T_BIT T_VARYING LPAR INT RPAR { VarBit $4 }
;

/* bit... A VOIR */

/* boolean_factor */
boolean_factor:
    T_NOT boolean_test { Not_boolean_test $2 }
| boolean_test { Boolean_test $1 }
;

/* boolean_primary */
boolean_primary:
    predicate { Boolean_predicate $1 }
| LPAR search_condition RPAR { Boolean_search_condition $2 }
;

/* boolean_term */
boolean_term:
    boolean_factor { Boolean_factor $1 }
| boolean_term T_AND boolean_factor { Boolean_and ($1, $3) }
;

/* boolean_test */
boolean_test:
    boolean_primary { Boolean_primary $1 }
| boolean_primary T_IS truth_value { Boolean_primary_is ($1, $3) }
| boolean_primary T_IS T_NOT truth_value { Boolean_primary_is_not ($1, $4) }
;

/* case_... A VOIR */

/* cast_operand */
cast_operand:
    value_exp { Cast_op_value $1 }
| T_NULL { Cast_op_null }
;

/* cast_spec */
cast_spec:
    T_CAST LPAR cast_operand T_AS cast_target RPAR { ($3, $5) }
;

/* cast_target */
cast_target:
    domain_name { Cast_domain $1 }
| data_type { Cast_data_type $1 }
;

/* catalog_name */
/*A VOIR : not used
catalog_name:
    id { $1 }
;
*/

/* char_factor */
char_factor:
    char_primary { ($1, None) }
| char_primary collate_clause { ($1, Some $2) }
;

/* char_length_exp */
char_length_exp:
    T_CHAR_LENGTH LPAR string_value_exp RPAR { Char_length_exp $3 }
;

/* char_primary */
char_primary:
    value_exp_primary { Char_primary_value_exp $1 }
| string_value_fct { Char_primary_string_value_fct $1 }
;

/* char_representation A VOIR (avec le lexeur) */

/* char_set_... A VOIR */

/* char_string_lit A compléter quand il y aura les char_set_... et voir si on fait une liste ?*/
char_string_lit:
/*    [ introducer char_set_spec ] 
    "'" { char_representation } "'" 
    { { separator } "'" { char_representation } "'" } 
  T_QUOTE char_representation T_QUOTE { $2 }
*/
  STRING { $1 }
;

/* char_string_type */
char_string_type:
  T_CHAR { Char None } 
| T_CHAR LPAR INT RPAR { Char (Some $3) }
| T_CHAR T_VARYING LPAR INT RPAR { VarChar $4 }
| T_VARCHAR LPAR INT RPAR { VarChar $3 }
;

/* char_substring_fct of type char_value_fct */
char_substring_fct:
      T_SUBSTRING LPAR char_value_exp T_FROM start_position RPAR
      { Char_substring_fct ($3, $5, None) }
  |   T_SUBSTRING LPAR char_value_exp T_FROM start_position T_FOR string_length RPAR
      { Char_substring_fct ($3, $5, Some $7) }
;

/* char_translation of type char_value_fct */
char_translation:
      T_TRANSLATE LPAR char_value_exp T_USING translation_name RPAR
      { Char_translation ($3, $5) }
;

/* char_value_exp of typr char_value_exp */
char_value_exp:
    concatenation { Concatenation $1 }
  | char_factor { Char_factor $1 }
;

/* char_value_fct of type char_value_fct */
char_value_fct:
      char_substring_fct { $1 }
  | fold { $1 }
  | form_conversion  { $1 }
  | char_translation { $1 }
  | trim_fct { $1 }
;

/* check_constraint_def of type search_condition */
check_constraint_def:
      T_CHECK LPAR search_condition RPAR { $3 }
;

/* close_stmt A VOIR */

/* cobol_... A VOIR */

/* collate_clause of type collation_name */
collate_clause:
      T_COLLATE collation_name { $2 }
;

/* collat... A VOIR */

/* collation_name */
collation_name:
      qualified_name { $1 }
;

/* column_constraint of type column_constraint */
column_constraint:
      T_NOT T_NULL { Column_not_null }
  | unique_spec { Column_unique $1 }
  | refs_spec { Column_refs $1 }
  | check_constraint_def { Column_check_constraint $1 }
;

/* column_constraint_def of type column_constraint_def */
column_constraint_def:
      constraint_name_def column_constraint { (Some $1, $2, []) }
  | constraint_name_def column_constraint constraint_attributes { (Some $1, $2, $3) }
  | column_constraint constraint_attributes { (None, $1, $2) }
  | column_constraint { (None, $1, []) }
;

/* column_constraint_def_list of type column_constraint_def list */
column_constraint_def_list:
      column_constraint_def { [$1] }
  | column_constraint_def column_constraint_def_list { $1 :: $2 }
;

/* column_def of type column_def */
column_def:
      column_name column_type
      { { col_name = $1;
	  col_type = $2;
	  col_default_clause = None;
	  col_constaints = [];
	  col_collate_clause = None 
	}
      }	

  | column_name column_type default_clause
      { { col_name = $1;
	  col_type = $2;
	  col_default_clause = Some $3;
	  col_constaints = [];
	  col_collate_clause = None
	}
      }	

  | column_name column_type column_constraint_def_list
      { { col_name = $1;
	  col_type = $2;
	  col_default_clause = None;
	  col_constaints = $3;
	  col_collate_clause = None 
	}
      }	

  | column_name column_type collate_clause
      { { col_name = $1;
	  col_type = $2;
	  col_default_clause = None;
	  col_constaints = [];
	  col_collate_clause = Some $3 
	}
      }

  | column_name column_type default_clause column_constraint_def_list
      { { col_name = $1;
	  col_type = $2;
	  col_default_clause = Some $3;
	  col_constaints = $4;
	  col_collate_clause = None 
	}
      }	

  | column_name column_type column_constraint_def_list collate_clause
      { { col_name = $1;
	  col_type = $2;
	  col_default_clause = None;
	  col_constaints = $3;
	  col_collate_clause = Some $4 
	}
      }	

  | column_name column_type default_clause collate_clause
      { { col_name = $1;
	  col_type = $2;
	  col_default_clause = Some $3;
	  col_constaints = [];
	  col_collate_clause = Some $4 
	}
      }
;

/* column_type */
column_type:
      data_type { Data_type $1 }
  | domain_name { Domain $1 }
;

/* column_name of type column_name = id */
column_name:
      id { $1 }
/* A VOIR : we could allow quoted strings as column names, be it would conflic with string values
  | STRING { $1 }
*/
;

/* column_name_list of type column_name list */
column_name_list:
      column_name { [ $1 ] }
  | column_name T_COMMA column_name_list { $1 :: $3 }
;

/* column_ref of type column_ref */
column_ref:
      column_name { (None, $1) }
  | qualifier T_DOT column_name { (Some $1, $3) }
;

/* comment_... A VOIR */

/* commit_... A VOIR  */

/* comp_op */
comp_op:
      T_EQUALS { Equals }
  | not_equals_op { Not_equals }
  | T_LESS { Strict_less }
  | T_GREATER { Strict_greater }
  | less_than_or_equals_op { Less_or_equals }
  | greater_than_or_equals_op { Greater_or_equals }
;

not_equals_op:
      T_BANG T_EQUALS { () }
  | T_LESS T_GREATER { () }
;

less_than_or_equals_op:
      T_LESS T_EQUALS { () }
  | T_EQUALS T_LESS { () }
;

greater_than_or_equals_op:
      T_GREATER T_EQUALS { () }
  | T_EQUALS T_GREATER { () }
;

/* comp_predicate of type comp_predicate */
comp_predicate:
      row_value_constructor comp_op row_value_constructor { Predicate_comp ($1, $2, $3) }
;

/* concatenation */
concatenation:
      char_value_exp concatenation_op char_factor { ($1, $3) }
;

concatenation_op: T_AROBAS { () }/* A VOIR */

/* condition... A VOIR */

/* connection... A VOIR */

/* connect_stmt A VOIR */

/* constraint_attributes of type constraint_attribute list */
constraint_attributes:
      constraint_check_time { [ $1] }
  | T_DEFERRABLE { [ Deferrrable] }
  | T_NOT T_DEFERRABLE { [Not_deferrable] }
  | constraint_check_time T_DEFERRABLE { [ $1 ; Deferrrable ] }
  | constraint_check_time T_NOT T_DEFERRABLE { [ $1 ; Not_deferrable ] }
  | T_DEFERRABLE constraint_check_time { [ $2 ; Deferrrable ] }
  | T_NOT T_DEFERRABLE constraint_check_time { [ $3 ; Not_deferrable ] }
;

/* constraint_check_time of type constraint_attribute */
constraint_check_time:
      T_INITIALLY T_DEFERRED { Initially_deferred }
  | T_INITIALLY T_IMMEDIATE { Initially_immediate }
;

/* constraint_name of type constraint_name = qualified_name */
constraint_name: qualified_name { $1 }
;

/* constraint_name_def of type constraint_name */
constraint_name_def: T_CONSTRAINT constraint_name { $2 }
;

/* constraint_name_list of type constraint_name list */
/* A VOIR : not used
constraint_name_list:
      constraint_name { [ $1 ] }
  | constraint_name T_COMMA constraint_name_list { $1 :: $3 }
;
*/

/* correlation_name of type correlation_name = id */
correlation_name: id { $1 }
;

/* corresponding_column_list */
corresponding_column_list: column_name_list { $1 }
;

/* corresponding_spec of type column_name list */
corresponding_spec:
      T_CORRESPONDING { [] }
  | T_CORRESPONDING T_BY LPAR corresponding_column_list RPAR { $4 } 
;

/* cross join of (table_ref * table_ref) */
cross_join:
      table_ref T_CROSS T_JOIN table_ref { ($1, $4) }
;

/* currenttimestamp_value_fct of type datetime_value_fct */
currenttimestamp_value_fct:
      T_CURRENT_TIMESTAMP { Current_timestamp None }
  | T_CURRENT_TIMESTAMP LPAR timestamp_precision RPAR { Current_timestamp (Some $3) }
;

/* current_date_value_fct of type datetime_value_fct */
current_date_value_fct: T_CURRENT_DATE { Current_date }
;

/* current_time_value_fct of type datetime_value_fct */
current_time_value_fct: 
      T_CURRENT_TIME { Current_time None }
  | T_CURRENT_TIME LPAR time_precision RPAR { Current_time (Some $3) }
;

/* cursor_... A VOIR */

/* c_... A VOIR */

/* data_type */
data_type:
    char_string_type          { $1 }
  | national_char_string_type  { $1 }
  | bit_string_type { $1 }
  | num_type { $1 }
  | datetime_type { $1 }
  | interval_type { $1 }
;

/* datetime_factor of type datetime_factor */
datetime_factor:
      datetime_primary { ($1, None) }
  | datetime_primary time_zone { ($1, Some $2) }
;

/* datetime_field of type datetime_field */
datetime_field:
      non_second_datetime_field { $1 }
  | T_SECOND { Datetime_field_second }
;

/* datetime_lit */
datetime_lit:
    date_lit { Date_lit $1 }
| time_lit { Time_lit $1 }
| timestamp_lit { Timestamp_lit $1 }
;

/* datetime_primary of type datetime_primary */
datetime_primary:
    value_exp_primary { Datetime_value_exp $1 }
| datetime_value_fct { Datetime_value_fct $1 }
;

/* datetime_term of type datetime_term = datetime_factor */
datetime_term: datetime_factor { $1 }
;

/* datetime_type of type data_type */
datetime_type:
    T_DATE { Date }

| T_TIME { Time (None, false) }
| T_TIME LPAR INT RPAR { Time (Some $3, false) }
| T_TIME T_WITH T_TIME T_ZONE { Time (None, true) }
| T_TIME LPAR INT RPAR T_WITH T_TIME T_ZONE { Time (Some $3, true) }

| T_TIMESTAMP { Timestamp (None, false) }
| T_TIMESTAMP LPAR INT RPAR { Timestamp (Some $3, false) }
| T_TIMESTAMP T_WITH T_TIME T_ZONE { Timestamp (None, true) }
| T_TIMESTAMP LPAR INT RPAR T_WITH T_TIME T_ZONE { Timestamp (Some $3, true) }
;

/* datetime_value_exp of type datetime_value_exp */
datetime_value_exp:
      datetime_term { Datetime_term $1 }
  | interval_value_exp T_PLUS datetime_term { Datetime_interval_value_plus_term ($1, $3) }
  | datetime_value_exp T_PLUS interval_term { Datetime_value_plus_interval_term ($1, $3) }
  | datetime_value_exp T_MINUS interval_term { Datetime_value_minus_interval_term ($1, $3) }
;

/* datetime_value_fct of type datetime_value_fct */
datetime_value_fct:
      current_date_value_fct { $1 }
  | current_time_value_fct { $1 }
  | currenttimestamp_value_fct { $1 }
;

/* date_lit of type date_value */
date_lit:
      T_DATE date_string { $2 }
;

/* date_string of type date_value */
date_string:
      T_QUOTE date_value T_QUOTE { $2 }
;

/* date_value */
date_value:
      INT T_MINUS INT T_MINUS INT { { date_year = $1;
				      date_month = $3;
				      date_day = $5
				    } 
				  } 
;

/* deallocate_... A VOIR */

/* declare_cursor A VOIR */

/* default_clause of type default_clause */
default_clause:
      T_DEFAULT default_option { $2 }
;

/* default_option of type default_clause */
default_option:
      lit { Default_lit $1 }
  | datetime_value_fct { Default_datetime_value_fct $1 }
  | T_USER { Default_user }
  | T_CURRENT_USER { Default_current_user }
  | T_SESSION_USER { Default_session_user }
  | T_SYSTEM_USER { Default_system_user }
  | T_NULL { Default_null }
;

/* default_spec */
default_spec:
      T_DEFAULT { () }
;

/* delete_rule */
delete_rule:
       T_ON T_DELETE ref_action { $3 }
;

/* delete_stmt_pos A VOIR (utilisation de cursor_name ) */

/* delete_stmt_searched of type delete_stmt_searched */
delete_stmt_searched:
      T_DELETE T_FROM table_name { ($3, None) }
  | T_DELETE T_FROM table_name T_WHERE search_condition { ($3, Some $5) }
;

/* delimited_id of type string */
/* A VOIR : not used 
delimited_id:
      DELIMITED_ID { $1 }
;
*/

/* derived_column of type derived_column */
derived_column:
      value_exp { ($1, None) }
  | value_exp as_clause { ($1, Some $2) }
;

/* derived_column_list of type column_name list */
derived_column_list:
      column_name_list { $1 }
;

/* derived_table of type derived_table = table_subquery */
derived_table:
      table_subquery { $1 }
;

/* describe_... A VOIR */

/* descriptor... A VOIR */

/* diag_size A VOIR */

/* directly_exec_stmt */
directly_exec_stmt:
      direct_sql_data_stmt { Direct_sql_data_stmt $1 }
  | sql_schema_stmt { Sql_schema_stmt $1 }
/* A VOIR  | sql_transaction_stmt { Sql_transaction_stmt $1 } */
/* A VOIR  | sql_connection_stmt { Sql_connection_stmt $1 } */
/* A VOIR  | sql_session_stmt { Sql_session_stmt $1 } */
 /* A VOIR  | direct_implt_def_stmt { Direct_implt_def_stmt $1 } */
;

/* direct_implt_def_stmt A VOIR */

/* direct_select_stmt_n_rows of type direct_select_stmt_n_rows */
direct_select_stmt_n_rows:
      query_exp { ($1, None) }
  | query_exp order_by_clause { ($1, Some $2) }
;

/* direct_sql_data_stmt of type direct_sql_data_stmt */
direct_sql_data_stmt:
      delete_stmt_searched { Direct_delete_stmt_searched $1 }
  | insert_stmt { Direct_insert_stmt $1 }
  | update_stmt_searched { Direct_update_stmt_searched $1 }
/* A VOIR  | temporary_table_decl { Direct_temporary_table_decl $1 } */
  | direct_select_stmt_n_rows { Direct_select_stmt_n_rows $1 }
;

/* direct_sql_stmt of type direct_sql_stmt = directly_exec_stmt */
direct_sql_stmt:
      directly_exec_stmt T_SEMICOLON { $1 }
;

/* disconnect_... A VOIR */

/* domain_constraint of type domain_constraint */
domain_constraint:
      constraint_name_def check_constraint_def { (Some $1, $2, []) }
  | constraint_name_def check_constraint_def constraint_attributes { (Some $1, $2, $3) }
  | check_constraint_def constraint_attributes { (None, $1, $2) }
  | check_constraint_def { (None, $1, []) }
;

/* of type domain_constraint list */
domain_constraint_list:
      { [] }
  | domain_constraint domain_constraint_list { $1 :: $2 }
;

/* domain_def of type domain_def */
domain_def:
      T_CREATE T_DOMAIN domain_name as_data_type default_clause_opt domain_constraint_list collate_clause_opt
      {
	{
	  dom_name = $3;
	  dom_data_type = $4;
	  dom_default_clause = $5;
	  dom_constaints = $6;
	  dom_collate_clause = $7
	} 
      }	

/* of type data_type */
as_data_type:
      data_type { $1 }
  | T_AS data_type { $2 }
;

/* of type default_close opt */
default_clause_opt:
      { None }
  | default_clause { Some $1 }
;

/* of type collation_name opt */
collate_clause_opt:
      { None }
  | collate_clause { Some $1 }
;

/* domain_name of type domain_name = qualified_name */
domain_name: qualified_name { $1 }
;

/* drop_assertion_stmt of type sql_schema_manipulat_stmt */
drop_assertion_stmt:
      T_DROP T_ASSERTION constraint_name { Drop_assertion_stmt $3 }
;

/* drop_behavior of type drop_behavior */
drop_behavior:
      T_CASCADE { Cascade }
  | T_RESTRICT { Restrict }
;

/* drop_char_set_stmt A VOIR (utilise char_set_name) */

/* drop_collation_stmt of type sql_schema_manipulat_stmt */
drop_collation_stmt:
      T_DROP T_COLLATION collation_name { Drop_collation_stmt $3 }
;

/* drop_column_def of type alter_table_action */
drop_column_def:
  | T_DROP column_name { Drop_column ($2, Cascade) }
  | T_DROP T_COLUMN column_name { Drop_column ($3, Cascade) }
  | T_DROP column_name drop_behavior { Drop_column ($2, $3) }
  | T_DROP T_COLUMN column_name drop_behavior { Drop_column ($3, $4) }
;

/* drop_column_default_clause */
drop_column_default_clause:
      T_DROP T_DEFAULT { () }
;

/* drop_domain_constraint_def of type alter_domain_action */
drop_domain_constraint_def:
      T_DROP  T_CONSTRAINT constraint_name { Drop_domain_constraint $3 }
;

/* drop_domain_default_clause of type alter_domain_action */
drop_domain_default_clause:
      T_DROP T_DEFAULT { Drop_domain_default_clause } 
;

/* drop_domain_stmt of type sql_schema_manipulat_stmt */
drop_domain_stmt:
      T_DROP T_DOMAIN domain_name { Drop_domain_stmt ($3, Cascade) }
  | T_DROP T_DOMAIN domain_name drop_behavior { Drop_domain_stmt ($3, $4) }
;

/* drop_schema_stmt of type sql_schema_manipulat_stmt */
drop_schema_stmt:
      T_DROP T_SCHEMA schema_name drop_behavior { Drop_schema_stmt ($3, $4) }
;

/* drop_table_constraint_def of type alter_table_action */
drop_table_constraint_def:
      T_DROP T_CONSTRAINT constraint_name { Drop_table_constraint ($3, Cascade) }
  | T_DROP T_CONSTRAINT constraint_name drop_behavior { Drop_table_constraint ($3, $4) }
;

/* drop_table_stmt of type sql_schema_manipulat_stmt */
drop_table_stmt:
      T_DROP T_TABLE table_name { Drop_table_stmt ($3, Cascade) }
  | T_DROP T_TABLE table_name drop_behavior { Drop_table_stmt ($3, $4) }
;

/* drop_translation_stmt of type sql_schema_manipulat_stmt */
drop_translation_stmt:
      T_DROP T_TRANSLATION translation_name { Drop_translation_stmt $3 }
;

/* drop_view_stmt of type sql_schema_manipulat_stmt */
drop_view_stmt:
      T_DROP T_VIEW table_name { Drop_view_stmt ($3, Cascade) }
  | T_DROP T_VIEW table_name drop_behavior { Drop_view_stmt ($3, $4) }

/* dyn_... A VOIR */

/* e19... A VOIR */

/* else_clause A VOIR */

/* embdd A VOIR */

/* end field, of type end_field */
end_field:
      non_second_datetime_field { ($1, None) }
  | T_SECOND { (Datetime_field_second, None) }
  | T_SECOND LPAR INT RPAR { (Datetime_field_second, Some $3) }
;

/* escape_char of type escape_char = char_value_exp */
escape_char:
      char_value_exp { $1 }
;

/* exact_num_lit of type exact_num_lit */
exact_num_lit:
      INT { Exact_int $1 }
/*  | INT DOT { Exact_float (float_of_string ((string_of_int $1)^".0")) }
  | INT T_DOT INT { Exact_float (float_of_string ((string_of_int $1)^"."^(string_of_int $3))) }
  | T_DOT INT { Exact_float (float_of_string ("0."^(string_of_int $2))) }
*/
  | SIMPLE_FLOAT { Exact_float $1 }
;

exact_num_type:
    T_NUMERIC { Numeric None }
| T_NUMERIC LPAR INT RPAR { Numeric (Some ($3, None)) }
| T_NUMERIC LPAR INT T_COMMA INT RPAR { Numeric (Some ($3, Some $5)) }
| T_DECIMAL { Decimal None }
| T_DECIMAL LPAR INT RPAR { Decimal (Some ($3, None)) }
| T_DECIMAL LPAR INT T_COMMA INT RPAR { Decimal (Some ($3, Some $5)) }
| T_INTEGER { Integer }
| T_INTEGER LPAR INT RPAR { Numeric (Some ($3, None)) (* A VOIR : it's for MySQL *)}
| T_SMALLINT { SmallInteger }
;

/* execute... A VOIR */

/* existing_char_set_name A VOIR */

/* exists_predicate of type predicate */
exists_predicate:
      T_EXISTS table_subquery { Predicate_exists $2 }
;

/* explicit_table of type table_name */
explicit_table:
      T_TABLE table_name { $2 }
;

/* exponent : from the lexer to build approximate_num_lit  */

/* extended_cursor_name A VOIR */
/* extended_stmt_name A VOIR */

/* external_... A VOIR */

/* extract_exp of type (extract_field, extract_source) */
extract_exp:
      T_EXTRACT LPAR extract_field T_FROM extract_source RPAR { ($3, $5) }
;

/* extract_field of type extract_field */
extract_field:
      datetime_field { Extract_datetime $1 }
  | time_zone_field { Extract_time_zone $1 }
;

/* extract_source of type extract_source */
extract_source:
      datetime_value_exp { Extract_source_datetime $1 }
  | interval_value_exp { Extract_source_interval $1 }
;

/* factor of type factor */
factor:
      sign num_primary { (Some $1, $2) }
  | num_primary { (None, $1) }
;

/* fetch_... A VOIR */

/* fold of type char_value_fct */
fold:
      T_UPPER LPAR char_value_exp RPAR { Char_fold (Fold_upper, $3) }
  | T_LOWER LPAR char_value_exp RPAR { Char_fold (Fold_lower, $3) }
;

/* form_conversion of type char_value_fct */
form_conversion:
      T_CONVERT LPAR char_value_exp T_USING form_conversion_name RPAR
      { Char_form_conversion ($3, $5) }
;

/* form_conversion_name of type form_conversion_name = qualified_name */
form_conversion_name: qualified_name { $1 }
;

/* fortran_... A VOIR */

/* from_clause of type from_clause */
from_clause:
      T_FROM table_ref_list { $2 }
;

/* general_lit of type general_lit */
general_lit:
      char_string_lit { Char_string_lit $1 }
/* A VOIR  | national_char_string_lit { National_char_string_lit $1 } */
/* A VOIR  | bit_string_lit { Bit_string_lit $1 } */
/* A VOIR  | hex_string_lit { Hex_string_lit $1 } */
  | datetime_lit { Datetime_lit $1 }
  | interval_lit { Interval_lit $1 }
;

/* general_set_fct of type set_fct_spec */
general_set_fct:
      /*T_COUNT*/ set_fct_type LPAR T_ASTERISK RPAR 
      { 
	(* A VOIR : must do this rather than use T_COUNT, or else
	   T_COUNT cannot be used in ste_fct_type. Strange ? *)
	if $1 = Set_fct_count then
	  Set_fct_spec_count
	else
	  raise (Failure "must be COUNT(*)")
      }
  | set_fct_type LPAR value_exp RPAR { Set_fct_spec_general ($1, None, $3) }

/* A VOIR remplacer value_exp par une liste de value_exp ? (comme pour MySQL) */
  | set_fct_type LPAR set_quantifier value_exp RPAR { Set_fct_spec_general ($1, Some $3, $4) }
;

/* general_value_spec of type general_value_spec */
general_value_spec:
  | T_USER { Gen_val_user }
  | T_CURRENT_USER { Gen_val_current_user }
  | T_SESSION_USER { Gen_val_session_user }
  | T_SYSTEM_USER { Gen_val_system_user }
  | T_VALUE { Gen_val_value }
/* A VOIR      parameter_spec { Gen_val_paramter_spec $1 } */
/* A VOIR  | dyn_parameter_spec { Gen_val_dyn_paramter_spec $1 } */
/* A VOIR  | variable_spec { Gen_val_variable_spec $1 } */
;

/* get_... A VOIR */

/* goto... A VOIR */

/* grantee of type grantee */
grantee:
      T_PUBLIC { Grantee_public }
  | author_id { Grantee_author_id $1 }
;

/* grantee_list of type grantee list */
grantee_list:
      grantee { [$1] }
  | grantee T_COMMA grantee_list { $1 :: $3 }
;

/* grant_stmt of type grant_stmt */
grant_stmt:
      T_GRANT privileges T_ON object_name T_TO grantee_list 
      { 
	{
	  grant_privileges = $2;
	  grant_object = $4;
	  grant_grantees = $6;
	  grant_with_grant_option = false
	} 
      }	
  | T_GRANT privileges T_ON object_name T_TO grantee_list T_WITH T_GRANT T_OPTION
      { 
	{
	  grant_privileges = $2;
	  grant_object = $4;
	  grant_grantees = $6;
	  grant_with_grant_option = true
	} 
      }	
;

/* grouping_column_ref of type grouping_column_ref */
grouping_column_ref:
      column_ref { ($1, None) }
  | column_ref collate_clause { ($1, Some $2) }
;

/* grouping_column_ref_list of type grouping_column_ref list */
grouping_column_ref_list:
      grouping_column_ref { [$1] }
  |  grouping_column_ref T_COMMA grouping_column_ref_list { $1 :: $3 }
;

/* group_by_clause of type group_by_clause = grouping_column_ref list */
group_by_clause:
      T_GROUP T_BY grouping_column_ref_list { $3 }
;

/* having_clause of type having_clause = search_condition */
having_clause:
      T_HAVING search_condition { $2 }
;

/* hex... A VOIR */

/* high A VOIR */

/* host_... A VOIR */

/* id A VOIR  pour l'instant c'est un identificateur */
id: IDENT { $1 }
      

/* id_... A VOIR */

/* implt_... A VOIR */

/* indicator... A VOIR */

/* insert_columns_and_source of type insert_columns_and_source */
insert_columns_and_source:
      LPAR insert_column_list RPAR query_exp { Insert_query_exp ($2, $4) }
  | query_exp { Insert_query_exp ([], $1) }
  | T_DEFAULT T_VALUES { Insert_default_values }
;

/* insert_column_list of type insert_column_list = column_name list */
insert_column_list:
      column_name_list { $1 }
;

/* insert_stmt of type insert_stmt */
insert_stmt:
      T_INSERT T_INTO table_name insert_columns_and_source { ($3, $4) }
;

/* integrity... A VOIR */
/* intermediate A VOIR */

/* interval_factor of type interval factor = (sign option) * interval_primary */
interval_factor:
      interval_primary { (None, $1) }
  | sign interval_primary { (Some $1, $2) }
;

/* interval_lit of type interval_lit */
interval_lit:
      T_INTERVAL interval_string interval_qualifier { (None, $2, $3) }
  | T_INTERVAL sign interval_string interval_qualifier { (Some $2, $3, $4) }
;

/* interval_primary of type interval_primary */
interval_primary:
      value_exp_primary { ($1, None) }
  | value_exp_primary interval_qualifier { ($1, Some $2) }
;

/* interval_qualifier of type interval_qualifier */
interval_qualifier:
      start_field T_TO end_field { Interval_start_end ($1, $3) }
  | single_datetime_field { Interval_single_datetime_field $1 }
;

/* A VOIR : interval_string of type string */
interval_string: STRING { $1 }
;

/* interval_term of type interval_term */
interval_term:
      interval_factor { Interval_term_factor $1 }
  | interval_term T_ASTERISK factor { Interval_term_mult ($1, $3) }
  | interval_term T_DIV factor { Interval_term_div ($1, $3) }
  | term T_ASTERISK interval_factor { Interval_term_mult_interval ($1, $3) }
;

/* interval_type of type data_type */
interval_type:
      T_INTERVAL interval_qualifier { Interval $2 }
;

/* interval_value_exp of type interval_value_exp */
interval_value_exp:
      interval_term { Interval_value_term $1 }
  | interval_value_exp T_PLUS interval_term { Interval_value_plus ($1, $3) }
  | interval_value_exp T_MINUS interval_term { Interval_value_minus ($1, $3) }
  | LPAR datetime_value_exp T_MINUS datetime_term RPAR interval_qualifier { Interval_value_date ($2, $4, $6) }
;

/* in_predicate of type predicate */
in_predicate:
      row_value_constructor T_IN in_predicate_value { Predicate_in ($1, $3) }
  | row_value_constructor T_NOT T_IN in_predicate_value { Predicate_not_in ($1, $4) }
;

/* in_predicate_value of type in_predicate_value */
in_predicate_value:
      table_subquery { In_subquery $1 }
  | LPAR in_value_list RPAR { In_values $2 }
;

/* in_value_list of type in_value_list = value_exp list */
in_value_list:
      value_exp { [$1] }
  | value_exp T_COMMA in_value_list { $1 :: $3 }
;

/* isolation_level A VOIR */
/* item_number A VOIR */

/* joined_table of type joined_table */
joined_table:
      cross_join { Cross_join $1 }
  | qualified_join { Qualified_join $1 }
  | LPAR joined_table RPAR { $2 }
;

/* join_column_list of type join_column_list = column_name list */
join_column_list: column_name_list { $1 }
;

/* join_condition of type join_condition = search_condition */
join_condition: T_ON search_condition { $2 }
;

/* join_spec of type join_spec */
join_spec:
      join_condition { Join_condition $1 }
  | named_columns_join { Join_named_columns $1 }
;

/* join_type of type join_type */
join_type:
      T_INNER { Inner_join }
  | outer_join_type T_OUTER { $1 }
  | outer_join_type { $1 }
  | T_UNION { Union }
;

/* length_exp of type length_exp */
length_exp:
      char_length_exp { $1 }
  | octet_length_exp  { $1 }
/* A VOIR  | bit_length_exp  */
;

/* levels_clause of type levels_clause */
levels_clause:
      T_CASCADED { Levels_cascaded }
  | T_LOCAL { Levels_local }
;

/* level_of_siolation A VOIR */

/* like_predicate of type predicate */
like_predicate:
      match_value T_LIKE pattern { Predicate_like ($1, $3, None) }
  | match_value T_LIKE pattern T_ESCAPE escape_char { Predicate_like ($1, $3, Some $5) }
  | match_value T_NOT T_LIKE pattern { Predicate_not_like ($1, $4, None) }
  | match_value T_NOT T_LIKE pattern T_ESCAPE escape_char { Predicate_not_like ($1, $4, Some $6) }
;

/* limited_collation_def A VOIR */

/* lit of type lit */
lit:
      signed_num_lit { Signed_num_lit $1 }
  | general_lit { General_lit $1 }
;

/* local_table_name of type local_table_name = qualified_id */
/* A VOIR : not used
local_table_name: qualified_id { $1 }
;
*/

/* low A VOIR */

/* mantissa in the lexeur to build APPROXIMATE_NUM */

/* match_predicate of type predicate */
match_predicate:
      row_value_constructor T_MATCH match_predicate_type table_subquery { Predicate_match ($1, false, $3, $4) }
  | row_value_constructor T_MATCH T_UNIQUE match_predicate_type table_subquery { Predicate_match ($1, true, $4, $5) }
;

/* of type match_type */
match_predicate_type:
      { (* A VOIR : which one is the default ? *)
	Match_full
      }	
  | T_PARTIAL { Match_partial }
  | T_FULL { Match_full }
;

/* match_type of type match_type */
match_type:
  | T_PARTIAL { Match_partial }
  | T_FULL { Match_full }
;

/* match_value of type match_value = char_value_exp */
match_value: char_value_exp { $1 }
;

/* module_contents A VOIR */
/* mumps_... A VOIR */

/* named_columns_join of type named_columns_join = join_column_list */
named_columns_join:
      T_USING LPAR join_column_list RPAR { $3 }
;

/* national_char_string_lit A VOIR dans le lexeur */
/*national_char_string_lit:
      ::= 
      "n" "'" { char_representation } "'" 
      { { separator } "'" { char_representation } "'" } 
*/

national_char_string_type:
  T_NATIONAL T_CHAR { NationalChar None } 
| T_NATIONAL T_CHAR LPAR INT RPAR { NationalChar (Some $4) }
| T_NCHAR { NationalChar None } 
| T_NCHAR LPAR INT RPAR { NationalChar (Some $3) }
| T_NATIONAL T_CHAR T_VARYING LPAR INT RPAR { NationalVarChar $5 }
| T_NCHAR T_VARYING LPAR INT RPAR { NationalVarChar $4 }
;

/* next_in_c_list A VOIR */
/* nondelimiter_token A VOIR (lexeur ?) */

/* non_join_query_exp of type non_join_query_exp */
non_join_query_exp:
      non_join_query_term { Non_join_query_term $1 }
  | query_exp T_UNION query_term { Non_join_query_union ($1, false, None, $3) }
  | query_exp T_UNION T_ALL query_term { Non_join_query_union ($1, true, None, $4) }
  | query_exp T_UNION corresponding_spec query_term { Non_join_query_union ($1, false, Some $3, $4) }
  | query_exp T_UNION T_ALL corresponding_spec query_term { Non_join_query_union ($1, true, Some $4, $5) }

  | query_exp T_EXCEPT query_term { Non_join_query_except ($1, false, None, $3) }
  | query_exp T_EXCEPT T_ALL query_term { Non_join_query_except ($1, true, None, $4) }
  | query_exp T_EXCEPT corresponding_spec query_term { Non_join_query_except ($1, false, Some $3, $4) }
  | query_exp T_EXCEPT T_ALL corresponding_spec query_term { Non_join_query_except ($1, true, Some $4, $5) }
;

/* non_join_query_primary of type non_join_query_primary */
non_join_query_primary:
      simple_table { Non_join_simple_table $1 }
  | LPAR non_join_query_exp RPAR { Non_join_query_exp $2 }
;

/* non_join_query_term of type non_join_query_term */
non_join_query_term:
      non_join_query_primary { Non_join_query_primary $1 }
  | query_term T_INTERSECT query_primary { Intersect ($1, false, None, $3) }
  | query_term T_INTERSECT T_ALL query_primary { Intersect ($1, true, None, $4) }
  | query_term T_INTERSECT corresponding_spec query_primary { Intersect ($1, false, Some $3, $4) }
  | query_term T_INTERSECT T_ALL corresponding_spec query_primary  { Intersect ($1, true, Some $4, $5) }
;

/* non_second_datetime_field of type datetime_field */
non_second_datetime_field:
    T_YEAR { Datetime_field_year }
| T_MONTH { Datetime_field_month }
| T_DAY { Datetime_field_day }
| T_HOUR { Datetime_field_hour }
| T_MINUTE { Datetime_field_minute }
;

/* null_predicate of type predicate */
null_predicate:
      row_value_constructor T_IS T_NULL { Predicate_is_null $1 }
  | row_value_constructor T_IS T_NOT T_NULL { Predicate_is_not_null $1 }
;

/* null_spec */
null_spec: T_NULL { () }
;

/* number_of_conditions A VOIR */

/* num_primary of type num_primary */
num_primary:
      value_exp_primary { Num_value $1 }
  | num_value_fct { Num_fct $1 }
;

num_type:
    exact_num_type { Num (ExactNumType $1) }
| approximate_num_type { Num (Approximate_num_type $1) }
;

/* num_value_exp of type num_value_exp */
num_value_exp:
      term { Num_term $1 }
  | num_value_exp T_PLUS term { Num_plus ($1, $3) }
  | num_value_exp T_MINUS term { Num_minus ($1, $3) }
;

/* num_value_fct of type num_value_fct */
num_value_fct:
      position_exp { Fct_position $1 }
  | extract_exp { Fct_extract $1 }
  | length_exp { Fct_length $1 }
;

/* object_column of type object_column = column_name */
object_column: column_name { $1 }
;

/* object_name of type object_name */
object_name:
      table_name { Object_table $1 }
  | T_TABLE table_name { Object_table $2 }
  | T_DOMAIN domain_name { Object_domain $2 }
  | T_COLLATION collation_name { Object_collation $2 }
  | T_TRANSLATION translation_name { Object_translation $2}
/* A VOIR quand il y aura les char_set ...  | "character" "set" char_set_name { Object_character_set $3 }*/
;

/* occurences A VOIR */

/* octet_length_exp of type length_exp */
octet_length_exp:
      T_OCTET_LENGTH LPAR string_value_exp RPAR { Octet_length_exp $3 }
;

/* open_stmt A VOIR */

/* ordering_spec of type ordering_spec */
ordering_spec:
      T_ASC { Asc }
  | T_DESC { Desc }
;

/* order_by_clause of type order_by_clause = sort_spec_list */
order_by_clause:
      T_ORDER T_BY sort_spec_list { $3 }
;

/* outer_join_type of type join_type */
outer_join_type:
      T_LEFT { Left_outer_join }
  | T_RIGHT { Right_outer_join }
  | T_FULL { Full_outer_join }
;

/* overlaps_predicate of type predicate */
overlaps_predicate:
      row_value_constructor T_OVERLAPS row_value_constructor { Predicate_overlaps ($1, $3) }
;

/* pad_attribute A VOIR */
/* parameter_decl... A VOIR */
/* parameter_using_clause A VOIR */
/* pascal... A VOIR */

/* pattern of type pattern = char_value_exp */
pattern: char_value_exp { $1 }
;

/* pl1... A VOIR */

/* position_exp of type (char_value_exp * char_value_exp) */
position_exp:
      T_POSITION LPAR char_value_exp T_IN char_value_exp { ($3, $5) }
;

/* predicate of type predicate */
predicate:
      comp_predicate { $1 }
  | between_predicate { $1 }
  | in_predicate { $1 }
  | like_predicate { $1 }
  | null_predicate { $1 }
  | quantified_comp_predicate { $1 }
  | exists_predicate { $1 }
  | unique_predicate { $1 }
  | match_predicate { $1 }
  | overlaps_predicate { $1 }
;

/* prepare_stmt A VOIR */
/* prep... A VOIR */

/* privileges of type privileges */
privileges:
      T_ALL T_PRIVILEGES { All_privileges }
  | action_list { Actions $1 }
;

/* privilege_column_list of type privilege_column list */
privilege_column_list: column_name_list { $1 }
;

/* procedure... A VOIR */

/* qualified_id of type qualified_id = id */
qualified_id: id { $1 }
;

/* qualified_join of type qualified_join */
qualified_join:
      table_ref T_JOIN table_ref { ($1, false, None, $3, None) }
  | table_ref T_NATURAL T_JOIN table_ref { ($1, true, None, $4, None) }
  | table_ref join_type T_JOIN table_ref { ($1, false, Some $2, $4, None) }
  | table_ref T_NATURAL join_type T_JOIN table_ref { ($1, true, Some $3, $5, None) }
  | table_ref T_JOIN table_ref join_spec { ($1, false, None, $3, Some $4) }
  | table_ref T_NATURAL T_JOIN table_ref join_spec { ($1, true, None, $4, Some $5) }
  | table_ref join_type T_JOIN table_ref join_spec { ($1, false, Some $2, $4, Some $5) }
  | table_ref T_NATURAL join_type T_JOIN table_ref join_spec { ($1, true, Some $3, $5, Some $6) }
;

/* qualified_local_table_name of type qualified_local_table_name = local_table_name */
/* A VOIR : not used 
qualified_local_table_name:
      T_MODULE T_DOT local_table_name { $3 }
;
*/

/* qualified_name of type qualified_name */
qualified_name:
      qualified_id { (None, $1) }
/* A VOIR : conflit  | schema_name T_DOT qualified_id { (Some $1, $3) } */
;

/* qualifier of type qualifier */
qualifier:
      table_name { Qualifier_table $1 }
  | correlation_name { Qualifier_correlation $1 }
;

/* quantified_comp_predicate of type quantified_comp_predicate */
quantified_comp_predicate:
      row_value_constructor comp_op quantifier table_subquery { Predicate_quantified_comp ($1, $2, $3, $4) }
;

/* quantifier of type quantifier */
quantifier:
      T_ALL { Quantifier_all }
  | T_SOME { Quantifier_some }
  | T_ANY { Quantifier_some }
;

/* query_exp of type query_exp */
query_exp:
      non_join_query_exp { Query_exp_non_join $1 }
  | joined_table { Query_exp_joined_table $1 }
;

/* query_primary of type query_primary */
query_primary:
      non_join_query_primary { Query_prim_non_join $1 }
  | joined_table { Query_prim_joined_table $1 }
;

/* query_spec of type query_spec */
query_spec:
      T_SELECT set_quantifier select_list table_exp { (Some $2, $3, $4) }
  | T_SELECT select_list table_exp { (None, $2, $3) }
;

/* query_term of type query_term */
query_term:
      non_join_query_term { Query_term_non_join $1 }
  | joined_table { Query_term_joined_table $1 }
;

/* refd_table_and_columns of type refd_table_and_columns */
refd_table_and_columns:
      table_name { ($1, []) }
  | table_name  LPAR ref_column_list RPAR { ($1, $3) }
;

/* referencing_columns of type referencing_columns = ref_column_list */
referencing_columns: ref_column_list { $1 }
;

/* refs_spec of type refs_spec */
refs_spec:
      T_REFERENCES refd_table_and_columns { ($2, None, None) }
  | T_REFERENCES refd_table_and_columns T_MATCH match_type { ($2, Some $4, None) }
  | T_REFERENCES refd_table_and_columns ref_triggered_action { ($2, None, Some $3) }
  | T_REFERENCES refd_table_and_columns T_MATCH match_type ref_triggered_action { ($2, Some $4, Some $5) }
;

/* ref_action of type ref_action */
ref_action:
      T_CASCADE { Action_cascade }
  | T_SET T_NULL { Action_set_null }
  | T_SET T_DEFAULT { Action_set_default }
  | T_NO T_ACTION { Action_no_action }
;

/* ref_column_list of type ref_column_list = column_name list */
ref_column_list: column_name_list { $1 }
;

/* ref_constraint_def of type ref_constraint_def */
ref_constraint_def:
      T_FOREIGN T_KEY LPAR referencing_columns RPAR refs_spec { ($4, $6) }
;

/* ref_triggered_action of type ref_triggered_action */
ref_triggered_action:
      update_rule { (Some $1, None) }
  | update_rule delete_rule { (Some $1, Some $2) }
  | delete_rule { (None, Some $1) }
  | delete_rule update_rule { (Some $2, Some $1) }
;

/* regular_id A VOIR lexeur ? */

/* result A VOIR */

/* result_exp of type result_exp = value_exp */
/* A VOIR not used
result_exp: value_exp { $1 }
;
*/

/* result_using_clause A VOIR */

/* revoke_stmt of type sql_schema_manipulat_stmt */
revoke_stmt:
      T_REVOKE privileges T_ON object_name T_FROM grantee_list drop_behavior
      { Revoke_stmt (false, $2, $4, $6, $7) }
  | T_REVOKE T_GRANT T_OPTION T_FOR privileges T_ON object_name T_FROM grantee_list drop_behavior
      { Revoke_stmt (true, $5, $7, $9, $10) }
;

/* rollback_stmt A VOIR */

/* row_subquery of type row_subquery = subquery */
row_subquery: subquery { $1 }
;

/* row_value_constructor of type row_value_cons */
row_value_constructor:
      row_value_constructor_elem { Row_value_elem $1 }
  | LPAR row_value_const_list RPAR { Row_value_const_list $2 }
  | row_subquery { Row_value_subquery $1 }
;

/* row_value_constructor_elem of type row_value_cons_elem */
row_value_constructor_elem:
      value_exp { Row_value_exp $1 }
  | null_spec { Row_value_null }
  | default_spec { Row_value_default }
;

/* row_value_const_list of type row_value_const_list = row_value_cons_elem list */
row_value_const_list:
      row_value_constructor_elem { [$1] }
  | row_value_constructor_elem T_COMMA row_value_const_list { $1 :: $3 }
;

/* scalar_subquery of type scalar_subquery = subquery */
scalar_subquery: subquery { $1 }
;

/* schema_author_id of type schema_author_id = author_id */
/* A VOIR : not used
schema_author_id: author_id { $1 }
;
*/

/* schema_char_set_name of type schema_char_set_name = char_set_name */
/* A VOIR ; utiliser char_set ... schema_char_set_name: char_set_name { $1 } 
;
*/

/* schema_char_set_spec A VOIR */

/* schema_collation_name of type schema_collation_name = collation_name */
/* A VOIR : not used 
schema_collation_name: collation_name { $1 }
;
*/

/* schema_def A VOIR */
/* schema_element A VOIR */

/* schema_name of type schema_name */
schema_name:
      unqualified_schema_name { (None, $1) }
/* A VOIR conflit  | catalog_name T_DOT unqualified_schema_name { (Some $1, $3) } */
;

/* schema_name_clause A VOIR */

/* schema_translation_name of type schema_translation_name = translation_name */
/* A VOIR : not used 
schema_translation_name: translation_name { $1 }
;
*/


/* scope_option A VOIR */
/* searched_case A VOIR */
/* searched_when_clause A VOIR */

/* search_condition of type search_condition */
search_condition:
      boolean_term { Search_cond_boolean $1 }
  | search_condition T_OR boolean_term { Search_cond_or ($1, $3) }
;

/* seconds_value of type seconds_value */
seconds_value:
      INT { float_of_int $1 }
  | SIMPLE_FLOAT { $1 }
;

/* select_list of type select_list */
select_list:
      T_ASTERISK { Select_all }
  | select_sublist_list { print_string "select_list"; print_newline () ; Select_list $1 }
;

/* select_stmt_single_row of type select_stmt_single_row */
/* A VOIR : utilise select_target_list et donc les paramter_spec 
select_stmt_single_row:
      T_SELECT select_list T_INTO select_target_list table_exp
      { (None, $2, $4, $5) }
  | T_SELECT set_quantifier select_list T_INTO select_target_list table_exp
      { (Some $2, $3, $5, $6) }
;
*/


/* of type select_sublist list */
select_sublist_list:
      select_sublist { [$1] }
  | select_sublist T_COMMA select_sublist_list { $1 :: $3 }
;

/* select_sublist of type select_sublist */
select_sublist:
      derived_column { Select_column $1 }
/*  | qualifier T_DOT T_ASTERISK { Select_qualifier $1 }*/
;

/* select_target_list of type target_spec list */
/* A VOIR : utilise target_spec 
select_target_list:
      target_spec { [$1] }
  | target_spec T_COMMA select_target_list { $1 :: $3 }
;
*/


/* set_catalog_stmt A VOIR */

/* set_clause of type set_clause */
set_clause:
      object_column T_EQUALS update_source { ($1, $3) }
;

/* set_clause_list of type set_clause_list = set_clause list */
set_clause_list:
      set_clause { [$1] }
  | set_clause T_COMMA set_clause_list { $1 :: $3 }
;

/* set_column_default_clause of type set_column_default_clause = default_clause */
set_column_default_clause:
      T_SET default_clause { $2 }
;

/* set_connection_stmt A VOIR */
/* set_constraints_mode_stmt A VOIR */
/* set_count A VOIR */
/* set_descriptor... A VOIR */

/* set_domain_default_clause of type alter_domain_action */
set_domain_default_clause: 
      T_SET default_clause { Set_domain_default_clause $2 }
;

/* set_fct_spec of type set_fct_spec */
set_fct_spec:
/* A VOIR : see general_set_fct
      T_COUNT LPAR T_ASTERISK RPAR { Set_fct_spec_count }
*/
  | general_set_fct { (*Set_fct_spec_general*) $1 }
;

/* set_fct_type of type set_fct_type */
set_fct_type:
      T_AVG { Set_fct_avg }
  | T_MAX { Set_fct_max }
  | T_MIN { Set_fct_min }
  | T_SUM { Set_fct_sum }
  | T_COUNT { Set_fct_count }
;

/* set_item_information A VOIR */
/* set_local_time_zone_stmt A VOIR */
/* set_names_stmt A VOIR */

/* set_quantifier of type set_quantifier */
set_quantifier:
      T_DISTINCT { Distinct }
  | T_ALL { All }
;

/* set_schema_stmt A VOIR */
/* set_session_author_id_stmt A VOIR */
/* set_time_zone_value A VOIR */
/* set_transaction_stmt A VOIR */

/* signed_num_lit of type signed_num_lit */
signed_num_lit:
      unsigned_num_lit { (None, $1) }
  | sign unsigned_num_lit { (Some $1, $2) }
;

/* simple_case A VOIR */
/* simple_latin... A VOIR */

/* simple_table of simple_table */
simple_table:
      query_spec { Simple_table_query $1 }
  | table_value_constructor { Simple_table_value $1 }
  | explicit_table { Simple_table_explicit $1 }
;

/* simple_target... A VOIR */
/* simple_value... A VOIR */
/* simple_when_clause A VOIR */

/* sign of type sign */
sign:
      T_PLUS { Sign_plus }
  | T_MINUS { Sign_minus }
;


/* single_datetime_field of type single_datetime_field */
single_datetime_field:
      non_second_datetime_field { Single_datetime_non_second ($1, None) }
  | non_second_datetime_field LPAR INT RPAR { Single_datetime_non_second ($1, Some $3) }
  | T_SECOND { Single_datetime_second None }
  | T_SECOND LPAR INT RPAR { Single_datetime_second (Some ($3, None)) }
  | T_SECOND LPAR INT T_COMMA INT RPAR { Single_datetime_second (Some ($3, Some $5)) }
;

/* sort_key of type sort_key */
sort_key:
      column_name { Sort_key_column $1 }
  | INT { Sort_key_int $1 }
;

/* sort_spec of type sort_spec */
sort_spec:
      sort_key { ($1, None, None) }
  | sort_key collate_clause { ($1, Some $2, None) }
  | sort_key ordering_spec { ($1, None, Some $2) }
  | sort_key collate_clause ordering_spec { ($1, Some $2, Some $3) }
;

/* sort_spec_list of type sort_spec_list = sort_spec list */
sort_spec_list:
      sort_spec { [$1] }
  | sort_spec T_COMMA sort_spec_list { $1 :: $3 }
;

/* source_char_set_spec A VOIR */
/* sql_conformance A VOIR */
/* sql_connection_stmt A VOIR */

/* sql_data_change_stmt of type sql_data_change_stmt */
/* A VOIR : not used
sql_data_change_stmt:      
    delete_stmt_pos
  | delete_stmt_searched { Delete_stmt_searched $1 }
  | insert_stmt { Insert_stmt $1 }
  | update_stmt_pos
  | update_stmt_searched { Update_stmt_searched $1 }
;
*/

/* sql_diag... A VOIR */
/* sql_dyn... A VOIR */
/* sql_edition A VOIR */
/* sql_embdd_language_char A VOIR */
/* sql_language... A VOIR */
/* sql_object_id A VOIR */
/* sql_prefix A VOIR */
/* sql_procedure_stmt A VOIR */
/* sql_provenance A VOIR */

/* sql_schema_def_stmt of type sql_schema_def_stmt */
sql_schema_def_stmt:
/* A VOIR      schema_def { Schema_def $1 } */
  | table_def { Table_def $1 }
  | view_def { View_def $1 }
  | grant_stmt { Grant $1 }
  | domain_def { Domain_def $1 }
/* A VOIR  | char_set_def { Char_set_def $1 } */
/* A VOIR  | collation_def { Collation_def $1 } */
/* A VOIR   | translation_def { Translation_def $1 } */
/* A VOIR   | assertion_def { Assertion_def $1 } */
;

/* sql_schema_manipulat_stmt of type sql_schema_manipulat_stmt */
sql_schema_manipulat_stmt:
      drop_schema_stmt { $1 }
  | alter_table_stmt { $1 }
  | drop_table_stmt { $1 }
  | drop_view_stmt { $1 }
  | revoke_stmt { $1 }
  | alter_domain_stmt { $1 }
  | drop_domain_stmt { $1 }
/* A VOIR   | drop_char_set_stmt { $1 }*/
  | drop_collation_stmt { $1 }
  | drop_translation_stmt { $1 }
  | drop_assertion_stmt { $1 }
;

/* sql_schema_stmt of type sql_schema_stmt */
sql_schema_stmt:
      sql_schema_def_stmt { Schema_def_stmt $1 }
  | sql_schema_manipulat_stmt { Schema_manipulat_stmt $1 }
;

/* sql_server_name A VOIR */
/* sql_session_stmt A VOIR */
/* sql_transaction_stmt A VOIR */
/* sql_variant A VOIR */

/* start_field of type start_fiels */
start_field:
      non_second_datetime_field { ($1, None) }
  | non_second_datetime_field LPAR INT RPAR { ($1, Some $3) }
;

/* start_position of type start_position = num_value_exp */
start_position: num_value_exp { $1 }
;

/* status_parameter A VOIR */
/* std_char_rep_name A VOIR */

/* std_collation_name of type std_collation_name = collation_name */
/* A VOIR : not used
std_collation_name: collation_name { $1 }
;
*/

/* std_translation_name of type std_translation_name = translation_name */
/* A VOIR : not used
std_translation_name: translation_name { $1 }
;
*/

/* std_univ_char_form_name A VOIR */
/* stmt_info... A VOIR */
/* stmt_name A VOIR */
/* stmt_or_decl A VOIR */

/* string_length of type string_length = num_value_exp */
string_length: num_value_exp { $1 }
;

/* string_value_exp of type string_value_exp */
string_value_exp:
      char_value_exp { String_value_char $1 }
/* A VOIR   | bit_value_exp { String_value_bit $1 } */
;

/* string_value_fct of type string_value_fct */
string_value_fct:
      char_value_fct { String_fct_char $1 }
/* A VOIR  | bit_value_fct { String_fct_bit $1 }*/
;

/* subquery of type subquery = query_exp */
subquery: LPAR query_exp RPAR { $2 }
;

/* system_descriptor_stmt A VOIR */

/* table_constraint of type table_constraint */
table_constraint:
      unique_constraint_def { Table_unique_constraint $1 }
  | ref_constraint_def { Table_ref_constraint $1 }
  | check_constraint_def { Table_check_constraint $1 }
;

/* table_constraint_def of type table_constraint_def */
table_constraint_def:
      table_constraint { (None, $1, []) }
  | constraint_name_def table_constraint { (Some $1, $2, []) }
  | table_constraint constraint_attributes { (None, $1, $2) }
  | constraint_name_def table_constraint constraint_attributes { (Some $1, $2, $3) }
;

/* table_def of type table_def */
table_def:
      T_CREATE T_TABLE table_name table_element_list
      {
	{
	  tab_name = $3 ;
	  tab_temporary = None ;
	  tab_elements = $4 ;
	  tab_on_commit_action = None
	} 
      }	

  | T_CREATE temporary_scope T_TABLE table_name table_element_list
      {
	{
	  tab_name = $4 ;
	  tab_temporary = Some $2 ;
	  tab_elements = $5 ;
	  tab_on_commit_action = None
	} 
      }	

  | T_CREATE T_TABLE table_name table_element_list on_commit_action
      {
	{
	  tab_name = $3 ;
	  tab_temporary = None ;
	  tab_elements = $4 ;
	  tab_on_commit_action = Some $5
	} 
      }	

  | T_CREATE temporary_scope T_TABLE table_name table_element_list on_commit_action
      {
	{
	  tab_name = $4 ;
	  tab_temporary = Some $2 ;
	  tab_elements = $5 ;
	  tab_on_commit_action = Some $6
	} 
      }	
;

/* of type temporary_scope */
temporary_scope:
      T_GLOBAL T_TEMPORARY { Global }
  | T_LOCAL T_TEMPORARY { Local }
;
/* of type on_commit_action */
on_commit_action:
      T_ON T_COMMIT T_DELETE T_ROWS { Delete_rows }
  | T_ON T_COMMIT T_PRESERVE T_ROWS { Preserve_rows }
;

/* table_element of type table_element */
table_element:
      column_def { Column_def $1 }
  | table_constraint_def { Table_constraint_def $1 }
;

/* table_element_list of type table_element_list = table_element list */
table_element_list:
      LPAR table_element_list2 RPAR { $2 }
;
/* of type table_element_list */
table_element_list2:
      table_element { [$1] }
  | table_element T_COMMA table_element_list2 { $1 :: $3 }
;

/* table_exp of type table_exp */
table_exp:
      from_clause { ($1, None, None, None) }
  | from_clause where_clause { ($1, Some $2, None, None) }
  | from_clause group_by_clause { ($1, None, Some $2, None) }
  | from_clause having_clause { ($1, None, None, Some $2) }
  | from_clause where_clause group_by_clause { ($1, Some $2, Some $3, None) }
  | from_clause where_clause having_clause { ($1, Some $2, None, Some $3) }
  | from_clause group_by_clause having_clause { ($1, None, Some $2, Some $3) }
  | from_clause where_clause group_by_clause having_clause { ($1, Some $2, Some $3, Some $4) }
;

/* table_name of type table_name */
table_name:
      qualified_name { Qualified_table_name $1 }
/* A VOIR conflit  | qualified_local_table_name  { Qualified_local_table_name $1 } */
;

/* table_ref of type table_ref */
table_ref:
      table_name { Table_ref_name ($1, None) }
  | table_name correlation_name { Table_ref_name ($1, Some ($2, [])) }
  | table_name T_AS correlation_name { Table_ref_name ($1, Some ($3, [])) }
  | table_name correlation_name LPAR derived_column_list RPAR { Table_ref_name ($1, Some ($2, $4)) }
  | table_name T_AS correlation_name LPAR derived_column_list RPAR { Table_ref_name ($1, Some ($3, $5)) }

  | derived_table correlation_name { Table_ref_derived ($1, $2, []) }
  | derived_table T_AS correlation_name { Table_ref_derived ($1, $3, []) }
  | derived_table correlation_name LPAR derived_column_list RPAR { Table_ref_derived ($1, $2, $4) }
  | derived_table T_AS correlation_name LPAR derived_column_list RPAR { Table_ref_derived ($1, $3, $5) }

  | joined_table { Table_ref_joined $1 }
;

/* of type table_ref list */
table_ref_list:
      table_ref { [$1] }
  | table_ref T_COMMA table_ref_list { $1 :: $3 }
;

/* table_subquery of type table_subquery = subquery */
table_subquery: subquery { $1 }
;

/* table_value_constructor of type table_value_constructor = table_value_const_list */
table_value_constructor:
      T_VALUES table_value_const_list { $2 }
;

/* table_value_const_list of type table_value_const_list = row_value_cons list */
table_value_const_list:
      row_value_constructor { [$1] }
  | row_value_constructor T_COMMA table_value_const_list { $1 :: $3 }
;

/* target_char_set_spec A VOIR */
/* target_spec A VOIR */
/* temporary_table_decl A VOIR */

/* term of type term */
term:
      factor { Factor $1 }
  | term T_ASTERISK factor { Mult ($1, $3) }
  | term T_DIV factor { Div ($1, $3) }
;

/* timestamp_lit of type timestamp_lit = timestamp_string */
timestamp_lit:
      T_TIMESTAMP timestamp_string { $2 }
;

/* timestamp_precision of type timestamp_precision = int */
timestamp_precision: INT { $1 }
;

/* A VOIR : timestamp_string of type string */
timestamp_string: STRING { $1 }
;

/* time_lit of type time_lit = time_string */
time_lit:
      T_TIME time_string { $2 }
;

/* time_precision of type time_precision = int */
time_precision: INT { $1 }
;

/* time_string of type time_string */
time_string:
      T_QUOTE time_value T_QUOTE { ($2, None) }
  | T_QUOTE time_value time_zone_interval T_QUOTE { ($2, Some $3) }
;

/* time_value of type time_value */
time_value:
      INT T_COLON INT T_COLON seconds_value
      {
	{
	  time_hour = $1;
	  time_minute = $3;
	  time_second = $5
	} 
      }	
;

/* time_zone of type time_zone = time_zone_specifier */
time_zone:
      T_AT time_zone_specifier { $2 }
;

/* time_zone_field of type time_zone_field */
time_zone_field:
      T_TIMEZONE_HOUR { Time_zone_hour }
  | T_TIMEZONE_MINUTE { Time_zone_minute }
;

/* A VOIR : time_zone_interval of type string */
time_zone_interval: STRING { $1 }
;

/* time_zone_specifier of type time_zone_specifier */
time_zone_specifier:
      T_LOCAL { Local_time_zone }
  | T_TIME T_ZONE interval_value_exp { Interval_time_zone $3 }
;

/* transaction... A VOIR */
/* translation_... A VOIR */
/* translation_name of type translation_name = qualified_name */
translation_name: qualified_name { $1 }
;

/* trim_char of type trim_char = char_value_exp */
trim_char: char_value_exp { $1 }
;

/* trim_fct of type char_value_fct */
trim_fct:
      T_TRIM LPAR trim_operands RPAR { Char_trim_fct $3 }
;

/* trim_operands of type trim_operands */
trim_operands:
      trim_source { (None, None, $1) }
  | trim_spec T_FROM trim_source { (Some $1, None, $3) }
  | trim_char T_FROM trim_source { (None, Some $1, $3) }
  | trim_spec trim_char T_FROM trim_source { (Some $1, Some $2, $4) }
;

/* trim_source of type trim_source = char_value_exp */
trim_source: char_value_exp { $1 }
;

/* trim_spec of type trim_spec */
trim_spec:
      T_LEADING { Trim_leading }
  | T_TRAILING { Trim_trailing }
  | T_BOTH { Trim_both }
;

/* truth_value of type truth_value */
truth_value:
      T_TRUE { True }
  | T_FALSE { False }
  | T_UNKNOWN { Unknown }
;

/* unique_column_list of type unique_column_list = column_name_list */
unique_column_list: column_name_list { $1 }
;

/* unique_constraint_def of type unique_constraint_def */
/* A VOIR : grammaire correcte ? */
unique_constraint_def:
/*      unique_spec "even" "in" "sql3)"  */
    unique_spec LPAR unique_column_list RPAR { ($1, $3) }
;

/* unique_predicate of type predicate */
unique_predicate: T_UNIQUE table_subquery { Predicate_unique $2 }
;

/* unique_spec */
unique_spec:
      T_UNIQUE  { Unique }
  | T_PRIMARY T_KEY { Primary_key }
;

/* unqualified_schema_name of type unqualified_schema_name = id */
unqualified_schema_name: id { $1 }
;

/* unsigned_integer of type int */
/* A VOIR : not used
unsigned_integer: INT { $1 }
;
*/

/* unsigned_lit of type unsigned_lit */
unsigned_lit:
      unsigned_num_lit { Unsigned_num_lit $1 }
  | general_lit { Unsigned_general_lit $1 }
;

/* unsigned_num_lit of type unsigned_num_lit */
unsigned_num_lit:
      exact_num_lit { Exact_num_lit $1 }
  | approximate_num_lit { Approximate_num_lit $1 }
;

/* unsigned_value_spec of type unsigned_value_spec */
unsigned_value_spec:
      unsigned_lit { Unsigned_lit $1 }
  | general_value_spec { General_value $1 }
;

/* updatability_clause A VOIR */

/* update_rule of type update_rule = ref_action */
update_rule: T_ON T_UPDATE ref_action { $3 }
;

/* update_source of type update_source */
update_source:
      value_exp { Update_value $1 }
  | null_spec { Update_null }
  | T_DEFAULT { Update_default }
;

/* update_stmt_pos A VOIR */

/* update_stmt_searched of type update_stmt_searched */
update_stmt_searched:
      T_UPDATE table_name T_SET set_clause_list { ($2, $4, None) }
  | T_UPDATE table_name T_SET set_clause_list T_WHERE search_condition { ($2, $4, Some $6) }
;

/* user_def_char_rep_name A VOIR */
/* user_name A VOIR */
/* using_arguments A VOIR */
/* using_clause A VOIR */
/* using_descriptor A VOIR */

/* value_exp of type value_exp */
value_exp:
      num_value_exp { Value_exp_num $1 }
  | string_value_exp { Value_exp_string $1 }
  | datetime_value_exp { Value_exp_datetime $1 } 
  | interval_value_exp { Value_exp_interval $1 } 
;

/* value_exp_primary of type value_exp_primary */
value_exp_primary:
      unsigned_value_spec { Unsigned_value_exp $1 }
  | column_ref { Column_ref $1 }
  | set_fct_spec { Set_fct_spec $1 }
  | scalar_subquery { Scalar_subquery $1 }
/* A VOIR       | case_exp { Case_exp $1 }*/
  | LPAR value_exp RPAR { Value_exp $2 }
  | cast_spec { Cast_spec $1 }
;

/* value_spec of type value_spec */
/* A VOIR : not used 
value_spec:
      lit { Lit $1 }
  | general_value_spec { General_value_spec $1 }
;
*/

/* variable_spec A VOIR */

/* view_column_list of type view_column_list = column_name_list */
view_column_list: column_name_list { $1 }
;

/* view_def of type view_def */
view_def:
      T_CREATE T_VIEW table_name T_AS query_exp
      {
	{
	  view_name = $3;
	  view_columns = [];
	  view_query = $5;
	  view_levels = None;
	  view_with_check_option = false
	} 

      }	

  | T_CREATE T_VIEW table_name LPAR view_column_list RPAR T_AS query_exp
      {
	{
	  view_name = $3;
	  view_columns = $5;
	  view_query = $8;
	  view_levels = None;
	  view_with_check_option = false
	} 

      }	

  | T_CREATE T_VIEW table_name T_AS query_exp T_WITH T_CHECK T_OPTION
      {
	{
	  view_name = $3;
	  view_columns = [];
	  view_query = $5;
	  view_levels = None;
	  view_with_check_option = true
	} 

      }	

  | T_CREATE T_VIEW table_name T_AS query_exp T_WITH levels_clause T_CHECK T_OPTION
      {
	{
	  view_name = $3;
	  view_columns = [];
	  view_query = $5;
	  view_levels = Some $7;
	  view_with_check_option = true
	} 

      }	

  | T_CREATE T_VIEW table_name LPAR view_column_list RPAR T_AS query_exp T_WITH T_CHECK T_OPTION
      {
	{
	  view_name = $3;
	  view_columns = $5;
	  view_query = $8;
	  view_levels = None;
	  view_with_check_option = true
	} 

      }	

  | T_CREATE T_VIEW table_name LPAR view_column_list RPAR T_AS query_exp T_WITH levels_clause T_CHECK T_OPTION
      {
	{
	  view_name = $3;
	  view_columns = $5;
	  view_query = $8;
	  view_levels = Some $10;
	  view_with_check_option = true
	} 

      }	
;

/* when_operand of type when_operand = value_exp */
/* A VOIR : not used
when_operand: value_exp { $1 }
;
*/

/* where_clause of type where_clause = search_condition */
where_clause: T_WHERE search_condition { $2 }
;


%%


