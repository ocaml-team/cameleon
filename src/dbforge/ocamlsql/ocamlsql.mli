(**************************************************************************)
(*                   Cameleon                                             *)
(*                                                                        *)
(*      Copyright (C) 2002 Institut National de Recherche en Informatique et   *)
(*      en Automatique. All rights reserved.                              *)
(*                                                                        *)
(*      This program is free software; you can redistribute it and/or modify  *)
(*      it under the terms of the GNU General Public License as published by  *)
(*      the Free Software Foundation; either version 2 of the License, or  *)
(*      any later version.                                                *)
(*                                                                        *)
(*      This program is distributed in the hope that it will be useful,   *)
(*      but WITHOUT ANY WARRANTY; without even the implied warranty of    *)
(*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the     *)
(*      GNU General Public License for more details.                      *)
(*                                                                        *)
(*      You should have received a copy of the GNU General Public License  *)
(*      along with this program; if not, write to the Free Software       *)
(*      Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA          *)
(*      02111-1307  USA                                                   *)
(*                                                                        *)
(*      Contact: Maxence.Guesdon@inria.fr                                *)
(**************************************************************************)


(*[Mo] This module is the top module of the OCamlSQL library. [Mo]*)

(*[Fonc] This function takes a string (sql statement) and parses it. It returns
   a direct_sql_stmt value. Raises Failure if an error occurs.
   Raises End_of_file if nothing is encountered before End_of_file.[Fonc]*)
val parse_sql_stmt : string -> Ocamlsql_types.direct_sql_stmt

(*[Fonc] This function takes a channel and try to parse it as a SQL statement.
   It returns a direct_sql_stmt value. Raises Failure if an error occurs.
   Raises End_of_file if nothing is encountered before End_of_file.[Fonc]*)
val parse_sql_stmt_from_channel : in_channel -> Ocamlsql_types.direct_sql_stmt 

(*[Fonc] This function takes a string (column definition) and parses it. It returns
   a column_def value. Raises Failure if an error occurs.[Fonc]*)
val parse_column_def : string -> Ocamlsql_types.column_def

(*[Mo] This module contains the functions used to get string representation of OCamlSQL types.[Mo]*)
module String : 
    sig
      val string_of_action : Ocamlsql_types.action -> string
      val string_of_alter_column_action :
	  Ocamlsql_types.alter_column_action -> string
      val string_of_alter_domain_action :
	  Ocamlsql_types.alter_domain_action -> string
      val string_of_alter_table_action :
	  Ocamlsql_types.alter_table_action -> string
      val string_of_approximate_num_lit :
	  Ocamlsql_types.approximate_num_lit -> string
      val string_of_approx_num_type :
	  Ocamlsql_types.approx_num_type -> Ocamlsql_types.column_name
      val string_of_author_id :
	  Ocamlsql_types.column_name -> Ocamlsql_types.column_name
      val string_of_boolean_factor : Ocamlsql_types.boolean_factor -> string
      val string_of_boolean_primary : Ocamlsql_types.boolean_primary -> string
      val string_of_boolean_term : Ocamlsql_types.boolean_term -> string
      val string_of_boolean_test : Ocamlsql_types.boolean_test -> string
      val string_of_cast_operand : Ocamlsql_types.cast_operand -> string
      val string_of_cast_spec : Ocamlsql_types.cast_spec -> string
      val string_of_cast_target :
	  Ocamlsql_types.cast_target -> Ocamlsql_types.column_name
      val string_of_catalog_name :
	  Ocamlsql_types.column_name -> Ocamlsql_types.column_name
      val string_of_char_factor : Ocamlsql_types.char_factor -> string
      val string_of_char_primary : Ocamlsql_types.char_primary -> string
      val string_of_char_string_lit : Ocamlsql_types.char_string_lit -> string
      val string_of_char_value_exp : Ocamlsql_types.escape_char -> string
      val string_of_char_value_fct : Ocamlsql_types.char_value_fct -> string
      val string_of_check_constraint : Ocamlsql_types.check_constraint -> string
      val string_of_collate_clause : Ocamlsql_types.collation_name -> string
      val string_of_collation_def :
	  Ocamlsql_types.collation_def -> Ocamlsql_types.collation_def
      val string_of_collation_name :
	  Ocamlsql_types.collation_name -> Ocamlsql_types.column_name
      val string_of_column_constraint : Ocamlsql_types.column_constraint -> string
      val string_of_column_constraint_def :
	  Ocamlsql_types.column_constraint_def -> string
      val string_of_column_type :
	  Ocamlsql_types.column_type -> Ocamlsql_types.column_name
      val string_of_column_def : Ocamlsql_types.column_def -> string
      val string_of_column_name :
	  Ocamlsql_types.column_name -> Ocamlsql_types.column_name
      val string_of_column_name_list : Ocamlsql_types.insert_column_list -> string
      val string_of_column_ref : Ocamlsql_types.column_ref -> string
      val string_of_comp_op : Ocamlsql_types.comp_op -> string
      val string_of_comp_predicate :
	  Ocamlsql_types.row_value_cons * Ocamlsql_types.comp_op *
	  Ocamlsql_types.row_value_cons -> string
      val string_of_concatenation : Ocamlsql_types.concatenation -> string
      val string_of_constraint_attribute :
	  Ocamlsql_types.constraint_attribute -> string
      val string_of_constraint_name :
	  Ocamlsql_types.constraint_name -> Ocamlsql_types.column_name
      val string_of_constraint_name_def :
	  Ocamlsql_types.constraint_name_def -> string
      val string_of_correlation_name :
	  Ocamlsql_types.column_name -> Ocamlsql_types.column_name
      val string_of_corresponding_column_list :
	  Ocamlsql_types.insert_column_list -> string
      val string_of_corresponding_spec :
	  Ocamlsql_types.insert_column_list -> string
      val string_of_data_type :
	  Ocamlsql_types.data_type -> Ocamlsql_types.column_name
      val string_of_datetime_factor : Ocamlsql_types.datetime_term -> string
      val string_of_datetime_field : Ocamlsql_types.datetime_field -> string
      val string_of_datetime_lit : Ocamlsql_types.datetime_lit -> string
      val string_of_datetime_primary : Ocamlsql_types.datetime_primary -> string
      val string_of_datetime_term : Ocamlsql_types.datetime_term -> string
      val string_of_datetime_value : int -> string
      val string_of_datetime_value_exp :
	  Ocamlsql_types.datetime_value_exp -> string
      val string_of_datetime_value_fct :
	  Ocamlsql_types.datetime_value_fct -> string
      val string_of_date_value : Ocamlsql_types.date_value -> string
      val string_of_default_clause : Ocamlsql_types.default_clause -> string
      val string_of_delete_rule : Ocamlsql_types.delete_rule -> string
      val string_of_delete_stmt_searched :
	  Ocamlsql_types.delete_stmt_searched -> Ocamlsql_types.sql_connection_stmt
      val string_of_derived_column : Ocamlsql_types.derived_column -> string
      val string_of_derived_column_list :
	  Ocamlsql_types.insert_column_list -> string
      val string_of_derived_table : Ocamlsql_types.table_subquery -> string
      val string_of_direct_sql_stmt : Ocamlsql_types.directly_exec_stmt -> string
      val string_of_directly_exec_stmt :
	  Ocamlsql_types.directly_exec_stmt -> Ocamlsql_types.sql_connection_stmt
      val string_of_direct_sql_data_stmt :
	  Ocamlsql_types.direct_sql_data_stmt -> Ocamlsql_types.sql_connection_stmt
      val string_of_direct_select_stmt_n_rows :
	  Ocamlsql_types.direct_select_stmt_n_rows ->
	    Ocamlsql_types.sql_connection_stmt
      val string_of_domain_constraint : Ocamlsql_types.domain_constraint -> string
      val string_of_domain_def :
	  Ocamlsql_types.domain_def -> Ocamlsql_types.schema_def
      val string_of_domain_name :
	  Ocamlsql_types.domain_name -> Ocamlsql_types.column_name
      val string_of_drop_behavior : Ocamlsql_types.drop_behavior -> string
      val string_of_end_field : Ocamlsql_types.end_field -> string
      val string_of_escape_char : Ocamlsql_types.escape_char -> string
      val string_of_exact_num_lit : Ocamlsql_types.exact_num_lit -> string
      val string_of_exact_num_type :
	  Ocamlsql_types.exact_num_type -> Ocamlsql_types.column_name
      val string_of_explicit_table : Ocamlsql_types.explicit_table -> string
      val string_of_extract_field : Ocamlsql_types.extract_field -> string
      val string_of_extract_source : Ocamlsql_types.extract_source -> string
      val string_of_factor : Ocamlsql_types.factor -> string
      val string_of_fold_style : Ocamlsql_types.fold_style -> string
      val string_of_form_conversion_name :
	  Ocamlsql_types.form_conversion_name -> Ocamlsql_types.column_name
      val string_of_from_clause : Ocamlsql_types.from_clause -> string
      val string_of_general_lit : Ocamlsql_types.general_lit -> string
      val string_of_general_set_fct : Ocamlsql_types.general_set_fct -> string
      val string_of_general_value_spec :
	  Ocamlsql_types.general_value_spec -> string
      val string_of_grantee : Ocamlsql_types.grantee -> Ocamlsql_types.column_name
      val string_of_grant_stmt :
	  Ocamlsql_types.grant_stmt -> Ocamlsql_types.schema_def
      val string_of_grouping_column_ref :
	  Ocamlsql_types.grouping_column_ref -> string
      val string_of_group_by_clause : Ocamlsql_types.group_by_clause -> string
      val string_of_having_clause : Ocamlsql_types.check_constraint -> string
      val string_of_id : Ocamlsql_types.column_name -> Ocamlsql_types.column_name
      val string_of_insert_columns_and_source :
	  Ocamlsql_types.insert_columns_and_source -> string
      val string_of_insert_column_list :
	  Ocamlsql_types.insert_column_list -> string
      val string_of_insert_stmt :
	  Ocamlsql_types.insert_stmt -> Ocamlsql_types.sql_connection_stmt
      val string_of_interval_factor : Ocamlsql_types.interval_factor -> string
      val string_of_interval_lit : Ocamlsql_types.interval_lit -> string
      val string_of_interval_primary : Ocamlsql_types.interval_primary -> string
      val string_of_interval_qualifier :
	  Ocamlsql_types.interval_qualifier -> string
      val string_of_interval_string :
	  Ocamlsql_types.interval_string -> Ocamlsql_types.interval_string
      val string_of_interval_term : Ocamlsql_types.interval_term -> string
      val string_of_interval_value_exp :
	  Ocamlsql_types.interval_value_exp -> string
      val string_of_in_predicate_value :
	  Ocamlsql_types.in_predicate_value -> string
      val string_of_in_value_list : Ocamlsql_types.in_value_list -> string
      val string_of_joined_table : Ocamlsql_types.joined_table -> string
      val string_of_join_column_list : Ocamlsql_types.insert_column_list -> string
      val string_of_join_condition : Ocamlsql_types.check_constraint -> string
      val string_of_join_spec : Ocamlsql_types.join_spec -> string
      val string_of_join_type : Ocamlsql_types.join_type -> string
      val string_of_length : Ocamlsql_types.length -> string
      val string_of_length_exp : Ocamlsql_types.length_exp -> string
      val string_of_levels_clause : Ocamlsql_types.levels_clause -> string
      val string_of_lit : Ocamlsql_types.lit -> string
      val string_of_local_table_name :
	  Ocamlsql_types.column_name -> Ocamlsql_types.column_name
      val string_of_match_predicate : Ocamlsql_types.match_predicate -> string
      val string_of_match_type : Ocamlsql_types.match_type -> string
      val string_of_match_value : Ocamlsql_types.escape_char -> string
      val string_of_named_columns_join :
	  Ocamlsql_types.named_columns_join -> string
      val string_of_national_char_string_lit :
	  Ocamlsql_types.national_char_string_lit -> string
      val string_of_non_join_query_exp :
	  Ocamlsql_types.non_join_query_exp -> string
      val string_of_non_join_query_primary :
	  Ocamlsql_types.non_join_query_primary -> string
      val string_of_non_join_query_term :
	  Ocamlsql_types.non_join_query_term -> string
      val string_of_num_primary : Ocamlsql_types.num_primary -> string
      val string_of_num_type :
	  Ocamlsql_types.num_type -> Ocamlsql_types.column_name
      val string_of_num_value_exp : Ocamlsql_types.start_position -> string
      val string_of_num_value_fct : Ocamlsql_types.num_value_fct -> string
      val string_of_object_column :
	  Ocamlsql_types.column_name -> Ocamlsql_types.column_name
      val string_of_object_name : Ocamlsql_types.object_name -> string
      val string_of_order_by_clause : Ocamlsql_types.order_by_clause -> string
      val string_of_ordering_spec : Ocamlsql_types.ordering_spec -> string
      val string_of_pattern : Ocamlsql_types.escape_char -> string
      val string_of_precision : Ocamlsql_types.precision -> string
      val string_of_predicate : Ocamlsql_types.predicate -> string
      val string_of_privilege_column :
	  Ocamlsql_types.privilege_column -> Ocamlsql_types.column_name
      val string_of_privileges : Ocamlsql_types.privileges -> string
      val string_of_qualified_id :
	  Ocamlsql_types.column_name -> Ocamlsql_types.column_name
      val string_of_qualified_join : Ocamlsql_types.qualified_join -> string
      val string_of_qualified_local_table_name :
	  Ocamlsql_types.column_name -> Ocamlsql_types.column_name
      val string_of_qualified_name :
	  Ocamlsql_types.collation_name -> Ocamlsql_types.column_name
      val string_of_qualifier :
	  Ocamlsql_types.qualifier -> Ocamlsql_types.column_name
      val string_of_quantified_comp_predicate :
	  Ocamlsql_types.row_value_cons * Ocamlsql_types.comp_op *
	  Ocamlsql_types.quantifier * Ocamlsql_types.table_subquery -> string
      val string_of_quantifier : Ocamlsql_types.quantifier -> string
      val string_of_query_exp : Ocamlsql_types.row_subquery -> string
      val string_of_query_primary : Ocamlsql_types.query_primary -> string
      val string_of_query_spec : Ocamlsql_types.query_spec -> string
      val string_of_query_term : Ocamlsql_types.query_term -> string
      val string_of_refd_table_and_columns :
	  Ocamlsql_types.refd_table_and_columns -> string
      val string_of_referencing_columns : Ocamlsql_types.ref_column_list -> string
      val string_of_refs_spec : Ocamlsql_types.refs_spec -> string
      val string_of_ref_action : Ocamlsql_types.delete_rule -> string
      val string_of_ref_column_list : Ocamlsql_types.ref_column_list -> string
      val string_of_ref_constraint_def :
	  Ocamlsql_types.ref_constraint_def -> string
      val string_of_ref_triggered_action :
	  Ocamlsql_types.ref_triggered_action -> string
      val string_of_result_exp : Ocamlsql_types.value_exp -> string
      val string_of_row_subquery : Ocamlsql_types.row_subquery -> string
      val string_of_row_value_cons : Ocamlsql_types.row_value_cons -> string
      val string_of_row_value_cons_elem :
	  Ocamlsql_types.row_value_cons_elem -> string
      val string_of_row_value_const_list :
	  Ocamlsql_types.row_value_const_list -> string
      val string_of_scalar_subquery : Ocamlsql_types.row_subquery -> string
      val string_of_scale : Ocamlsql_types.scale -> string
      val string_of_schema_author_id :
	  Ocamlsql_types.column_name -> Ocamlsql_types.column_name
      val string_of_schema_collation_name :
	  Ocamlsql_types.collation_name -> Ocamlsql_types.column_name
      val string_of_schema_def :
	  Ocamlsql_types.schema_def -> Ocamlsql_types.schema_def
      val string_of_schema_name : Ocamlsql_types.schema_name -> string
      val string_of_schema_translation_name :
	  Ocamlsql_types.translation_name -> Ocamlsql_types.column_name
      val string_of_search_condition : Ocamlsql_types.check_constraint -> string
      val string_of_seconds_value : Ocamlsql_types.seconds_value -> string
      val string_of_select_list : Ocamlsql_types.select_list -> string
      val string_of_select_sublist : Ocamlsql_types.select_sublist -> string
      val string_of_set_clause : Ocamlsql_types.set_clause -> string
      val string_of_set_clause_list : Ocamlsql_types.set_clause_list -> string
      val string_of_set_column_default_clause :
	  Ocamlsql_types.default_clause -> string
      val string_of_set_fct_spec : Ocamlsql_types.set_fct_spec -> string
      val string_of_set_fct_type : Ocamlsql_types.set_fct_type -> string
      val string_of_set_quantifier : Ocamlsql_types.set_quantifier -> string
      val string_of_sign : Ocamlsql_types.sign -> string
      val string_of_signed_num_lit : Ocamlsql_types.signed_num_lit -> string
      val string_of_simple_table : Ocamlsql_types.simple_table -> string
      val string_of_single_datetime_field :
	  Ocamlsql_types.single_datetime_field -> string
      val string_of_sort_key :
	  Ocamlsql_types.sort_key -> Ocamlsql_types.column_name
      val string_of_sort_spec : Ocamlsql_types.sort_spec -> string
      val string_of_sort_spec_list : Ocamlsql_types.order_by_clause -> string
      val string_of_sql_connection_stmt :
	  Ocamlsql_types.sql_connection_stmt -> Ocamlsql_types.sql_connection_stmt
      val string_of_sql_data_change_stmt :
	  Ocamlsql_types.sql_data_change_stmt -> Ocamlsql_types.sql_connection_stmt
      val string_of_sql_schema_def_stmt :
	  Ocamlsql_types.sql_schema_def_stmt -> Ocamlsql_types.schema_def
      val string_of_sql_schema_manipulat_stmt :
	  Ocamlsql_types.sql_schema_manipulat_stmt -> Ocamlsql_types.schema_def
      val string_of_sql_schema_stmt :
	  Ocamlsql_types.sql_schema_stmt -> Ocamlsql_types.sql_connection_stmt
      val string_of_sql_session_stmt :
	  Ocamlsql_types.sql_session_stmt -> Ocamlsql_types.sql_connection_stmt
      val string_of_sql_transaction_stmt :
	  Ocamlsql_types.sql_transaction_stmt -> Ocamlsql_types.sql_connection_stmt
      val string_of_start_field : Ocamlsql_types.start_field -> string
      val string_of_start_position : Ocamlsql_types.start_position -> string
      val string_of_std_collation_name :
	  Ocamlsql_types.collation_name -> Ocamlsql_types.column_name
      val string_of_std_translation_name :
	  Ocamlsql_types.translation_name -> Ocamlsql_types.column_name
      val string_of_string_length : Ocamlsql_types.string_length -> string
      val string_of_string_value_exp : Ocamlsql_types.string_value_exp -> string
      val string_of_string_value_fct : Ocamlsql_types.string_value_fct -> string
      val string_of_subquery : Ocamlsql_types.row_subquery -> string
      val string_of_table_constraint : Ocamlsql_types.table_constraint -> string
      val string_of_table_constraint_def :
	  Ocamlsql_types.table_constraint_def -> string
      val string_of_table_def :
	  Ocamlsql_types.table_def -> Ocamlsql_types.schema_def
      val string_of_temporary_scope : Ocamlsql_types.temporary_scope -> string
      val string_of_on_commit_action : Ocamlsql_types.on_commit_action -> string
      val string_of_table_element : Ocamlsql_types.table_element -> string
      val string_of_table_element_list :
	  Ocamlsql_types.table_element_list -> string
      val string_of_table_exp : Ocamlsql_types.table_exp -> string
      val string_of_table_name :
	  Ocamlsql_types.explicit_table -> Ocamlsql_types.column_name
      val string_of_table_ref : Ocamlsql_types.table_ref -> string
      val string_of_table_subquery : Ocamlsql_types.table_subquery -> string
      val string_of_table_value_constructor :
	  Ocamlsql_types.table_value_constructor -> string
      val string_of_table_value_const_list :
	  Ocamlsql_types.table_value_constructor -> string
      val string_of_temporary_table_decl :
	  Ocamlsql_types.temporary_table_decl -> Ocamlsql_types.sql_connection_stmt
      val string_of_term : Ocamlsql_types.term -> string
      val string_of_timestamp_lit : Ocamlsql_types.timestamp_lit -> string
      val string_of_timestamp_precision : int -> string
      val string_of_timestamp_string :
	  Ocamlsql_types.timestamp_lit -> Ocamlsql_types.timestamp_lit
      val string_of_time_lit : Ocamlsql_types.time_lit -> string
      val string_of_time_precision : int -> string
      val string_of_time_string : Ocamlsql_types.time_lit -> string
      val string_of_time_value : Ocamlsql_types.time_value -> string
      val string_of_time_zone : Ocamlsql_types.time_zone -> string
      val string_of_time_zone_field : Ocamlsql_types.time_zone_field -> string
      val string_of_time_zone_interval :
	  Ocamlsql_types.time_zone_interval -> Ocamlsql_types.time_zone_interval
      val string_of_time_zone_specifier : Ocamlsql_types.time_zone -> string
      val string_of_translation_name :
	  Ocamlsql_types.translation_name -> Ocamlsql_types.column_name
      val string_of_trim_char : Ocamlsql_types.escape_char -> string
      val string_of_trim_operands : Ocamlsql_types.trim_operands -> string
      val string_of_trim_source : Ocamlsql_types.trim_source -> string
      val string_of_trim_spec : Ocamlsql_types.trim_spec -> string
      val string_of_truth_value : Ocamlsql_types.truth_value -> string
      val string_of_unique_column_list :
	  Ocamlsql_types.insert_column_list -> string
      val string_of_unique_constraint_def :
	  Ocamlsql_types.unique_constraint_def -> string
      val string_of_unique_spec : Ocamlsql_types.unique_spec -> string
      val string_of_unqualified_schema_name :
	  Ocamlsql_types.unqualified_schema_name -> Ocamlsql_types.column_name
      val string_of_unsigned_lit : Ocamlsql_types.unsigned_lit -> string
      val string_of_unsigned_num_lit : Ocamlsql_types.unsigned_num_lit -> string
      val string_of_unsigned_value_spec :
	  Ocamlsql_types.unsigned_value_spec -> string
      val string_of_update_rule : Ocamlsql_types.update_rule -> string
      val string_of_update_source : Ocamlsql_types.update_source -> string
      val string_of_update_stmt_searched :
	  Ocamlsql_types.update_stmt_searched -> Ocamlsql_types.sql_connection_stmt
      val string_of_value_exp : Ocamlsql_types.value_exp -> string
      val string_of_value_exp_primary : Ocamlsql_types.value_exp_primary -> string
      val string_of_value_spec : Ocamlsql_types.value_spec -> string
      val string_of_view_column_list : Ocamlsql_types.insert_column_list -> string
      val string_of_view_def : Ocamlsql_types.view_def -> Ocamlsql_types.schema_def
      val string_of_where_clause : Ocamlsql_types.where_clause -> string
    end
