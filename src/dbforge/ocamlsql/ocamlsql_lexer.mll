(**************************************************************************)
(*                   Cameleon                                             *)
(*                                                                        *)
(*      Copyright (C) 2002 Institut National de Recherche en Informatique et   *)
(*      en Automatique. All rights reserved.                              *)
(*                                                                        *)
(*      This program is free software; you can redistribute it and/or modify  *)
(*      it under the terms of the GNU General Public License as published by  *)
(*      the Free Software Foundation; either version 2 of the License, or  *)
(*      any later version.                                                *)
(*                                                                        *)
(*      This program is distributed in the hope that it will be useful,   *)
(*      but WITHOUT ANY WARRANTY; without even the implied warranty of    *)
(*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the     *)
(*      GNU General Public License for more details.                      *)
(*                                                                        *)
(*      You should have received a copy of the GNU General Public License  *)
(*      along with this program; if not, write to the Free Software       *)
(*      Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA          *)
(*      02111-1307  USA                                                   *)
(*                                                                        *)
(*      Contact: Maxence.Guesdon@inria.fr                                *)
(**************************************************************************)

(*[Mo] Lexer for SQL queries. [Mo]*)

{
open Lexing
open Ocamlsql_parser

let line_number = ref 0

(* variables pour bufferiser les chaines de caract'eres *)
let string_buffer_initial = String.create 256;;
let string_buffer = ref string_buffer_initial;;
let string_index = ref 0;;

(* fonction de remise � z�ro de la chaine de caract�res tampon *)
let reset_string_buffer () =
  string_buffer := string_buffer_initial;
  string_index  := 0;
  ()
;;

(* Fonction d'ajout d'un caract�re dans la chaine de caract�res tampon *)
let ajout_char_string c =
  if !string_index >= String.length (!string_buffer) then 
  begin
    let new_buffer = String.create ((String.length (!string_buffer)) * 2) in
      String.blit (!string_buffer) 0 new_buffer 0 (String.length (!string_buffer));
      string_buffer := new_buffer
  end;
  String.set (!string_buffer) (!string_index) c;
  incr string_index
;;

let lecture_string () =
  let s = String.sub (!string_buffer) 0 (!string_index) in
    string_buffer := string_buffer_initial;
    s
;;

(* the table of key words *)
let keywords = Hashtbl.create 11

(* the list of couples (keyword, token) *)
let keywords_list = [
  "action", T_ACTION ;
  "add", T_ADD ;
  "all", T_ALL ;
  "alter", T_ALTER ;
  "and", T_AND ;
  "any", T_ANY ;
  "as", T_AS ;
  "asc", T_ASC ;
  "assertion", T_ASSERTION ;
  "at", T_AT ;
  "avg", T_AVG ;

  "between", T_BETWEEN ;
  "bit", T_BIT ;
  "both", T_BOTH ;
  "by", T_BY ;

  "cascade", T_CASCADE ;
  "cascaded", T_CASCADED ;
  "cast", T_CAST ;
  "char", T_CHAR ;
  "character", T_CHAR ;
  "char_length", T_CHAR_LENGTH ;
  "character_length", T_CHAR_LENGTH ;
  "check", T_CHECK ;
  "collate", T_COLLATE ;
  "collation", T_COLLATION ;
  "column", T_COLUMN ;
  "commit", T_COMMIT ;
  "constraint", T_CONSTRAINT ;
  "convert", T_CONVERT ;
  "corresponding", T_CORRESPONDING ;
  "count", T_COUNT ;
  "create", T_CREATE ;
  "cross", T_CROSS ;
  "current_date", T_CURRENT_DATE ;
  "current_time", T_CURRENT_TIME ;
  "current_timestamp", T_CURRENT_TIMESTAMP ;
  "current_user", T_CURRENT_USER ;

  "date", T_DATE ;
  "day", T_DAY ;
  "dec", T_DECIMAL ;
  "decimal", T_DECIMAL ;
  "default", T_DEFAULT ;
  "deferrable", T_DEFERRABLE ;
  "deferred", T_DEFERRED;
  "delete", T_DELETE ;
  "desc", T_DESC ;
  "distinct", T_DISTINCT ;
  "domain", T_DOMAIN ;
  "double", T_DOUBLE ;
  "drop", T_DROP ;

  "escape", T_ESCAPE ;
  "except", T_EXCEPT ;
  "exists", T_EXISTS ;
  "external", T_EXTERNAL ;
  "extract", T_EXTRACT ;

  "false", T_FALSE ;
  "float", T_FLOAT ;
  "for", T_FOR ;
  "foreign", T_FOREIGN ; 
  "from", T_FROM ;
  "full", T_FULL ;

  "global", T_GLOBAL ;
  "grant", T_GRANT ;
  "group", T_GROUP ;

  "having", T_HAVING ;
  "hour", T_HOUR ;

  "immediate", T_IMMEDIATE ;
  "in", T_IN ;
  "initially", T_INITIALLY ;
  "inner", T_INNER ;
  "insert", T_INSERT ;
  "int", T_INTEGER ;
  "integer", T_INTEGER ;
  "intersect", T_INTERSECT ;
  "interval", T_INTERVAL ;
  "into", T_INTO ;
  "is", T_IS ;

  "join", T_JOIN ;

  "key", T_KEY ;

  "leading", T_LEADING ;
  "left", T_LEFT ;
  "like", T_LIKE ;
  "local", T_LOCAL ;
  "lower", T_LOWER ;

  "match", T_MATCH ;
  "max", T_MAX ;
  "min", T_MIN ;
  "minute", T_MINUTE ;
  "module", T_MODULE ;
  "month", T_MONTH ;

  "national", T_NATIONAL ;
  "natural", T_NATURAL ;
  "nchar", T_NCHAR ;
  "no", T_NO ;
  "not", T_NOT ;
  "null", T_NULL ;
  "numeric", T_NUMERIC ;

  "octet_length", T_OCTET_LENGTH ;
  "on", T_ON ;
  "option", T_OPTION ;
  "or", T_OR ;
  "order", T_ORDER ;
  "outer", T_OUTER ;
  "overlaps", T_OVERLAPS ;

  "partial", T_PARTIAL ;
  "position", T_POSITION ;
  "preserve", T_PRESERVE ;
  "primary", T_PRIMARY ;
  "privileges", T_PRIVILEGES ;
  "public", T_PUBLIC ;

  "real", T_REAL ;
  "references", T_REFERENCES ;
  "restrict", T_RESTRICT ;
  "revoke", T_REVOKE ;
  "right", T_RIGHT ;
  "rows", T_ROWS ;

  "schema", T_SCHEMA ;
  "second", T_SECOND ;
  "select", T_SELECT ;
  "session_user", T_SESSION_USER ;
  "set", T_SET ;
  "smallint", T_SMALLINT ;
  "some", T_SOME ;
  "substring", T_SUBSTRING ;
  "sum", T_SUM ;
  "system_user", T_SYSTEM_USER ;

  "table", T_TABLE ; 
  "temporary", T_TEMPORARY ;
  "time", T_TIME ;
  "timestamp", T_TIMESTAMP ;
  "timezone_hour", T_TIMEZONE_HOUR ;
  "timezone_minute", T_TIMEZONE_MINUTE ;
  "to", T_TO ;
  "trailing", T_TRAILING ;
  "translate", T_TRANSLATE ;
  "translation", T_TRANSLATION ;
  "trim", T_TRIM ;
  "true", T_TRUE ;

  "unknown", T_UNKNOWN ;
  "union", T_UNION ;
  "unique", T_UNIQUE ;
  "update", T_UPDATE ;
  "upper", T_UPPER ;
  "usage", T_USAGE ;
  "user", T_USER ;
  "using", T_USING ;

  "value", T_VALUE ;
  "values", T_VALUES ;
  "varchar", T_VARCHAR ;
  "varying", T_VARYING ;
  "view", T_VIEW ;

  "where", T_WHERE ;
  "with", T_WITH ;

  "year", T_YEAR ;

  "zone", T_ZONE ;
];;

(* add the key words to the keywords table *)
List.iter (fun (word, token) -> Hashtbl.add keywords word token) keywords_list;;

(* variables used to keep strings *)
let string_buffer_initial = String.create 256;;
let string_buffer = ref string_buffer_initial;;
let string_index = ref 0;;

(* this function resets the buffer string *)
let reset_string_buffer () =
  string_buffer := string_buffer_initial;
  string_index  := 0;
  ()
;;

(* this function adds a char in the buffer string.*)
let add_char_to_buffer c =
  if !string_index >= String.length (!string_buffer) then 
  begin
    let new_buffer = String.create ((String.length (!string_buffer)) * 2) in
      String.blit (!string_buffer) 0 new_buffer 0 (String.length (!string_buffer));
      string_buffer := new_buffer
  end;
  String.set (!string_buffer) (!string_index) c;
  incr string_index
;;

(* this function returns the buffer string *)
let get_buffer_string () =
  let s = String.sub (!string_buffer) 0 (!string_index) in
    string_buffer := string_buffer_initial;
    s
;;



}

rule main = parse
    [' ' '\013' '\009' '\012'] +
      { main lexbuf }

  | [ '\010' ]
      { incr line_number;
         main lexbuf }

  | ['a'-'z' 'A'-'Z'] ['_' 'A'-'Z' 'a'-'z' '0'-'9' ]*
      { let s = Lexing.lexeme lexbuf in
          let su = String.lowercase s in
            try
              Hashtbl.find keywords su 
            with Not_found ->
	      IDENT s
      }

  | ['0'-'9']+ ('.' ['0'-'9']+)? ['e' 'E'] ['+' '-']? ['0'-'9']+
      { 
	let s = Lexing.lexeme lexbuf in
	match Str.split (Str.regexp "['e' 'E']") s with
	  mantissa :: exponent :: [] ->
	    APPROXIMATE_NUM
	      ((float_of_string mantissa), int_of_string exponent)
	| _ ->
	    raise (Failure ("Bad approximate_num_lit"))
      }	

  | ['0'-'9']+ '.' (['0'-'9']+)?
      { SIMPLE_FLOAT (float_of_string (Lexing.lexeme lexbuf))}

  | ['0'-'9']+
      { INT (int_of_string (Lexing.lexeme lexbuf))}
      
  | "," { T_COMMA }
  | "&" { T_AMPERSAND }
  | "*" { T_ASTERISK }
  | "''" { T_DBQUOTE }
  | "'" { T_QUOTE }
  | ":" { T_COLON }
  | "." { T_DOT }
  | "=" { T_EQUALS }
  | "<" { T_LESS }
  | ">" { T_GREATER }
  | "!" { T_BANG }
  | "+" { T_PLUS }
  | "-" { T_MINUS }
  | "/" { T_DIV }
  | ";" { T_SEMICOLON }
  | "_" { T_UNDERSCORE }
  | "@" { T_AROBAS }
  | "%" { T_PERCENT }

(* Parenthesis *)
  | "(" { LPAR }
  | ")" { RPAR }

(* brackets *)
  | "[" { LBRA }
  | "]" { RBRA }

(* strings *)
  | "\""
      {
	reset_string_buffer ();
        let string_start = lexbuf.lex_start_pos + lexbuf.lex_abs_pos in 
        begin try
          string lexbuf
        with 
	  Failure s ->
	    raise (Failure s)
        end;
        lexbuf.lex_start_pos <- string_start - lexbuf.lex_abs_pos ;
        STRING (get_buffer_string ()) 
      }

  | eof  { EOF }
  | _
      { EOF }
      (*
      { raise (Failure "Lexing error")
      *)

and string = parse
    "\\\""
    {
      add_char_to_buffer '"';
      string lexbuf 
    } 
  | "\""
      {
        ()
      }
      
  | eof 
      { raise (Failure "String not terminated") }
  | _
      { 
	let c = (Lexing.lexeme_char lexbuf 0) in
        add_char_to_buffer c;
        if c = '\010' then incr line_number;
        string lexbuf 
      }
