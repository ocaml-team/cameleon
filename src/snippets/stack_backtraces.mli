(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** A Chamo view to browse stack backtraces, in case of uncaught exception.
  The view reads the stack backtraces from a file. To automatically
  use this view, proceed as follows: suppose you redirect the output
  of your execution in a file foo.sbt, associate the stack_bactraces view factory
  to these files, in your [~/.cameleon2/chamo.view] file, in the
  [view_from_filename_patterns] variable, by adding
  [(".*\\.log$", stack_backtraces)] to the list of associations.

  Then, in chamo, when you a open a [.sbt] file, the new view is displayed
  with the list of traces showing where the uncaught exception appeared.
  Selecting a trace automatically opens the source file at the location
  of the exception (if the file can be found from where you launched chamo).

  The view automatically checks every 2 seconds if the file was modified.
  If the file cannot be found, this delay is 5 seconds before the next check.

  Simply put this piece of code in your [~/.cameleon2/chamo_init.ml] file to use
  it.

  @cgname Cameleon.Stack_backtraces
  @version 0.2
  @author Maxence Guesdon
  *)

val factory_name : string
type raise_kind = Raised | Reraised | Called | Primitive
type trace = {
  raise_kind : raise_kind;
  file : string;
  line : int;
  chars : int * int;
}
val string_of_raise_kind : raise_kind -> string
val trace_of_line : string -> trace option
val trace_list_of_file : string -> string * trace list

class view : Ed_view.topwin ->
  string ->
    object
      val mutable label : string
      val mutable on_destroy : unit -> unit
      val mutable on_label_change : string -> unit
      val mutable time : float
      method attributes : (string * string) list
      method box : GObj.widget
      method close : unit
      method copy : (unit -> unit) option
      method cut : (unit -> unit) option
      method destroy : unit
      method dup : Ed_view.topwin -> Ed_view.gui_view option
      method filename : string
      method grab_focus : unit
      method key_bindings : (Okey.keyhit_state * string) list
      method kind : string
      method label : string
      method menus : (string * GToolbox.menu_entry list) list
      method paste : (unit -> unit) option
      method reload : (unit -> unit) option
      method save : (unit -> unit) option
      method save_as : (unit -> unit) option
      method set_label : string -> unit
      method set_on_destroy : (unit -> unit) -> unit
      method set_on_focus_in : (unit -> unit) -> unit
      method set_on_label_change : (string -> unit) -> unit
      method update : unit
    end
val create_view : Ed_view.topwin -> string -> view
val open_file :
  Ed_view.topwin ->
  'a -> ?attributes:'b -> string -> [> `New_view of Ed_view.gui_view ]
class factory : Ed_view.view_factory
