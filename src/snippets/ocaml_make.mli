(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(**
   A command to launch "make" (on optional targets) and, in case of error, parse
   the error output looking for ocaml compilation error messages. The first
   error message is used to open the indicated file at the indicated location.
   Just add a shortcut like "C-m" in the ocaml mode configuration file to use it.

   To handle some warnings (i.e. to automatically open a file at a location
   when a warning is indicated), set the global variable
   [warn_error] to a string defining the warnings of interest,
   the same way as with the [-w] option of the OCaml compiler. This can be done
   by putting in your [~/.cameleon2/chamo_init.ml] file something like:
   {[ Cam_commands.set_global "warn_error" "FSE" ]}

   @cgname Cameleon.Ocaml_make
   @version 0.1
   @author Maxence Guesdon
*)

