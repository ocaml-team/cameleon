(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

let remove_trailing_whitespace args =
  match !Ed_gui.active_window with
    None -> ()
  | Some w ->
      match w#active_view with
        None -> ()
      | Some av ->
          begin
            match !Ed_sourceview.active_sourceview with
              None -> ()
            | Some v ->
                if Oo.id av = Oo.id v then
                  begin
                    let old_loc = v#file#location in
                    v#file#buffer#place_cursor v#file#buffer#start_iter;
                    let com = "sourceview_query_replace_re \"[ \t]+\n\" \"\n\"" in
                    Cam_commands.eval_command com;
                    v#set_location old_loc;
                  end
          end
;;
let _ =
  let com name = Cam_commands.create_com
    name [| |] remove_trailing_whitespace
  in
  Cam_commands.register_before (com "save_active_view");
  Cam_commands.register_before (com "save_active_view_as")



