(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

let oug_filename_var = "ougsource";;

let oug_data = ref (Ouglib.Data.create(), 0.0)

module D = Ouglib.Data

type file_elts = (int * int * D.elt_id) list
  (* list of (start_pos, end_pos, elt_id) *)

type loc_table = (string, file_elts) Hashtbl.t;;
let (loc_table : loc_table) = Hashtbl.create 111;;

let rec add_to_file_elts start_pos end_pos elt_id = function
  [] -> [start_pos, end_pos, elt_id]
| (sp, ep, _) as x :: q ->
    if start_pos < sp || (start_pos = sp && end_pos > ep) then
      (start_pos, end_pos, elt_id) :: x :: q
    else
      x :: (add_to_file_elts start_pos end_pos elt_id q)
;;

let fill_loc_table oug_data =
  Hashtbl.clear loc_table ;
  let f elt_id elt =
    match elt.D.kind with
      D.Class -> ()
    | _ ->
        let fname = elt.D.loc.D.loc_start.Lexing.pos_fname in
        let fname = Filename.basename fname in
        let l =
          try Hashtbl.find loc_table fname
          with Not_found -> []
        in
        let start_pos = elt.D.loc.D.loc_start.Lexing.pos_cnum in
        let end_pos = elt.D.loc.D.loc_end.Lexing.pos_cnum in
        let l = add_to_file_elts start_pos end_pos elt_id l in
        Hashtbl.replace loc_table fname l
  in
  Ouglib.Earray.iteri f oug_data.D.elements;
  prerr_endline (Printf.sprintf "oug: %d elements added"
   (Ouglib.Earray.length oug_data.D.elements))
;;

let rec elt_id_by_loc pos = function
  [] -> raise Not_found
| (sp, ep, elt_id) :: q ->
    if sp <= pos && pos <= ep then
      elt_id
    else
      elt_id_by_loc pos q
;;

let elt_id_on_cursor filename pos =
  (* FIXME: handle mode display mapping *)
  let filename = Filename.basename filename in
  try
    let l = Hashtbl.find loc_table filename in
    let elt_id = elt_id_by_loc pos l in
    elt_id
  with
    Not_found ->
      let msg = Printf.sprintf
        "No element in file \"%s\", character %d" filename pos
      in
      failwith msg
;;

let on_active_sourceview f args =
  match !Ed_sourceview.active_sourceview with
    None -> ()
  | Some v ->
      try
        let b = v#source_buffer in
        let iter = b#get_iter `INSERT in
        let pos = iter#offset in
        let elt_ids =
          if Array.length args <= 0 then
            [elt_id_on_cursor v#filename pos]
          else
            begin
              let filter = Ouglib.Analyze.parse_filter args.(0) in
              Ouglib.Lang.filter_elements (fst !oug_data) filter
            end
        in
        f elt_ids
      with
        Failure s ->
          Ed_misc.error_message (Ed_misc.to_utf8 s);
          failwith s
      | Ouglib.Lang.Parse_error (s, pos) ->
          let msg = Printf.sprintf "Character %d: %s" pos s in
          Ed_misc.error_message (Ed_misc.to_utf8 msg);
          failwith msg
;;

let print =
  let f elt_id =
    let data = fst !oug_data in
    let elt = Ouglib.Earray.get data.D.elements elt_id in
    Ed_misc.display_message
      (Ed_misc.to_utf8 (D.string_of_absname elt.D.name))
  in
  let print_list = List.iter f in
  on_active_sourceview print_list
;;

module L = Ouglib.Lang ;;

let goto_elt elt =
  let fname = elt.D.loc.D.loc_start.Lexing.pos_fname in
  match fname with
    "" ->
      Ed_misc.display_message (Ed_misc.to_utf8 "No filename for this element.")
  | _ ->
      let line = elt.D.loc.D.loc_start.Lexing.pos_lnum in
      let char = elt.D.loc.D.loc_start.Lexing.pos_cnum -
        elt.D.loc.D.loc_start.Lexing.pos_bol
      in
      let com = Printf.sprintf "open_file %s %d,%d"
        fname (line-1) char
      in
      Cam_commands.eval_command com
;;

let goto f_filter =
  let f elt_ids =
    let data = fst !oug_data in
    let filter = f_filter elt_ids in
    let elt_ids = L.filter_elements data filter in
    let elts = List.map (Ouglib.Earray.get data.D.elements) elt_ids in
    let entries = List.map
      (fun elt ->
         let label =
           Cam_misc.escape_menu_label
             (Ed_misc.to_utf8 (D.string_of_absname elt.D.name))
         in
         let go () = goto_elt elt in
         `I (label, go)
      )
      elts
    in
    GToolbox.popup_menu ~entries ~button: 1 ~time: Int32.zero
  in
  (on_active_sourceview f : string array -> unit)
;;

let goto_prev =
  let f elt_ids = L.Ref ([], L.Internal_set elt_ids, None, Some 1) in
  goto f
;;

let goto_succ =
  let f elt_ids = L.From (L.Internal_set elt_ids, [], None, Some 1) in
  goto f
;;

let load_data file =
  let data = Ouglib.Dump.load_data file in
  oug_data := (data, Unix.time());
  fill_loc_table data;;

let rec watch_load_data file () =
  try
    let mod_date = (Unix.stat file).Unix.st_mtime in
    let (_,date) = !oug_data in
    if date < mod_date then
    (try load_data file with _ -> ());
    ignore(GMain.Timeout.add ~ms: 5000 ~callback: (watch_load_data file));
    false
  with
    Unix.Unix_error _ -> false
;;

let start_oug_watch ?file () =
  let file =
    match file with
      Some f -> Some f
    | None ->
        try Some (Cam_commands.get_global oug_filename_var)
        with Not_found -> None
  in
  match file with
    None -> ()
  | Some file -> ignore(watch_load_data file ())
;;

let targets = [
  { Gtk.target = "STRING"; flags = []; info = 0};
  { Gtk.target = "text/plain"; flags = []; info = 0};
  { Gtk.target = "text/uri-list"; flags = []; info = 2};
]
;;

class oug_elts_box title f_data =
  let wintitle = Ed_misc.to_utf8
    (Printf.sprintf "Oug: %s" title)
  in
  let win = GWindow.window ~title: wintitle ~width: 400 ~height: 600 () in
  let vbox = GPack.vbox ~packing: win#add () in
  let table = GPack.table
    ~rows: 2 ~columns: 2
      ~packing: (vbox#pack ~expand: false ~fill: true) ()
  in
  let _ = GMisc.label ~text: "Filter:"
    ~yalign: 0.0
      ~xalign: 1.0 ~xpad: 3
      ~packing: (table#attach ~left: 0 ~top: 0)
    ()
  in
  let _ = GMisc.label ~text: (Ed_misc.to_utf8 title)
    ~yalign: 0.0
    ~xalign: 0.0 ~xpad: 3
      ~line_wrap: true
      ~packing: (table#attach ~expand: `BOTH ~left: 1 ~top: 0)
      ()
  in
  let _ = GMisc.label ~text: "Element(s) filtered:"
    ~yalign: 0.0
      ~xalign: 1.0 ~xpad: 3
      ~packing: (table#attach ~left: 0 ~top: 1)
      ()
  in
  let nb_elts = GMisc.label
    ~yalign: 0.0
      ~xalign: 0.0
      ~xpad: 3
      ~packing: (table#attach ~left: 1 ~top: 1)
      ()
  in
  let f_kind elt_id =
    Ed_misc.to_utf8
      (String.make 1
       (Ouglib.Data.char_of_elt_kind
        (Ouglib.Earray.get (fst !oug_data).D.elements elt_id).D.kind))
  in
  let f_name elt_id =
    Ed_misc.to_utf8
      (Ouglib.Data.string_of_absname
       (Ouglib.Earray.get (fst !oug_data).D.elements elt_id).D.name)
  in
  let columns = [
      None, Gmylist.String f_kind ;
      None, Gmylist.String f_name ;
    ]
  in
  object(self)
    inherit [D.elt_id] Gmylist.plist `SINGLE columns false as plist
    method update =
      let l = f_data () in
      self#update_data l;
      let len = List.length l in
      nb_elts#set_text (string_of_int len)

    method on_enter () =
      match plist#selection with
        [] -> ()
      | elt_id :: _ ->
          let elt = Ouglib.Earray.get (fst !oug_data).D.elements elt_id in
          goto_elt elt

    method on_double_click _ = self#on_enter()

    initializer
      let data_get _ sel ~info ~time =
        match self#selection with
          elt_id :: _ ->
            let name = f_name elt_id in
            sel#return ?typ: None ?format: None (Ed_misc.to_utf8 name)
        | [] -> ()
      in
      view#drag#source_set targets
        ~modi:[`BUTTON1 ] ~actions:[`COPY ];
      ignore(self#view#drag#connect#data_get ~callback: data_get);

      vbox#pack ~expand: true plist#box;
      let hbox = GPack.hbox ~packing: vbox#pack () in
      let wb_refresh = GButton.button ~stock: `REFRESH
        ~packing: (hbox#pack ~expand: true) () in
      let wb_close = GButton.button ~stock: `CLOSE
        ~packing: (hbox#pack ~expand: true) () in
      ignore(wb_close#connect#clicked win#destroy);
      ignore(wb_refresh#connect#clicked (fun () -> self#update));
      self#update ;
      win#show ()
  end;;

let com_oug_filter args =
  if Array.length args <= 0 then
    ()
  else
    begin
      try
        let filter = Ouglib.Analyze.parse_filter args.(0) in
        let f_data () = Ouglib.Lang.filter_elements (fst !oug_data) filter in
        ignore (new oug_elts_box args.(0) f_data)
      with
        Failure s ->
          Ed_misc.error_message (Ed_misc.to_utf8 s);
          failwith s
      | Ouglib.Lang.Parse_error (s, pos) ->
          let msg = Printf.sprintf "Character %d: %s" pos s in
          Ed_misc.error_message (Ed_misc.to_utf8 msg);
          failwith msg
    end
;;

let com_start_oug args =
  let file = if Array.length args > 0 then Some args.(0) else None in
  start_oug_watch ?file ()
;;

let _ =
  let com = Cam_commands.create_com
    "oug_start_watch" [| "ougfile"|] com_start_oug
  in
  Cam_commands.register com;;

let _ =
  let com = Cam_commands.create_com
    "oug_lookup" [| |] print
  in
  Cam_commands.register com;;

let _ =
  let l = [
      "oug_goto_prev", goto_prev ;
      "oug_goto_succ", goto_succ ;
    ]
  in
  List.iter
    (fun (name, f) ->
       let com = Cam_commands.create_com name [| |] f in
       Cam_commands.register com
    )
    l
;;

let _ =
  let com = Cam_commands.create_com
    "oug_filter" [| "filter"|] com_oug_filter
  in
  Cam_commands.register com;;
