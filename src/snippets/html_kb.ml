(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Dumping key bindings into a HTML document. *)

let header = ref "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"  \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">
<html>
<head>
<title>Chamo key bindings</title>
<meta content=\"text/html; charset=iso-8859-1\" http-equiv=\"Content-Type\"/>
<style type=\"text/css\">
body {
font-family:\"Trebuchet MS\", Arial, Verdana;
font-size: 1.0em;
color: #333333;
}
h1 { padding-left: 4em ; color: brown; }
td.mode {
  font-size: 12pt;
  font-weight: bold;
  margin-top: 3px;
  background-color: brown;
  color: white;
  border-style:solid;
  border-color: brown;
  border-width: 2px;
  padding-left: 6px;
}

td.group {
  padding-left: 6px;
  padding-right: 6px;
  vertical-align: text-top;
}
table.bindings {
  width: 100%;
  font-size: 8pt;
  border-width : 2px;
  border-style: solid ;
  border-color: brown;
  border-collapse: collapse;
  margin-bottom: 1em;
}
table.bindings td {
/*  border-style: solid;
  border-width: 1px; 1px; 1px; 2px;
  border-color: black black black brown;*/
}

td.keystate {
  white-space: nowrap;
  font-family: Monospace, Courier;
  font-weight: bold;
  border-width: 1px; 1px; 1px; 2px;
  border-color: black black black brown;

  padding-right: 1em;
  padding-left: 1em;
}
td.command {
  white-space: nowrap;
  font-family: Monospace, Courier;
  font-weight: bold;
  padding-right: 1em;
  padding-left: 1em;
  border-width: 1px; 2px; 1px; &px;
  border-color: black brown black black;
}
</style>
</head>
<body>
<div id=\"page\">"
;;

let footer = ref "</div></body></html>"

let escape_html s =
  let l =
    [ "<", "&lt;" ;
      ">", "&gt;" ;
      "\"", "&quot;" ;
    ]
  in
  List.fold_left
    (fun s (pat, subs) -> Cam_misc.replace_in_string ~pat ~subs ~s)
    s
    l
;;

let html_of_bindings mode bindings =
  let b = Buffer.create 256 in
  Buffer.add_string b "<table class=\"bindings\">";
  Printf.bprintf b
    "<tr><td> </td><td class=\"mode\">%s</td></tr>\n"
    (escape_html mode);
  let f (ks, command) =
    Printf.bprintf b
      "<tr><td class=\"keystate\">%s</td><td class=\"command\">%s</td></tr>\n"
      (Ed_keymaps.string_of_state ks)
      (escape_html command)
  in
  List.iter f bindings;
  Buffer.add_string b "</table>";
  Buffer.contents b


let print_html_bindings file groups =
  let groups =
    Array.of_list (List.map Array.of_list groups)
  in
  let get i j =
    let len = Array.length groups.(i) in
    if j >= len then None else Some groups.(i).(j)
  in
  let max_height =
    Array.fold_left
      (fun acc t -> max acc (Array.length t))
      0
      groups
  in
  let b = Buffer.create 256 in
  Buffer.add_string b "<table><tr>\n";
  for i = 0 to Array.length groups - 1 do
    Buffer.add_string b "<td class=\"group\">";
    for j = 0 to max_height - 1 do
      begin
        match get i j with
          None -> ()
        | Some (name,bindings) ->
            Buffer.add_string b (html_of_bindings name bindings)
      end;
    done;
    Buffer.add_string b "</td>";
  done;
  Buffer.add_string b "</tr></table><br/>\n";

  let oc = open_out file in
  output_string oc !header;
  output_string oc "<h1>Chamo key bindings: quick reference</h1>";
  output_string oc (Buffer.contents b);
  output_string oc !footer;
  close_out oc
;;

let arrange_bindings l =
  let max_nb = List.fold_left
    (fun acc (_,l) -> max acc (List.length l))
    0
    l
  in
  let rec iter groups later (size, current) = function
    [] ->
      begin
        match current with
          [] -> List.rev groups
        | _ ->
            iter ((List.rev current) :: groups) [] (0, [])
              (List.rev later)
      end
  | (name, kb) :: q ->
        if List.length kb <= 0 then
        (* remove empty lists *)
        iter groups later (size, current) q
      else
        begin
          let len = List.length kb + 2 (* +2 for the title *) in
          if len <= max_nb && size + len > max_nb then
            iter groups ((name,kb)::later) (size, current) q
          else
            iter groups later (size+len, ((name,kb)::current)) q
        end
  in
  iter [] [] (0, []) l
;;

let print_bindings file l =
  let groups = arrange_bindings l in
  print_html_bindings file groups
;;

let cut_lists l max_size =
  let build_name name i =
    if i <= 1 then name else Printf.sprintf "%s (%d)" name i
  in
  let rec iter name acc current i n = function
    [] -> List.rev (((build_name name i), current) :: acc)
  | h :: q ->
      if n > max_size then
        iter name (((build_name name i), current)::acc) [] (i+1) 0 q
      else
        iter name acc (h::current) i (n+1) q
  in
  let f_list (name, lkb) = iter name [] [] 1 0 lkb in
  List.flatten (List.map f_list l)
;;

let print_key_bindings_com args =
  let len = Array.length args in
  if len >= 1 then
    begin
      let l =
        [ ("Window", Ed_gui_rc.window_key_bindings#get);
          ("Minibuffer", Ed_minibuffer_rc.key_bindings#get);
          ("Sourceview", Ed_sourceview_rc.key_bindings#get) ;
          ("Odoc", Ed_odoc_rc.key_bindings#get) ;
          ("Multiclip", Ed_multiclip_rc.key_bindings#get) ;
          ("Todo list", Ed_tdl_rc.key_bindings#get) ;
        ] @
          (List.map
           (fun name ->
              (Printf.sprintf "%s mode" name,
               let m = Ed_sourceview.get_mode name in
               m#key_bindings
              )
           )
             (Ed_sourceview.available_mode_names ())
          )
      in
      let l = List.sort
        (fun (_,l1) (_,l2) -> compare (List.length l2) (List.length l1))
          l
      in
      let l =
        if len >= 2 then
          begin
            let max_size = int_of_string args.(1) in
            cut_lists l max_size
          end
        else
          l
      in
      print_bindings args.(0) l
    end
;;

let _ =
  let com = Cam_commands.create_com "html_of_key_bindings"
    [| "file" |] print_key_bindings_com
  in
  Cam_commands.register com