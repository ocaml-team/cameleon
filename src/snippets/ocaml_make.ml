(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

let output_name = "make";;
let make_output = ref None;;
let make_output () =
  match !make_output with
    None ->
      let o = new Ed_outputs.text_output
        ~on_destroy: (fun () -> make_output := None)
          output_name
      in
      make_output := Some o ;
      o
  | Some o -> o
;;

let ocaml_make args =
  let (dir, targets) =
    match Array.length args with
      n when n <= 0 -> (Sys.getcwd (), "")
    | 1 -> (args.(1), "")
    | n -> (args.(1), String.concat " " (Array.to_list (Array.sub args 1 (n-1))))
  in
  let command = Printf.sprintf "cd %s ; make %s"
    (Filename.quote dir) targets
  in
  begin
    match !Ed_sourceview.active_sourceview with
      None -> ()
    | Some v -> v#file#write_file ()
  end;
  Ed_ocamlbuild.run ~output: (make_output()) command;

;;

Cam_commands.register (Cam_commands.create_com "ocaml_make" [| "directory ; targets"|] ocaml_make);;
