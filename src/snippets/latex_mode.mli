(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** This module defines a LaTeX mode for Chamo:
   - a "latex" output (using {!Ed_outputs.interactive_output}),
   - a "show_pdf" command launching xpdf on the pdf file corresponding the
     .tex file of the active sourceview,
   - a "pdflatex" command to run the pdflatex command on the .tex file
     of the active sourceview,
   - a "latex_make_html" command to run "make html" in the directory
     of the .tex file of the active sourceview,
   - a "latex_insert_itemize" command to insert an itemize template in the
     file of the active sourceview,
   - a "latex_goto_section" command which analyses the file in the active
     sourceview and pops up a menu to jump the defined \part, \chapter, \section,
     \subsection or \subsubsection items,
   - a "latex" mode, with defaults key bindings to call the commands.
   The bindings are stored in a ~/.cameleon2/chamo.sourceview.mode.latex file.

   @cgname Cameleon.Chamo.Latex_mode
   @version 0.1
   @author Maxence Guesdon
*)