(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Main module of the cam-config utility *)

let opt_l = "\tprint the directory of the "^Cam_installation.software^" libraries"
let opt_b = "\tprint the directory of the "^Cam_installation.software^" executables"
let opt_m = "\tprint the directory of the "^Cam_installation.software^" man pages"
let opt_t = "\tprint the directory of the "^Cam_installation.software^" templates"
let opt_p = "\tprint the directory of the "^Cam_installation.software^" plugins"
let opt_q = "\tprint the directory of the "^Cam_installation.software^" pixmaps"
let opt_g = "\tprint the directory of the "^Cam_installation.software^" glade files"
let opt_v = "\tprint the version of "^Cam_installation.software
let opt_greater = "v\n\texit with 0 is cameleon version >= v orelse 1"

let usage ="Usage : "^Sys.argv.(0)^" [option] \nwhere option can be one of the following :"

let p s = (fun () ->  print_endline s ; exit 0)

let greater required =
  let current =
    let v = Cam_installation.software_version in
    try
      let p = String.index v  '+' in
      String.sub v 0 p
    with Not_found -> v
  in
  let f s =
    try
      let p = String.index_from s 0 '.' in
      let major = int_of_string (String.sub s 0 p) in
      try
	let p2 = String.index_from s (p+1) '.' in
	let minor = int_of_string (String.sub s (p+1) (p2 - p - 1)) in
	try
          let len = String.length s in
          let add = int_of_string (String.sub s (p2+1) (len - p2 - 1)) in
          (major, minor, add)
	with
          _ ->
            (major, minor, 0)
      with
	_ ->
          (major, 0, 0)
    with _ -> (0,0,0)
  in
  let (major,minor,add) = f current in
  let (major_r, minor_r, add_r) = f required in
  if major > major_r or
    (major = major_r && (minor > minor_r or
			 (minor = minor_r && add >= add_r))) then
    exit 0
  else
    exit 1


let options = [
  "-l", Arg.Unit (p Cam_installation.lib_dir), opt_l ;
  "-b", Arg.Unit (p Cam_installation.bin_dir), opt_b ;
  "-m", Arg.Unit (p Cam_installation.man_dir), opt_m ;
  "-t", Arg.Unit (p Cam_installation.templates_dir), opt_t ;
  "-p", Arg.Unit (p Cam_installation.plugins_dir), opt_p ;
  "-q", Arg.Unit (p Cam_installation.pixmaps_dir), opt_q ;
  "-g", Arg.Unit (p Cam_installation.glade_dir), opt_g ;
  "-v", Arg.Unit (p Cam_installation.software_version), opt_v ;
  "--greater", Arg.String greater, opt_greater ;
]

let _ = Arg.parse options
    (fun _ -> ())
    usage
