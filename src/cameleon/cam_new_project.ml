(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(* $Id: cam_new_project.ml 758 2011-01-13 07:53:27Z zoggy $ *)

type project_kind = [
    `Library
  | `Tool
  | `Tool_and_library
  ]

let mod_args = "args"
let mod_config = "config"
let mod_constant = "constant"
let mod_main = "main"
let mod_messages = "messages"
let mod_misc = "misc"
let mod_types = "types"

let default_ocaml_modules =
  [
    (* filename postfix, description, templates for mli, templates for ml *)
    (mod_args, "Command line arguments", [], []);
    (mod_config, "Configuration values", [], []);
    (mod_constant, "Constants", [], []);
    (mod_main, "Main module", [], []);
    (mod_messages, "Messages", [], []);
    (mod_misc, "Misc functions", [], []);
    (mod_types, "Basic types", [], []);
  ]

type newp_options = {
    mutable n_name : string ;
    mutable n_file_prefix : string ;
    mutable ocaml_modules : (string * string * string list * string list) list;
  }
let new_project args = ()

let _ = Cam_commands.register
    {
      Cam_commands.com_name = Cam_constant.com_new_project ;
      Cam_commands.com_args = [| |] ;
      Cam_commands.com_more_args = None ;
      Cam_commands.com_f = new_project ;
    }
