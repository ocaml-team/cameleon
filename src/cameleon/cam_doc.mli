(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(* $Id: cam_doc.mli 758 2011-01-13 07:53:27Z zoggy $ *)

(** Handling dump of documentation. *)

type doc_source = {
  mutable ds_file : string;
  mutable ds_label_com : (string * string) option;
}

val font_doc_code : Config_file.string_cp
val font_doc_code_bold : Config_file.string_cp
val font_doc_bold : Config_file.string_cp
val font_doc_normal : Config_file.string_cp
val color_doc_type : Config_file.string_cp
val color_doc_keyword : Config_file.string_cp
val color_doc_constructor : Config_file.string_cp
val color_doc_code : Config_file.string_cp

val doc_bookmarks : string Config_file.list_cp
val add_doc_bookmark : string -> unit

val com_next_element : string
val com_prev_element : string
val com_follow_link : string
val com_follow_link_in_new : string
val com_close : string
val com_search : string
val com_search_backward : string
val com_back : string
val com_add_bookmark : string
val com_home : string
val com_end : string
val com_menu : string
val doc_browser_actions : string list

val keymap_doc : ((Gdk.Tags.modifier list * int) * string) Config_file.list_cp

val init_keymaps : unit -> unit

type element =
    E_Type of string
  | E_Class of string
  | E_Class_type of string
  | E_Exception of string
  | E_Module of string
  | E_Module_type of string
  | E_Value of string
  | E_Attribute of string
  | E_Method of string
  | E_Section of string

val max_menu_length : int

val default_doc_modules : Odoc_info.Module.t_module list ref

val load_doc_files : string list -> Odoc_info.Module.t_module list

val update :
  reload: bool -> Odoc_info.Module.t_module list ref ->
  (Odoc_info.Module.t_module list ref -> element -> unit) ->
    (Odoc_info.Module.t_module list ref -> unit) ->
      (Odoc_info.Module.t_module list ref -> unit) -> GMenu.menu -> unit

val get_module :
    Odoc_info.Module.t_module list ->
      string -> Odoc_info.Module.t_module option
val get_module_type :
    Odoc_info.Module.t_module list ->
      string -> Odoc_info.Module.t_module_type option
val get_module_of_type :
    Odoc_info.Module.t_module list ->
      Odoc_info.Name.t -> Odoc_info.Module.t_module option
val get_module_type_of_type :
    Odoc_info.Module.t_module list ->
      Odoc_info.Name.t -> Odoc_info.Module.t_module_type option
val get_module_of_exception :
    Odoc_info.Module.t_module list ->
      Odoc_info.Name.t -> Odoc_info.Module.t_module option
val get_module_type_of_exception :
    Odoc_info.Module.t_module list ->
      Odoc_info.Name.t -> Odoc_info.Module.t_module_type option
val get_module_of_value :
    Odoc_info.Module.t_module list ->
      Odoc_info.Name.t -> Odoc_info.Module.t_module option
val get_module_type_of_value :
    Odoc_info.Module.t_module list ->
      Odoc_info.Name.t -> Odoc_info.Module.t_module_type option
val get_class :
    Odoc_info.Module.t_module list ->
      string -> Odoc_info.Class.t_class option
val get_class_type :
    Odoc_info.Module.t_module list ->
      string -> Odoc_info.Class.t_class_type option
val get_class_of_attribute :
    Odoc_info.Module.t_module list ->
      Odoc_info.Name.t -> Odoc_info.Class.t_class option
val get_class_of_method :
    Odoc_info.Module.t_module list ->
      Odoc_info.Name.t -> Odoc_info.Class.t_class option
val get_class_type_of_attribute :
    Odoc_info.Module.t_module list ->
      Odoc_info.Name.t -> Odoc_info.Class.t_class_type option
val get_class_type_of_method :
    Odoc_info.Module.t_module list ->
      Odoc_info.Name.t -> Odoc_info.Class.t_class_type option

(** {2 Configuring doc sources} *)

val config_doc_sources : f_update_menu: (unit -> unit) -> unit
