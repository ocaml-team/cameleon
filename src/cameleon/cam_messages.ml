(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(* $Id: cam_messages.ml 758 2011-01-13 07:53:27Z zoggy $ *)


let usage_browser ="Usage: "^Sys.argv.(0)^" <file1.odoc> [<file2.odoc> ...]"
let usage = "Usage: "^Sys.argv.(0)^" [options]\nwhere options are:"

let home = Cam_installation.home

let login =
  try Sys.getenv "USER"
  with Not_found -> Filename.basename home

let rc_dir = Cam_installation.rc_dir

let plugins_dir =
  let d = Filename.concat rc_dir "plugins" in
  let _ =
    try (* A VOIR avec le CDK Unix2.safe_mkdir *) Unix.mkdir d 0o700
    with _ -> ()
  in
  d

let software_author = "Maxence Guesdon"
let software_author_mail = "Maxence.Guesdon@inria.fr"
let software_copyright =
  "Copyright 2004-2010 Institut National de Recherche en \n"^
  "Informatique et en Automatique. All rights reserved.\n"^
  "This software is distributed under the terms of the\n"^
  "GNU Library General Public License version 2.\n"^
  "(see file LICENSE in the distribution)"

let about = "About "^Cam_installation.software^" ..."

let bad_format s = Printf.sprintf "Unexpected XML: %s" s
let loading_file f = Printf.sprintf "Loading file %s" f
let ok = "Ok"
let yes = "Yes"
let no = "No"
let confirm = "Confirm"
let cancel = "Cancel"
let copy = "Copy"
let cut = "Cut"
let paste = "Paste"
let edit = "Edit"
let up = "Up"
let down = "Down"
let add_menu = "Add menu"
let add_command = "Add command"
let separator = "Separator"
let menu = "Menu"
let add = "Add"
let remove = "Remove"

let configuration = "Configuration"
let doc = "Doc"
let quit = "Quit"
let really_quit = "Really quit "^Cam_installation.software^" ?"
let log_window = "Log window"
let file = "File"
let search_exact = "Exact search"
let search_regexp = "Regexp search"
let running_com com = "Running "^com^" ..."
let error = "Error"
let error_exec com = "Error while executing command\n"^com
let back = "Back"
let search = "Search"
let error_not_found ele name = ele^" "^name^" not found."
let error_not_found_module n = error_not_found "Module" n
let error_not_found_module_type n = error_not_found "Module type" n
let error_not_found_mmt n = error_not_found "Module or module type" n
let error_not_found_class n = error_not_found "Class" n
let error_not_found_class_type n = error_not_found "Class type" n
let error_not_found_cct n = error_not_found "Class or class type" n
let error_not_found_exception n = error_not_found "Exception" n
let error_not_found_type n  = error_not_found "Type" n
let error_not_found_value n = error_not_found "Value" n
let error_not_found_attribute n = error_not_found "Attribute" n
let error_not_found_method n = error_not_found "Method" n
let error_load_file f s = Printf.sprintf "Error while loading %s: %s" f s
let authors = "Authors"
let version = "Version"
let since = "Since"
let raises = "Raise"
let see_also = "See also"
let returns = "Return"
let deprecated = "Deprecated"
let parameters = "Parameters"
let inherits = "Inherits"
let doc_box = "Doc box"
let close = "Close"
let name = "Name"
let kind = "Kind"
let nothing_found s = "Nothing found for "^s^"."
let error_unknown_action a = "Unknown action "^a^"."
let label = "Label"
let command = "Command"
let doc_flag = "Use this menu as doc menu"
let icon_file = "Icon file"
let icon = "Icon"
let file_type = "File type"
let rule = "Rule"
let file_types_rules = "File types rules"
let commands = "Commands"
let default_command = "Default command"
let file_types_handlers = "File types handlers"
let open_with = "Open with..."
let use_ = "use..."
let doc_file = "Documentation file"
let doc_files = "Documentation files"
let doc_sources = "Documentation sources"
let common_keyboard_shortcuts = "Common keyboard shortcuts"
let docbrowser_keyboard_shortcuts = "Docbrowser keyboard shortcuts"
let binding = "Binding"
let edit_binding = "Edit binding"

let update_dir = "Update directory"
let commit_dir = "Commit directory"
let enter_comment = "Enter a comment"
let enter_comment_commit = "Please enter a comment for commit"
let add_dir = "Add directory"
let should_create_dir d = "Directory\n"^d^"\ndoes not exist.\nCreate it ?"
let tag_dir = "Tag selected directory"
let enter_tag_for_dir d ="Enter a tag for the working revision of files in "^d^" :"
let error_add_files l =
  String.concat "\n"
    ("The following files could not be added:" :: l)
let add_files = "Add file"
let add_files = "Add file"
let add_binary_files = "Add binary file"
let commit_files = "Commit selected files"
let remove_files = "Remove selected files"
let tag_files = "Tag selected files"
let enter_tag_for_files l =
  String.concat "\n"
    ("Enter a tag for the working revision of the following files :" :: l)

let tags_of_file = "List tags of the selected file"
let resolve_conflicts = "Resolve merge conflicts"
let log = "Log"
let tag = "Tag"
let revision = "Revision"
let error_remove_files l =
  String.concat "\n"
    ("The following files could not be removed:" :: l)
let confirm_remove_files l =
  String.concat "\n"
    ("The following files will be removed from the repository and locally deleted :" :: l)
let tags_of file = "Tags of "^file
let last_diff = "Last differences"
let diff_with = "Differences with ..."
let diff_between = "Differences between ..."

let plugins = "Plugins"
let no_plugin_loaded = "No plug-in loaded."
let plugin_to_reload = "Plug-in to reload"

let shared_plugins_to_load =
  "The shared plugins to load (from "^Cam_installation.plugins_dir^")"
let personal_plugins_to_load =
  "The personal plugins to load (from "^plugins_dir^")"
let already_loaded = "Already loaded"

let h_reload_plugin = "Select a loaded plug-in and reload it"
let configure_doc_sources = "Configure doc sources"
