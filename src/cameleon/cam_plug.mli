val login : string
module Constant :
  sig
    val com_history_size : int
    val log_max_size : int
    val com_log_window : string
    val com_new_project : string
    val com_new_file : string
    val com_iconify : string
    val com_deiconify : string
    val com_update_files_view : string
    val com_refresh_view : string
    val com_close_view : string
    val com_edit : string
    val com_edit_with : string
    val com_quit : string
    val com_configure_menus : string
    val com_configure_doc_sources : string
    val com_configure_bbar : string
    val com_configure_plugins : string
    val com_configure_ft_rules : string
    val com_configure_ft_handlers : string
    val com_configure_common_keyboard_shortcuts : string
    val com_configure_docbrowser_keyboard_shortcuts : string
    val com_reload_plugin : string
    val com_display_doc_box : string
    val com_about_box : string
    val com_list_commands : string
    val com_prompt_command : string
    val com_on_exit : string
  end
module Commands :
  sig
    type command = string array -> unit
    type command_desc =
      Cam_commands.command_desc = {
      com_name : string;
      com_args : string array;
      com_more_args : string option;
      com_f : command;
    }
    val register :
      ?table:(string, command_desc) Hashtbl.t ->
      ?replace:bool -> command_desc -> unit
    val register_before :
      ?table:(string, command_desc) Hashtbl.t -> command_desc -> unit
    val register_after :
      ?table:(string, command_desc) Hashtbl.t -> command_desc -> unit
    val get_com :
      ?table:(string, command_desc) Hashtbl.t -> string -> command_desc
    val get_com_or_fail :
      ?table:(string, command_desc) Hashtbl.t -> string -> command_desc
    val string_to_argv : string -> string array
    val argv_to_string : string array -> string
    val launch_command :
      ?history:bool ->
      ?table:(string, command_desc) Hashtbl.t ->
      string -> string array -> unit
    val same_previous_command : unit -> bool
    val ask_launch_command :
      ?history:bool ->
      ?table:(string, command_desc) Hashtbl.t ->
      ?width:int -> string -> string array -> unit
    val eval_command :
      ?history:bool ->
      ?table:(string, command_desc) Hashtbl.t -> string -> unit
    val available_command_names :
      ?table:(string, command_desc) Hashtbl.t -> unit -> string list
    val unit_com : string -> (unit -> unit) -> command_desc
    val create_com :
      string -> ?more:string -> string array -> command -> command_desc
    val set_global : string -> string -> unit
    val get_global : string -> string
    val safe_get_global : string -> string
  end
module Config : sig val debug_level : int val socket_dir : string end
module Dbg : sig val print : ?level:int -> string -> unit end
module Misc :
  sig
    val map_opt : ('a -> 'b) -> 'a option -> 'b option
    val chop_n_char : int -> string -> string
    val my_int_of_string : string -> int
    val list_remove_doubles : ?pred:('a -> 'a -> bool) -> 'a list -> 'a list
    val add_shortcut :
      < connect : < destroy : callback:(unit -> unit) -> GtkSignal.id; .. >;
        event : GObj.event_ops; get_oid : int; .. > ->
      (string * ((unit -> bool) option * (unit -> unit))) list ->
      (Okey.modifier list * Gdk.keysym) * string -> unit
    val file_of_string : file:string -> string -> unit
    val select_in_list :
      ?ok:string ->
      ?cancel:string ->
      ?allow_empty:bool ->
      ?value_in_list:bool ->
      title:string -> choices:string list -> string -> string option
    val remove_char : string -> char -> string
    val treat_gtk_events : unit -> unit
    val get_wm_window_position_offset : unit -> int * int
    val subdirs : string -> string list
    val line_of_char : string -> int -> int
    val char_of_line : string -> int -> int
    val replace_in_string : pat:string -> subs:string -> s:string -> string
    val escape_menu_label : string -> string
    val utf8_index_of_char : string -> int -> int
    val utf8_char_of_index : string -> int -> int
    val utf8_string_length : string -> int
    val utf8_char_of_code : int -> string
  end
module View :
  sig
    type ressource_kind = [ `Dir | `File | `None ]
    type ressource_name = string
    type view_name = string
    class type view =
      object
        method changed : bool
        method close : bool
        method name : view_name
        method refresh : unit
        method ressource : ressource_name
        method ressource_kind : ressource_kind
      end
    class type view_factory =
      object
        method create :
          ressource_name -> string array -> view * GWindow.window
        method create_no_window :
          GWindow.window ->
          ressource_name -> string array -> view * GObj.widget
        method known_ressource_kinds : ressource_kind list
        method name : view_name
      end
    val register_factory : view_factory -> unit
    val open_ressource : ressource_name -> view_name -> string array -> view
    val open_ressource_no_window :
      ressource_name ->
      view_name ->
      string array -> GWindow.window -> (GObj.widget -> unit) -> view
    val refresh_ressource_views : ressource_name -> unit
    val close_ressource_views : ressource_name -> unit
    val available_views : ?kind:ressource_kind -> unit -> view_name list
    class view_window :
      ?allow_shrink:bool ->
      ?width:int ->
      ?height:int ->
      title:string ->
      view ->
      object method vbox : GPack.box method window : GWindow.window end
    val create_view_window :
      ?width:int -> ?height:int -> title:string -> view -> view_window
  end
module Rc :
  sig
    val rc_dir : string
    val core_ini : Config_file.group
    val save_core : unit -> unit
    val load_core : unit -> unit
    val gui_ini : Config_file.group
    val save_gui : unit -> unit
    val load_gui : unit -> unit
    val handle_window : GWindow.window -> string -> unit
    val add_binding :
      < get : ((Gdk.Tags.modifier list * int) * 'a) list;
        set : ((Gdk.Tags.modifier list * int) * 'a) list -> 'b; .. > ->
      string -> 'a -> 'b
  end
module Cam_files :
  sig
    module O :
      sig
        module Raw :
          sig
            type cp =
              Config_file.Raw.cp =
                String of string
              | Int of int
              | Float of float
              | List of cp list
              | Tuple of cp list
              | Section of (string * cp) list
            val of_string : string -> cp
            val to_channel : out_channel -> cp -> unit
          end
        type 'a wrappers =
          'a Config_file.wrappers = {
          to_raw : 'a -> Raw.cp;
          of_raw : Raw.cp -> 'a;
        }
        exception Wrong_type of (out_channel -> unit)
        class type ['a] cp =
          object
            method add_hook : ('a -> 'a -> unit) -> unit
            method get : 'a
            method get_default : 'a
            method get_default_formatted : Format.formatter -> unit
            method get_formatted : Format.formatter -> unit
            method get_help : string
            method get_help_formatted : Format.formatter -> unit
            method get_name : string list
            method get_short_name : string option
            method get_spec : Arg.spec
            method reset : unit
            method set : 'a -> unit
            method set_raw : Raw.cp -> unit
            method set_short_name : string -> unit
          end
        type groupable_cp =
            < get_default_formatted : Format.formatter -> unit;
              get_formatted : Format.formatter -> unit; get_help : string;
              get_help_formatted : Format.formatter -> unit;
              get_name : string list; get_short_name : string option;
              get_spec : Arg.spec; reset : unit; set_raw : Raw.cp -> unit >
        exception Double_name
        exception Missing_cp of groupable_cp
        class group :
          object
            method add : 'a cp -> unit
            method command_line_args :
              section_separator:string -> (string * Arg.spec * string) list
            method read :
              ?obsoletes:string ->
              ?no_default:bool ->
              ?on_type_error:(groupable_cp ->
                              Raw.cp ->
                              (out_channel -> unit) ->
                              string -> in_channel -> unit) ->
              string -> unit
            method write : ?with_help:bool -> string -> unit
          end
        class int_cp :
          ?group:group ->
          string list -> ?short_name:string -> int -> string -> [int] cp
        class float_cp :
          ?group:group ->
          string list -> ?short_name:string -> float -> string -> [float] cp
        class bool_cp :
          ?group:group ->
          string list -> ?short_name:string -> bool -> string -> [bool] cp
        class string_cp :
          ?group:group ->
          string list ->
          ?short_name:string -> string -> string -> [string] cp
        class ['a] list_cp :
          'a wrappers ->
          ?group:group ->
          string list ->
          ?short_name:string -> 'a list -> string -> ['a list] cp
        class ['a] option_cp :
          'a wrappers ->
          ?group:group ->
          string list ->
          ?short_name:string -> 'a option -> string -> ['a option] cp
        class ['a] enumeration_cp :
          (string * 'a) list ->
          ?group:group ->
          string list -> ?short_name:string -> 'a -> string -> ['a] cp
        class ['a, 'b] tuple2_cp :
          'a wrappers ->
          'b wrappers ->
          ?group:group ->
          string list ->
          ?short_name:string -> 'a * 'b -> string -> ['a * 'b] cp
        class ['a, 'b, 'c] tuple3_cp :
          'a wrappers ->
          'b wrappers ->
          'c wrappers ->
          ?group:group ->
          string list ->
          ?short_name:string -> 'a * 'b * 'c -> string -> ['a * 'b * 'c] cp
        class ['a, 'b, 'c, 'd] tuple4_cp :
          'a wrappers ->
          'b wrappers ->
          'c wrappers ->
          'd wrappers ->
          ?group:group ->
          string list ->
          ?short_name:string ->
          'a * 'b * 'c * 'd -> string -> ['a * 'b * 'c * 'd] cp
        class string2_cp :
          ?group:group ->
          string list ->
          ?short_name:string ->
          string * string -> string -> [string, string] tuple2_cp
        class font_cp :
          ?group:group ->
          string list -> ?short_name:string -> string -> string -> string_cp
        class filename_cp :
          ?group:group ->
          string list -> ?short_name:string -> string -> string -> string_cp
        val int_wrappers : int wrappers
        val float_wrappers : float wrappers
        val bool_wrappers : bool wrappers
        val string_wrappers : string wrappers
        val list_wrappers : 'a wrappers -> 'a list wrappers
        val option_wrappers : 'a wrappers -> 'a option wrappers
        val enumeration_wrappers : (string * 'a) list -> 'a wrappers
        val tuple2_wrappers :
          'a wrappers -> 'b wrappers -> ('a * 'b) wrappers
        val tuple3_wrappers :
          'a wrappers ->
          'b wrappers -> 'c wrappers -> ('a * 'b * 'c) wrappers
        val tuple4_wrappers :
          'a wrappers ->
          'b wrappers ->
          'c wrappers -> 'd wrappers -> ('a * 'b * 'c * 'd) wrappers
        class ['a] cp_custom_type :
          'a wrappers ->
          ?group:group ->
          string list -> ?short_name:string -> 'a -> string -> ['a] cp
        type 'a option_class = 'a Config_file.option_class
        type 'a option_record = 'a Config_file.option_record
        type options_file = Config_file.options_file
        val create_options_file : string -> options_file
        val set_options_file : options_file -> string -> unit
        val load : options_file -> unit
        val append : options_file -> string -> unit
        val save : options_file -> unit
        val save_with_help : options_file -> unit
        val option_hook : 'a option_record -> (unit -> unit) -> unit
        val string_option : string option_class
        val color_option : string option_class
        val font_option : string option_class
        val int_option : int option_class
        val bool_option : bool option_class
        val float_option : float option_class
        val string2_option : (string * string) option_class
        val option_option : 'a option_class -> 'a option option_class
        val list_option : 'a option_class -> 'a list option_class
        val sum_option : (string * 'a) list -> 'a option_class
        val tuple2_option :
          'a option_class * 'b option_class -> ('a * 'b) option_class
        val tuple3_option :
          'a option_class * 'b option_class * 'c option_class ->
          ('a * 'b * 'c) option_class
        val tuple4_option :
          'a option_class * 'b option_class * 'c option_class *
          'd option_class -> ('a * 'b * 'c * 'd) option_class
        val ( !! ) : 'a option_record -> 'a
        val ( =:= ) : 'a option_record -> 'a -> unit
        val shortname : 'a option_record -> string
        val get_help : 'a option_record -> string
        type option_value =
          Config_file.option_value =
            Module of option_module
          | StringValue of string
          | IntValue of int
          | FloatValue of float
          | List of option_value list
          | SmallList of option_value list
        and option_module = (string * option_value) list
        val define_option_class :
          string ->
          (option_value -> 'a) -> ('a -> option_value) -> 'a option_class
        val to_value : 'a option_class -> 'a -> option_value
        val from_value : 'a option_class -> option_value -> 'a
        val value_to_string : option_value -> string
        val string_to_value : string -> option_value
        val value_to_int : option_value -> int
        val int_to_value : int -> option_value
        val bool_of_string : string -> bool
        val value_to_bool : option_value -> bool
        val bool_to_value : bool -> option_value
        val value_to_float : option_value -> float
        val float_to_value : float -> option_value
        val value_to_string2 : option_value -> string * string
        val string2_to_value : string * string -> option_value
        val value_to_list : (option_value -> 'a) -> option_value -> 'a list
        val list_to_value : ('a -> option_value) -> 'a list -> option_value
      end
    type file_type = string
    type rule =
      Cam_files.rule = {
      rule_name : string;
      rule_f : string array -> string -> bool;
    }
    val ft_unknown : string
    val rc_ft : string
    val ft_ini : O.group
    val save_ft : unit -> unit
    val load_ft : unit -> unit
    val value_to_ft_handler : O.Raw.cp -> string * string list
    val ft_handler_to_value : string * string list -> O.Raw.cp
    val ft_handler_cp_wrapper : (string * string list) O.wrappers
    val value_to_rule : O.Raw.cp -> string * string
    val rule_to_value : string * string -> O.Raw.cp
    val rule_cp_wrapper : (string * string) O.wrappers
    val default_editor_command : string
    val default_ft_rules_and_handlers : (string * string * string list) list
    val default_ft_rules : (string * string) list
    val default_ft_handlers : (string * string list) list
    val ft_rules : (file_type * string) O.list_cp
    val ft_handlers : (file_type * string list) O.list_cp
    val default_open_file_command : O.string_cp
    module C :
      sig
        type parameter_kind = Configwin.parameter_kind
        type configuration_structure =
          Configwin.configuration_structure =
            Section of string * parameter_kind list
          | Section_list of string * configuration_structure list
        type return_button =
          Configwin.return_button =
            Return_apply
          | Return_ok
          | Return_cancel
        val string_to_key : string -> Gdk.Tags.modifier list * int
        val key_to_string : Gdk.Tags.modifier list * int -> string
        val key_cp_wrapper :
          (Gdk.Tags.modifier list * int) Config_file.wrappers
        class key_cp :
          ?group:Config_file.group ->
          string list ->
          ?short_name:string ->
          Gdk.Tags.modifier list * int ->
          string -> [Gdk.Tags.modifier list * int] Config_file.cp_custom_type
        val string :
          ?editable:bool ->
          ?expand:bool ->
          ?help:string ->
          ?f:(string -> unit) -> string -> string -> parameter_kind
        val custom_string :
          ?editable:bool ->
          ?expand:bool ->
          ?help:string ->
          ?f:('a -> unit) ->
          to_string:('a -> string) ->
          of_string:(string -> 'a) -> string -> 'a -> parameter_kind
        val bool :
          ?editable:bool ->
          ?help:string ->
          ?f:(bool -> unit) -> string -> bool -> parameter_kind
        val strings :
          ?editable:bool ->
          ?help:string ->
          ?f:(string list -> unit) ->
          ?eq:(string -> string -> bool) ->
          ?add:(unit -> string list) ->
          string -> string list -> parameter_kind
        val list :
          ?editable:bool ->
          ?help:string ->
          ?f:('a list -> unit) ->
          ?eq:('a -> 'a -> bool) ->
          ?edit:('a -> 'a) ->
          ?add:(unit -> 'a list) ->
          ?titles:string list ->
          ?color:('a -> string option) ->
          string -> ('a -> string list) -> 'a list -> parameter_kind
        val color :
          ?editable:bool ->
          ?expand:bool ->
          ?help:string ->
          ?f:(string -> unit) -> string -> string -> parameter_kind
        val font :
          ?editable:bool ->
          ?expand:bool ->
          ?help:string ->
          ?f:(string -> unit) -> string -> string -> parameter_kind
        val combo :
          ?editable:bool ->
          ?expand:bool ->
          ?help:string ->
          ?f:(string -> unit) ->
          ?new_allowed:bool ->
          ?blank_allowed:bool ->
          string -> string list -> string -> parameter_kind
        val text :
          ?editable:bool ->
          ?expand:bool ->
          ?help:string ->
          ?f:(string -> unit) -> string -> string -> parameter_kind
        val custom_text :
          ?editable:bool ->
          ?expand:bool ->
          ?help:string ->
          ?f:('a -> unit) ->
          to_string:('a -> string) ->
          of_string:(string -> 'a) -> string -> 'a -> parameter_kind
        val html :
          ?editable:bool ->
          ?expand:bool ->
          ?help:string ->
          ?f:(string -> unit) -> string -> string -> parameter_kind
        val filename :
          ?editable:bool ->
          ?expand:bool ->
          ?help:string ->
          ?f:(string -> unit) -> string -> string -> parameter_kind
        val filenames :
          ?editable:bool ->
          ?help:string ->
          ?f:(string list -> unit) ->
          ?eq:(string -> string -> bool) ->
          string -> string list -> parameter_kind
        val date :
          ?editable:bool ->
          ?expand:bool ->
          ?help:string ->
          ?f:(int * int * int -> unit) ->
          ?f_string:(int * int * int -> string) ->
          string -> int * int * int -> parameter_kind
        val hotkey :
          ?editable:bool ->
          ?expand:bool ->
          ?help:string ->
          ?f:(Gdk.Tags.modifier list * int -> unit) ->
          string -> Gdk.Tags.modifier list * int -> parameter_kind
        val custom :
          ?label:string ->
          GPack.box -> (unit -> unit) -> bool -> parameter_kind
        val edit :
          ?apply:(unit -> unit) ->
          string ->
          ?width:int ->
          ?height:int -> configuration_structure list -> return_button
        val get :
          string ->
          ?width:int ->
          ?height:int -> configuration_structure list -> return_button
        val simple_edit :
          ?apply:(unit -> unit) ->
          string ->
          ?width:int -> ?height:int -> parameter_kind list -> return_button
        val simple_get :
          string ->
          ?width:int -> ?height:int -> parameter_kind list -> return_button
        val box :
          parameter_kind list -> GData.tooltips -> GPack.box * (unit -> unit)
        val tabbed_box :
          configuration_structure list ->
          (string * (unit -> unit)) list -> GData.tooltips -> GPack.box
      end
    val file_type_choices : unit -> file_type list
    type conf_ft =
      Cam_files.conf_ft = {
      mutable conf_ft : string;
      mutable conf_rule : string;
    }
    val params_for_ft_rule : rule list -> conf_ft -> C.parameter_kind list
    val edit_ft_rules : rule list -> unit
    type conf_fth =
      Cam_files.conf_fth = {
      mutable conf_ft : string;
      mutable conf_hdls : string list;
    }
    val params_for_ft : conf_fth -> C.parameter_kind list
    val edit_ft_handlers : unit -> unit
    val rules : (string, rule) Hashtbl.t
    val register_rule : rule -> unit
    val file_types : unit -> file_type list
    val file_type_of_file : string -> file_type
    val _regexp_rule : string array -> string -> bool
    val file_type_handlers : (file_type, string list) Hashtbl.t
    val associate_handler : file_type -> string -> unit
    val command_on_files : string -> string list -> unit
    val edition_commands_menu_entries :
      ?line:int -> string -> GToolbox.menu_entry list
    val popup_file_commands_menu : string -> unit
    val _configure_ft_rules : 'a -> unit
    val _configure_ft_handlers : 'a -> unit
  end
module Hooks :
  sig
    val set_display_message : (?to_utf8:bool -> string -> unit) -> unit
    val set_error_message : (?to_utf8:bool -> string -> unit) -> unit
    val set_warning_message : (?to_utf8:bool -> string -> unit) -> unit
    val display_message : ?to_utf8:bool -> string -> unit
    val error_message : ?to_utf8:bool -> string -> unit
    val warning_message : ?to_utf8:bool -> string -> unit
    val get_display_message : unit -> ?to_utf8:bool -> string -> unit
    val get_error_message : unit -> ?to_utf8:bool -> string -> unit
    val get_warning_message : unit -> ?to_utf8:bool -> string -> unit
  end
module Doc_gui :
  sig
    val modules_window :
      (GWindow.window * string GList.clist * GEdit.combo *
       Odoc_info.Name.t list)
      option ref
    val create_or_update_list_window :
      Odoc_info.Module.t_module list ref -> unit
    val search_elements_by_exact_names :
      Odoc_info.Module.t_module list ref -> string -> unit
    val search_exact : Odoc_info.Module.t_module list ref -> unit
    val search_regexp : Odoc_info.Module.t_module list ref -> unit
    val update_module_box_if_displayed :
      Odoc_info.Module.t_module list ref -> unit
    val open_element :
      Odoc_info.Module.t_module list ref -> Cam_doc.element -> unit
    val show_odoc_info_and_code :
      title:string -> info:Odoc_info.info -> code:string -> unit
  end
module Modules_view :
  sig
    module M :
      sig
        val usage_browser : string
        val usage : string
        val home : string
        val login : string
        val rc_dir : string
        val plugins_dir : string
        val software_author : string
        val software_author_mail : string
        val software_copyright : string
        val about : string
        val bad_format : string -> string
        val loading_file : string -> string
        val ok : string
        val yes : string
        val no : string
        val confirm : string
        val cancel : string
        val copy : string
        val cut : string
        val paste : string
        val edit : string
        val up : string
        val down : string
        val add_menu : string
        val add_command : string
        val separator : string
        val menu : string
        val add : string
        val remove : string
        val configuration : string
        val doc : string
        val quit : string
        val really_quit : string
        val log_window : string
        val file : string
        val search_exact : string
        val search_regexp : string
        val running_com : string -> string
        val error : string
        val error_exec : string -> string
        val back : string
        val search : string
        val error_not_found : string -> string -> string
        val error_not_found_module : string -> string
        val error_not_found_module_type : string -> string
        val error_not_found_mmt : string -> string
        val error_not_found_class : string -> string
        val error_not_found_class_type : string -> string
        val error_not_found_cct : string -> string
        val error_not_found_exception : string -> string
        val error_not_found_type : string -> string
        val error_not_found_value : string -> string
        val error_not_found_attribute : string -> string
        val error_not_found_method : string -> string
        val error_load_file : string -> string -> string
        val authors : string
        val version : string
        val since : string
        val raises : string
        val see_also : string
        val returns : string
        val deprecated : string
        val parameters : string
        val inherits : string
        val doc_box : string
        val close : string
        val name : string
        val kind : string
        val nothing_found : string -> string
        val error_unknown_action : string -> string
        val label : string
        val command : string
        val doc_flag : string
        val icon_file : string
        val icon : string
        val file_type : string
        val rule : string
        val file_types_rules : string
        val commands : string
        val default_command : string
        val file_types_handlers : string
        val open_with : string
        val use_ : string
        val doc_file : string
        val doc_files : string
        val doc_sources : string
        val common_keyboard_shortcuts : string
        val docbrowser_keyboard_shortcuts : string
        val binding : string
        val edit_binding : string
        val update_dir : string
        val commit_dir : string
        val enter_comment : string
        val enter_comment_commit : string
        val add_dir : string
        val should_create_dir : string -> string
        val tag_dir : string
        val enter_tag_for_dir : string -> string
        val error_add_files : string list -> string
        val add_files : string
        val add_binary_files : string
        val commit_files : string
        val remove_files : string
        val tag_files : string
        val enter_tag_for_files : string list -> string
        val tags_of_file : string
        val resolve_conflicts : string
        val log : string
        val tag : string
        val revision : string
        val error_remove_files : string list -> string
        val confirm_remove_files : string list -> string
        val tags_of : string -> string
        val last_diff : string
        val diff_with : string
        val diff_between : string
        val plugins : string
        val no_plugin_loaded : string
        val plugin_to_reload : string
        val shared_plugins_to_load : string
        val personal_plugins_to_load : string
        val already_loaded : string
        val h_reload_plugin : string
        val configure_doc_sources : string
      end
    module O :
      sig
        module Raw :
          sig
            type cp =
              Config_file.Raw.cp =
                String of string
              | Int of int
              | Float of float
              | List of cp list
              | Tuple of cp list
              | Section of (string * cp) list
            val of_string : string -> cp
            val to_channel : out_channel -> cp -> unit
          end
        type 'a wrappers =
          'a Config_file.wrappers = {
          to_raw : 'a -> Raw.cp;
          of_raw : Raw.cp -> 'a;
        }
        exception Wrong_type of (out_channel -> unit)
        class type ['a] cp =
          object
            method add_hook : ('a -> 'a -> unit) -> unit
            method get : 'a
            method get_default : 'a
            method get_default_formatted : Format.formatter -> unit
            method get_formatted : Format.formatter -> unit
            method get_help : string
            method get_help_formatted : Format.formatter -> unit
            method get_name : string list
            method get_short_name : string option
            method get_spec : Arg.spec
            method reset : unit
            method set : 'a -> unit
            method set_raw : Raw.cp -> unit
            method set_short_name : string -> unit
          end
        type groupable_cp =
            < get_default_formatted : Format.formatter -> unit;
              get_formatted : Format.formatter -> unit; get_help : string;
              get_help_formatted : Format.formatter -> unit;
              get_name : string list; get_short_name : string option;
              get_spec : Arg.spec; reset : unit; set_raw : Raw.cp -> unit >
        exception Double_name
        exception Missing_cp of groupable_cp
        class group :
          object
            method add : 'a cp -> unit
            method command_line_args :
              section_separator:string -> (string * Arg.spec * string) list
            method read :
              ?obsoletes:string ->
              ?no_default:bool ->
              ?on_type_error:(groupable_cp ->
                              Raw.cp ->
                              (out_channel -> unit) ->
                              string -> in_channel -> unit) ->
              string -> unit
            method write : ?with_help:bool -> string -> unit
          end
        class int_cp :
          ?group:group ->
          string list -> ?short_name:string -> int -> string -> [int] cp
        class float_cp :
          ?group:group ->
          string list -> ?short_name:string -> float -> string -> [float] cp
        class bool_cp :
          ?group:group ->
          string list -> ?short_name:string -> bool -> string -> [bool] cp
        class string_cp :
          ?group:group ->
          string list ->
          ?short_name:string -> string -> string -> [string] cp
        class ['a] list_cp :
          'a wrappers ->
          ?group:group ->
          string list ->
          ?short_name:string -> 'a list -> string -> ['a list] cp
        class ['a] option_cp :
          'a wrappers ->
          ?group:group ->
          string list ->
          ?short_name:string -> 'a option -> string -> ['a option] cp
        class ['a] enumeration_cp :
          (string * 'a) list ->
          ?group:group ->
          string list -> ?short_name:string -> 'a -> string -> ['a] cp
        class ['a, 'b] tuple2_cp :
          'a wrappers ->
          'b wrappers ->
          ?group:group ->
          string list ->
          ?short_name:string -> 'a * 'b -> string -> ['a * 'b] cp
        class ['a, 'b, 'c] tuple3_cp :
          'a wrappers ->
          'b wrappers ->
          'c wrappers ->
          ?group:group ->
          string list ->
          ?short_name:string -> 'a * 'b * 'c -> string -> ['a * 'b * 'c] cp
        class ['a, 'b, 'c, 'd] tuple4_cp :
          'a wrappers ->
          'b wrappers ->
          'c wrappers ->
          'd wrappers ->
          ?group:group ->
          string list ->
          ?short_name:string ->
          'a * 'b * 'c * 'd -> string -> ['a * 'b * 'c * 'd] cp
        class string2_cp :
          ?group:group ->
          string list ->
          ?short_name:string ->
          string * string -> string -> [string, string] tuple2_cp
        class font_cp :
          ?group:group ->
          string list -> ?short_name:string -> string -> string -> string_cp
        class filename_cp :
          ?group:group ->
          string list -> ?short_name:string -> string -> string -> string_cp
        val int_wrappers : int wrappers
        val float_wrappers : float wrappers
        val bool_wrappers : bool wrappers
        val string_wrappers : string wrappers
        val list_wrappers : 'a wrappers -> 'a list wrappers
        val option_wrappers : 'a wrappers -> 'a option wrappers
        val enumeration_wrappers : (string * 'a) list -> 'a wrappers
        val tuple2_wrappers :
          'a wrappers -> 'b wrappers -> ('a * 'b) wrappers
        val tuple3_wrappers :
          'a wrappers ->
          'b wrappers -> 'c wrappers -> ('a * 'b * 'c) wrappers
        val tuple4_wrappers :
          'a wrappers ->
          'b wrappers ->
          'c wrappers -> 'd wrappers -> ('a * 'b * 'c * 'd) wrappers
        class ['a] cp_custom_type :
          'a wrappers ->
          ?group:group ->
          string list -> ?short_name:string -> 'a -> string -> ['a] cp
        type 'a option_class = 'a Config_file.option_class
        type 'a option_record = 'a Config_file.option_record
        type options_file = Config_file.options_file
        val create_options_file : string -> options_file
        val set_options_file : options_file -> string -> unit
        val load : options_file -> unit
        val append : options_file -> string -> unit
        val save : options_file -> unit
        val save_with_help : options_file -> unit
        val option_hook : 'a option_record -> (unit -> unit) -> unit
        val string_option : string option_class
        val color_option : string option_class
        val font_option : string option_class
        val int_option : int option_class
        val bool_option : bool option_class
        val float_option : float option_class
        val string2_option : (string * string) option_class
        val option_option : 'a option_class -> 'a option option_class
        val list_option : 'a option_class -> 'a list option_class
        val sum_option : (string * 'a) list -> 'a option_class
        val tuple2_option :
          'a option_class * 'b option_class -> ('a * 'b) option_class
        val tuple3_option :
          'a option_class * 'b option_class * 'c option_class ->
          ('a * 'b * 'c) option_class
        val tuple4_option :
          'a option_class * 'b option_class * 'c option_class *
          'd option_class -> ('a * 'b * 'c * 'd) option_class
        val ( !! ) : 'a option_record -> 'a
        val ( =:= ) : 'a option_record -> 'a -> unit
        val shortname : 'a option_record -> string
        val get_help : 'a option_record -> string
        type option_value =
          Config_file.option_value =
            Module of option_module
          | StringValue of string
          | IntValue of int
          | FloatValue of float
          | List of option_value list
          | SmallList of option_value list
        and option_module = (string * option_value) list
        val define_option_class :
          string ->
          (option_value -> 'a) -> ('a -> option_value) -> 'a option_class
        val to_value : 'a option_class -> 'a -> option_value
        val from_value : 'a option_class -> option_value -> 'a
        val value_to_string : option_value -> string
        val string_to_value : string -> option_value
        val value_to_int : option_value -> int
        val int_to_value : int -> option_value
        val bool_of_string : string -> bool
        val value_to_bool : option_value -> bool
        val bool_to_value : bool -> option_value
        val value_to_float : option_value -> float
        val float_to_value : float -> option_value
        val value_to_string2 : option_value -> string * string
        val string2_to_value : string * string -> option_value
        val value_to_list : (option_value -> 'a) -> option_value -> 'a list
        val list_to_value : ('a -> option_value) -> 'a list -> option_value
      end
    module N :
      sig
        type t = string
        val simple : t -> t
        val concat : t -> t -> t
        val depth : t -> int
        val get_relative : t -> t -> t
        val father : t -> t
      end
    val to_utf8 : string -> string
    val color_type : O.string_cp
    val color_value : O.string_cp
    val color_exception : O.string_cp
    val color_module : O.string_cp
    val color_module_type : O.string_cp
    val color_class : O.string_cp
    val color_class_type : O.string_cp
    val color_comment : O.string_cp
    val color_included_module : O.string_cp
    val open_source_command : O.string_cp
    val f_open_file : ?char:int -> string -> unit
    val dump_files : (string, Odoc_info.Module.t_module list) Hashtbl.t
    val get_modules_from_dir : string -> Odoc_info.Module.t_module list
    type row_content =
      Cam_modules_view.row_content =
        ME of Odoc_info.Module.module_element
      | CE of Odoc_info.Class.class_element
    val location_of_module_element :
      Odoc_info.Module.module_element -> Odoc_info.location option
    val location_of_class_element :
      Odoc_info.Class.class_element -> Odoc_info.location option
    val location_of_ele : row_content -> Odoc_info.location option
    class box :
      string ->
      object
        val mutable selection : row_content option
        method box : GPack.box
        method col_data : row_content GTree.column
        method col_display : string GTree.column
        method color_of_element : row_content -> string
        method display_string_of_ele : row_content -> string
        method init_col_display :
          col_display:GTree.view_column ->
          complete:row_content GTree.column ->
          renderer:GTree.cell_renderer_text -> GTree.tree_store -> unit
        method insert_ele : ?parent:Gtk.tree_iter -> row_content -> unit
        method menu_ctx : row_content option -> GToolbox.menu_entry list
        method on_collapse : row_content -> unit
        method on_expand : row_content -> unit
        method on_select : row_content -> unit
        method on_unselect : row_content -> unit
        method select : row_content -> unit
        method selection : row_content option
        method string_type_of_ele : row_content -> string option
        method subs_of_ele : row_content -> row_content list
        method unselect : row_content -> unit
        method update : unit
        method view : GTree.view
      end
    class view :
      Cam_view.view_name ->
      Cam_view.ressource_name ->
      box ->
      bool ->
      object
        method changed : bool
        method close : bool
        method name : Cam_view.view_name
        method refresh : unit
        method ressource : Cam_view.ressource_name
        method ressource_kind : Cam_view.ressource_kind
      end
    class factory : Cam_view.view_factory
  end
