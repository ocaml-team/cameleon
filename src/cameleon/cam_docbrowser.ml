(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(* $Id: cam_docbrowser.ml 758 2011-01-13 07:53:27Z zoggy $ *)

(** Main module for the standalone documentation browser. *)

ignore (GMain.Main.init ());;

let doc_files = ref ([] : string list)

let options_list = [
] ;;

Arg.parse options_list
    (fun s -> doc_files := !doc_files @ [s])
    Cam_messages.usage_browser;;


Cam_doc.default_doc_modules := Cam_doc.load_doc_files !doc_files;;


Cam_doc_gui.create_or_update_list_window Cam_doc.default_doc_modules;;

let _ =
  match !Cam_doc_gui.modules_window with
    None -> ()
  | Some (w,_,_,_) ->
      ignore (w#connect#destroy GMain.Main.quit);
      Cam_rc.load_core ();
      Cam_rc.load_gui ();
      Cam_doc.init_keymaps ();
      GMain.Main.main ()
