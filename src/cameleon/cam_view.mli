(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(* $Id: cam_view.mli 758 2011-01-13 07:53:27Z zoggy $ *)

(** Views on ressources *)

(** {2 Resources and views} *)

type ressource_kind = [ `None | `Dir | `File ]
type ressource_name = string
type view_name = string
class type view =
  object
    method changed : bool

    (** Return [true] if the associated window must be closed, too. *)
    method close : bool

    method name : view_name
    method refresh : unit
    method ressource : ressource_name
    method ressource_kind : ressource_kind
  end
class type view_factory =
  object
    method create : ressource_name -> string array -> view * GWindow.window
    method create_no_window :
       GWindow.window -> ressource_name -> string array -> view * GObj.widget
    method known_ressource_kinds : ressource_kind list
    method name : view_name
  end

(** {2 Using factories} *)

val register_factory : view_factory -> unit
val open_ressource : ressource_name -> view_name -> string array -> view
val open_ressource_no_window :
   ressource_name -> view_name -> string array -> GWindow.window -> (GObj.widget -> unit) -> view
val refresh_ressource_views : ressource_name -> unit
val close_ressource_views : ressource_name -> unit
val available_views : ?kind: ressource_kind -> unit -> view_name list

(** {2 Windows for views} *)

class view_window :
    ?allow_shrink: bool ->
    ?width:int ->
      ?height:int ->
	title:string ->
	  view ->
	    object
	      method vbox : GPack.box
	      method window : GWindow.window
	    end

val create_view_window :
  ?width:int ->
  ?height:int ->
  title:string -> view -> view_window
