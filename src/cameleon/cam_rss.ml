(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(* $Id: cam_rss.ml,v 1.2 2005/06/10 08:09:39 zoggy Exp $ *)

(** Cameleon2 plugin. *)

let gui = ref None

let open_files = ref []
let close_file f =
  Cam_plug.Hooks.display_message (Printf.sprintf "file %s closed" f);
  open_files := List.filter ((<>) f) !open_files

let rec open_file f =
  match !gui with
    Some g ->
      if List.mem f !open_files then
        ()
      else
        begin
          try
            g#add_file_box (Some f);
            open_files := f :: !open_files
          with
          | Failure s ->
              GToolbox.message_box "Error" s
          | e ->
              GToolbox.message_box "Error"
                (Printf.sprintf "Could not open file %s:\n%s"
                 f (Printexc.to_string e))
        end;
      g#gui#show ()
  | None ->
      let g = new Rss_gui.gui
        ~quit_on_destroy: false
          ~on_close_file: close_file
          ~default_file: false
          []
      in
      gui := Some g;
      ignore
        (g#gui#connect#destroy
         (fun _ ->
            gui := None;
            open_files := []
         )
        );
      open_file f

let _open_file args =
  Array.iter open_file args

let _ =
  let module C = Cam_plug.Commands in
  C.register
    { C.com_name = "open_rss_file" ;
      C.com_args = [| |] ;
      C.com_more_args = Some "files to open" ;
      C.com_f = _open_file ;
    }
