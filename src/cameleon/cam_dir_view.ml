(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(* $Id: cam_dir_view.ml 758 2011-01-13 07:53:27Z zoggy $ *)

class dir_view root =
  object (self)
    inherit Gdir.gdir ()

    val mutable expanded = [root]

    method expand_dir dir = List.mem dir expanded
    method on_expand_dir dir =
      if not (List.mem dir expanded) then
	expanded <- dir :: expanded
    method on_collapse_dir dir =
      if List.mem dir expanded then
	expanded <- List.filter ((<>) dir) expanded

    method roots = [root]

    method menu_ctx = function
	None -> []
      |	Some dir ->
	  let view_names = Cam_view.available_views ~kind: `Dir () in
	  List.map
	    (fun s -> `I (Printf.sprintf "%s view" s, fun () -> ignore (Cam_view.open_ressource dir s [| |])))
	    view_names
  end

class view
    (name : Cam_view.view_name)
    (root : Cam_view.ressource_name)
    (gdir : Gdir.gdir)
    (close_window_on_close : bool) =
  object (self)
    method changed = false
    method close = close_window_on_close
    method name = name
    method refresh = gdir#update
    method ressource = root
    method ressource_kind : Cam_view.ressource_kind = `Dir
  end

class factory : Cam_view.view_factory =
  object (self)
    method create res_name args =
      let gdir = new dir_view res_name in
      let v = new view (self#name) res_name gdir true in
      let w = Cam_view.create_view_window
	  ~title: (Printf.sprintf "%s [%s]" res_name self#name)
	  v
      in
      let _ = w#vbox#pack ~expand: true gdir#box#coerce in
      (v, w#window)

    method create_no_window _ res_name args =
      let gdir = new dir_view res_name in
      let v = new view (self#name) res_name gdir false in
      (v, gdir#box#coerce)

    method known_ressource_kinds = [`Dir]
    method name = "directory"
  end

let _ = Cam_view.register_factory (new factory)
