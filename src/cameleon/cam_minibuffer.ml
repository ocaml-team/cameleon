(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

class minibuffer_window () =
  let key_bindings_trees = ref [] in
  let title = Printf.sprintf "%s: minibuffer" Cam_installation.software in
  let w = GWindow.window ~title ~width:600 ~height:150 () in
  let mb = new Ed_minibuffer.minibuffer () in
  object(self)
    method minibuffer = mb
    method window = w

    initializer
      ignore (w#event#connect#delete (fun _ -> w#misc#hide (); true));
      w#add mb#box;
      mb#set_on_active_change
        (fun b ->
           if b then
             begin
               Okey.reset_state w;
               let kb = List.map
                 (fun (k,com) -> (k, fun () -> Cam_commands.eval_command com))
                 mb#key_bindings
               in
               key_bindings_trees := Okey.trees_of_list kb; 
               w#show ()
             end
           else
             w#misc#hide ()
        );
      Okey.set_handler_trees
        ~stop: Ed_gui_rc.abort_binding#get
        (fun () -> !key_bindings_trees)
        w
  end
  
let the_mb = ref None

let get_mb () =
  match !the_mb with
  | Some w -> w#window#show () ; w#minibuffer
  | None ->
      let w = new minibuffer_window () in
      the_mb := Some w;
      w#minibuffer
       

let prompt_command_history = Ed_minibuffer.history ()
let prompt_command args =
  let mb = get_mb () in
  let on_return com =
    match Ed_misc.no_blanks com with
      "" -> ()
    | _ -> Cam_commands.eval_command com
  in
  Cam_hooks.warning_message "couocu";
  Ed_misc.select_string
    ~history: prompt_command_history
    mb ~title: "Command"
    ~choices: (Cam_commands.available_command_names ())
    ""
    on_return

let _ =
  Cam_commands.register
    { Cam_commands.com_name = Cam_constant.com_prompt_command ;
      Cam_commands.com_args = [| |] ;
      Cam_commands.com_more_args = None ;
      Cam_commands.com_f = prompt_command ;
    } ;
