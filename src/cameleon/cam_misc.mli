(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(* $Id: cam_misc.mli 758 2011-01-13 07:53:27Z zoggy $ *)

(** Utils *)

val map_opt : ('a -> 'b) -> 'a option -> 'b option
val chop_n_char : int -> string -> string

(** Same as [int_of_string] but support strings beginning with '+'. *)
val my_int_of_string : string -> int

(*i==v=[List.list_remove_doubles]=1.0====*)
(** [list_remove_doubles ?pred l] remove doubles in the given list [l], according
   to the optional equality function [pred]. Default equality function is [(=)].
@author Maxence Guesdon
@version 1.0
@cgname List.list_remove_doubles*)
val list_remove_doubles : ?pred:('k -> 'k -> bool) -> 'k list -> 'k list
(*/i==v=[List.list_remove_doubles]=1.0====*)

val add_shortcut :
  < connect : < destroy : callback:(unit -> unit) -> GtkSignal.id; .. >;
    event : GObj.event_ops; get_oid : int; .. > ->
  (string * ((unit -> bool) option * (unit -> unit))) list ->
  (Okey.modifier list * Gdk.keysym) * string -> unit

(*i==v=[File.file_of_string]=1.1====*)
(** [file_of_string ~file str] creates a file named
   [filename] whose content is [str].
@author Fabrice Lefessant
@version 1.1
@raise Sys_error if the file could not be opened.
@cgname File.file_of_string*)
val file_of_string : file:string -> string -> unit
(*/i==v=[File.file_of_string]=1.1====*)

val select_in_list :
    ?ok:string -> ?cancel:string ->
      ?allow_empty: bool ->
	?value_in_list: bool ->
	  title:string ->
	  choices: string list ->
	    string -> string option

(** [remove_char s c] returns the given string [s] without character [c]. *)
val remove_char : string -> char -> string

(** Handle all pending GTK events. *)
val treat_gtk_events : unit -> unit

(** Return the offset of position due to the window manager's decoration. *)
val get_wm_window_position_offset : unit -> int * int
(*i==v=[File.subdirs]=0.1====*)
(** [subdirs path] returns the list of subdirectories of the given directory name.
   Returned names are relative to the given path.
@author Maxence Guesdon
@version 0.1
@raise Unix_error if an error occurs.
@cgname File.subdirs*)
val subdirs : string -> string list
(*/i==v=[File.subdirs]=0.1====*)

(** [line_of_char file char] gets the line number (0-based)
   in a file from a character number. *)
val line_of_char : string -> int -> int

(** [char_of_line file line] returns the offset of the first character
     of the given [line] of the give [file]. Line number are zero-based,
     as the returned character number. *)
val char_of_line : string -> int -> int

(*i==v=[String.replace_in_string]=1.0====*)
(** [replace_in_string ~pat ~subs ~s] replaces all occurences of
   pattern [pat] by [subs] in string [s].
@author Maxence Guesdon
@version 1.0
@cgname String.replace_in_string*)
val replace_in_string : pat:string -> subs:string -> s:string -> string
(*/i==v=[String.replace_in_string]=1.0====*)

(** [escape_menu_label string] returns a the string where all '_' have
   been escaped to be displayed correctly in Lablgtk menus.*)
val escape_menu_label : string -> string

(** [utf8_index_of_char string n] returns the position of the first byte
     the [n]th character in the given UTF-8 [string].
     @raise Not_found if there is no such character.*)
val utf8_index_of_char : string -> int -> int

(** [utf8_char_of_index string i] returns the number of characters
     found from the beginning of the UTF-8 string to position [i] (included).
     @raise Invalid_argument if the given position is out of the string bounds.*)
val utf8_char_of_index : string -> int -> int

(** [utf8_string_length string] returns the number of utf8 characters in the
     given string. *)
val utf8_string_length : string -> int

(** [utf8_char_of_code code] returns the string representing the UTF-8 character
  with the given [code].
  @raise Failure if the code is out of range. Only 4 bytes UTF-8 is supported by now.
  *)
val utf8_char_of_code : int -> string
