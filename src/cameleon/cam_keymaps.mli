(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(* $Id: cam_keymaps.mli 758 2011-01-13 07:53:27Z zoggy $ *)

(** Keyboard shortcuts management *)

val keymap_common :
    ((Okey.modifier list * Gdk.keysym) * string) Config_file.list_cp

val init_common_keymaps : unit -> unit
val set_window_common_keymaps : GWindow.window -> unit

val configure_keymaps :
  string ->
  < get : ((Gdk.Tags.modifier list * Gdk.keysym) * string) list;
    set : ((Gdk.Tags.modifier list * Gdk.keysym) * string) list -> 'a; .. > ->
  bool -> string list -> (unit -> unit) -> unit -> unit

val configure_common_keymaps : unit -> unit
