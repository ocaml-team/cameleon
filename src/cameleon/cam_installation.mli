(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Names of installation directories, completed by the configure script. *)

val bin_dir : string

val lib_dir : string

val man_dir : string

val templates_dir : string

val plugins_dir : string

val pixmaps_dir : string

val glade_dir : string

val glade_file : string

val lablgtk2_dir : string

val pcre_dir : string

val languages_specs_dir : string

(** Software name *)
val software : string

(** The version number of the software *)
val software_version : string

(** {2 Additional tools} *)

val dbforge_gui : string
val report_gui : string

(** {2 Some useful values for user configuration} *)

(** The user homedir *)
val home : string

(** The cameleon rc directory of the user *)
val rc_dir : string
