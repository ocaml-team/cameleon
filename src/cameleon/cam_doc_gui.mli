(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(* $Id: cam_doc_gui.mli 758 2011-01-13 07:53:27Z zoggy $ *)

(** Gui for documentation browser. *)

open Odoc_info

val modules_window :
    (GWindow.window * string GList.clist * GEdit.combo * Odoc_info.Name.t list)
    option ref

val create_or_update_list_window :
    Odoc_info.Module.t_module list ref -> unit
val search_elements_by_exact_names :
    Odoc_info.Module.t_module list ref -> string -> unit
val search_exact : Odoc_info.Module.t_module list ref ->unit
val search_regexp : Odoc_info.Module.t_module list ref ->unit
val update_module_box_if_displayed : Odoc_info.Module.t_module list ref ->unit
val open_element : Odoc_info.Module.t_module list ref -> Cam_doc.element -> unit
val show_odoc_info_and_code :
    title: string -> info: Odoc_info.info -> code: string -> unit
