(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Some commands to interact with chamo. *)


let chamo_open_file args =
  let args =
    if Array.length args < 1 then
      failwith "chamo_open_file: not enough arguments";
    if Array.length args < 2 then
      [| args.(0) ; "1,0" |]
    else
      [| args.(0) ; Printf.sprintf "%s,0" args.(1) |]
  in
  match !Ed_gui.gui_windows, !Ed_gui.active_window with
    [], _ ->
      Cam_commands.launch_command
        "in_new_window" (Array.append [|"open_file"|] args)
  | (w :: q), None ->
      w#window#misc#grab_focus ();
      Cam_commands.launch_command "open_file" args
  | _, Some _ ->
      Cam_commands.launch_command "open_file" args

let _ = Cam_commands.register
  (Cam_commands.create_com "chamo_open_file"
    ~more: "additional arguments: line" [|"file"|] chamo_open_file)
