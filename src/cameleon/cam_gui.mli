(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(* $Id: cam_gui.mli 758 2011-01-13 07:53:27Z zoggy $ *)

class main : unit ->
  object
    val hbox1 : GPack.box
    val hbox_buttons : GPack.box
    val menubar : GMenu.menu_shell
    val statusbar : GMisc.statusbar
    val toplevel : GWindow.window
    val xml : Glade.glade_xml Gtk.obj
    method bind : name:string -> callback:(unit -> unit) -> unit
    method check_widgets : unit -> unit
    method hbox1 : GPack.box
    method hbox_buttons : GPack.box
    method main : GWindow.window
    method menubar : GMenu.menu_shell
    method reparent : GObj.widget -> unit
    method statusbar : GMisc.statusbar
    method toplevel : GWindow.window
    method window : GWindow.window
    method xml : Glade.glade_xml Gtk.obj
  end
