%(***********************************************************************)
%(*                               Report                                *)
%(*                                                                     *)
%(*            Maxence Guesdon, projet Cristal, INRIA Rocquencourt      *)
%(*                                                                     *)
%(*  Copyright 2001 Institut National de Recherche en Informatique et   *)
%(*  en Automatique.  All rights reserved.  This file is distributed    *)
%(*  under the terms of the GNU General Public License version 2.       *)
%(*                                                                     *)
%(***********************************************************************)
\documentclass[11pt]{article} 
\usepackage[latin1]{inputenc} 
\usepackage[T1]{fontenc} 
\usepackage{fullpage} 
\usepackage{hevea}
\parindent 0pt
\title{Report\\ version 0.1 \\ Documentation}
\date{\today}
\author{Maxence Guesdon\\ Copyright � 2001 Institut National de Recherche en Informatique et en Automatique }
\begin{document}
\maketitle

%%%%%%%%%%%%%
\subsection*{License}
The Report tool is copyright � 2001 Institut National de Recherche en Informatique et en Automatique (INRIA).
INRIA holds all ownership rights to Report. \\
The Report tool is open source and can be freely redistributed. See the file LICENSE in the distribution for
 licensing information. The present documentation is distributed under the same conditions.

%\tableofcontents

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
\label{introduction}

The Report tool aims to make generation of XML documents from OCaml applications easier.
The main idea is to separate the structure of the document from the information
computed by the application and placed in the document.
For example, in the following document~:
\begin{verbatim}
<h1>Crepes</h1>
<list>
  <item>flour</item>
  <item>eggs</item>
  <item>sugar</item>
</list>
\end{verbatim}
the document structure is
\begin{verbatim}
<h1> </h1>
<list>
  <item> </item>
  <item> </item>
  <item> </item>
</list>
\end{verbatim}
while computed information is ``Crepes'', ``flour'', ``eggs'' and ``sugar''.

To build our XML document, we must therefore describe its structure as well as
the way to fill it with information. Then, at runtime, the application will use
this description to generate the final XML document.

In practice, Report allows to graphically describe the document 
(structure + information), and then to generate OCaml code which uses the {\tt Report}
library. In particular, this library contains a function which computes a document
description to produce the final XML document. 
An important point is that the way to compute the information needed in the document
is given in the form of OCaml code.

The {\tt Report} library as well as the generated code are detailed in \ref{libreport}. 
The description of document and the use of the {\tt report} tool are explained in
\ref{tool}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{The {\tt Report} library}
\label{libreport}

%%%%%%%%%%%%%%%%%%%%
\subsection{Types}
The {\tt Report} module defines the types used to describe a document.
\begin{verbatim}
(** A report element. *)
type 'a report_ele =
  | Leaf of (unit -> string)
  | Tag of 'a tag
  | List of 'a liste
  | Cond of  'a cond
  | Sub of (unit -> 'a report)

(** A tag. *)
and 'a tag = {
    tag : string ;
    atts : (string * (unit -> string)) list ;
    tag_subs : 'a report_ele list
  } 
    
(** A list of substructures. *)
and 'a liste =
    { list_subs : 'a report_ele list ;
      f : (unit -> 'a list) ;
      mutable current : 'a ;
    }

(** Conditional *)
and 'a cond =
    { cond : unit -> bool ;
      subs_then : 'a report_ele list ;
      subs_else : 'a report_ele list ;
    } 

(** A report description is a list of report elements. *)
and 'a report = {
    rep_eles : 'a report_ele list ;
  } 
\end{verbatim}

The type {\tt report\_ele} is the type of the tree nodes~:
\begin{itemize}
\item {\tt Leaf} is used for the leafs of the tree and needs the code of a function
	taking {\tt ()} and returning a string. While computing the document description,
	{\tt ()} will be applied to this function in order to obtain the string to insert
	into the final XML document.
	This way of hiding OCaml code ``under'' {\tt fun () ->} allows not to
	compute the result until the computation of the leaf, thus allowing to use
	values computed ``above'' the leaf.

\item {\tt Tag} is used to define an XML node of the final XML document. It needs the
	tag name, eventually a list of attributes and values, as well as subtrees of the
	node. Values of attributes are OCaml code too, as for the leafs.

\item {\tt List} allows to insert, while computing the final document, a list of subtrees
	for each element in a list. The field {\tt f} contains the OCaml function returning
	the list of elements to iterate on.
	The field {\tt current} is used to store the current element while walking through
	the subtrees. This way, the value of the field {\tt current} can be used in the
	functions which appear in the subtrees of the node.

\item {\tt Cond} allows to use one list of subtrees or another, depending on the boolean 
	value returned by the given function.

\item {\tt Sub} is used to insert another document in the current one.
	The given function returns a document description which is computed too.
\end{itemize}

The document example of the introduction could be described by~:
\begin{verbatim}
let rec report  = 
  ({
   rep_eles = [
     Tag { tag = "h1" ; atts = [] ; 
           tag_subs = [
             Leaf (fun () -> "Crepes");
           ] } ;
     Tag { tag = "list" ; atts = [] ; 
           tag_subs = [
             ( let rec ing = 
                 { f = (fun () -> ing_of_recipe "Crepes") ; 
                   current = (Report.coerce 0) ; 
                   list_subs = [
                     Tag { tag = "item" ; atts = [] ; 
                           tag_subs = [ Leaf (fun () -> ing.current.ing_name) ] }
                   ] }
                in
                List (Report.coerce ing))
           ] 
          }
    ]
   } : int report)
\end{verbatim}

The call to {\tt Report.coerce} is necessary to force the type but type constraints
are already satisfied (notably the use of the {\tt current} field) at this point.

As we can see, this structure can quickly become a pain to define and read.
To solve this problem, the {\tt report} tool allows to graphically define
the document description and generate the OCaml code of this structure.

Moreover, the {\tt report} value could have had parameters ; this is a
 way to parametrize the final XML document.

%%%%%%%%%%%%%%%%%%%%
\subsection{Functions}
The {\tt Report} module contains the following functions~:
\begin{verbatim}
(** Coerce report elements. *)
val coerce : 'a -> 'b

(** Compute a report and print it to the given formatter. *)
val compute : ?html: bool -> Format.formatter -> 'a report -> unit

(** Compute a report and print it in a file. *)
val compute_file : ?html: bool -> string -> 'a report -> unit
\end{verbatim}

The {\tt coerce} function is used to insert a {\tt 'a report\_ele} into
the node of a {\tt 'b report\_ele}, when {\tt 'a} cannot be used as {\tt 'b}.
This function must only be used for this purpose, as in the example above.

The {\tt compute} function takes a file name and a document 
description and computes this description to generate the final XML document in
the given {\tt formatter}. The {\tt html} optional parameter allows not to 
close some tags (like {\tt br}) to generate HTML compliant documents.

The {\tt compute\_file} function acts like {\tt compute} but writes in 
the given file instead of a formatter.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{The {\tt report} and {\tt report.gui} tools}
\label{tool}

The {\tt report.gui} tool allows to describe a document (structure + code to fill it
with information), as well as the parameters of this document (these parameters
become parameters of the {\tt report} value in the code example in \ref{libreport}).
The document is describe through a graphical user interface with a tree oriented view.

The {\tt report} tool generates the OCaml code of the document description,
using the types defined in the {\tt Report} library (cf. \ref{libreport}).

Here is the {\tt report} command usage~:
{\tt \bf report [options]} {\bf \em description\_file}\\
The following options are supported~:
\begin{description}
\item[\bf {\tt -gen}]
Generate the OCaml code for the document described in the given file.
The code is generated in a file whose name is function of {\bf \em description\_file}.
If {\bf \em description\_file} has an extension, the generated file as the same
name with the extension replaced by {\tt .ml}. If {\bf \em description\_file}
has no extension, then the generated file has the same name, with the
{\tt .ml} extension appended.

\item[\bf {\tt -o} {\em file}]
Generate the OCaml code in file {\em file}, instead of the default file,
when the {\tt -gen} option is given.
\end{description}

\end{document}