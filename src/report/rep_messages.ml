(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

let software = "Report"
let software_author = "Maxence Guesdon"
let software_author_mail = "Maxence.Guesdon@inria.fr"
let software_copyright =
  "Copyright 2004-2010 Institut National de Recherche en \n"^
  "Informatique et en Automatique. All rights reserved.\n"^
  "This software is distributed under the terms of the\n"^
  "GNU General Public License version2.\n"^
  "(see file LICENSE in the distribution)"

let software_about =
  software^" version "^Rep_installation.software_version^"\n\n"^
  software_author^"\n"^
  software_author_mail^"\n\n"^
  software_copyright



(** {2 Command line options and messages} *)

let usage =
  let e = Sys.argv.(0) in
  e^" [options] <file>\n"^
  "Examples:\n"^
  e^" -ibin -o foo bar    translate the binary file bar to XML file foo\n"^
  e^" -obin -o foo bar    translate the XML file bar to binary file foo\n"^
  e^" -gen bar            generate code from file bar to bar.ml \n"^
  e^" -gen -ibin bar      generate code from binary file bar to bar.ml \n"^
  e^" -gen -o foo.ml bar  generate code from file bar to foo.ml\n"

let usage_gui = (Filename.basename Sys.argv.(0))^" [options] [files]\n"
let options_are = "Options are :"

let opt_gen = " generate code to (chop_extension <file>).ml"
let opt_out = " <file>  Output code to <file> or translate the given file into <file>"
let opt_bin_in = " binary input"
let opt_bin_out = " binary output"

(** {2 Messages and labels} *)

let error = "Error"
let fun_unit = "fun () -> "
let tag = "Tag : "
let ocaml_id = "OCaml id : "
let name = "Name : "
let generated_by = "File generated by "^software^" version "^Rep_installation.software_version
let parameters = "Parameters"
let add_parameter = "Add parameter"
let code = "Code : fun () -> "
let add = "Add"
let edit = "Edit"
let remove = "Remove"
let header = "Header"
let attributes = "Attributes"
let about = "About"
let open_file = "Open file"

let insert_leaf = "Insert leaf"
let insert_tag = "Insert tag"
let insert_list = "Insert list"
let insert_cond = "Insert conditionnal"
let insert_sub = "Insert subreport"
let insert_mark = "Insert mark"
let edit_selected = "Edit selected"
let edit_params = "Edit parameters"
let edit_header = "Edit header"

let bad_format s =
  "Unexpected xml: "^s
