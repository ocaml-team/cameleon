(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Types for report description. *)

type report_ele =
  | Leaf of leaf
  | Tag of tag
  | List of liste
  | Cond of cond
  | Sub of sub
  | Mark of mark
  | Then of cond
  | Else of cond

and leaf = {
    mutable leaf : string ;
  }

and tag = {
    mutable tag : string ;
    mutable atts : (string * string) list ;
    mutable tag_subs : report_ele list ;
  }

and sub = {
    mutable sub_code : string ;
  }

and liste = {
    mutable var : string ;
    mutable f : string ;
    mutable list_subs : report_ele list ;
  }

and cond = {
    mutable cond : string ;
    mutable subs_then : report_ele list ;
    mutable subs_else : report_ele list ;
  }

and mark = {
    mutable mark_id : string ; (** the caml id for the mark *)
    mutable mark_name : string ; (** the mark name *)
  }

type report = {
    mutable rep_header : string ;
    mutable rep_params : string list;
    mutable rep_eles : report_ele list ;
  }
