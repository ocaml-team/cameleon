(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

type 'a report_ele = 'a Rep_types.report_ele =
  | Leaf of (unit -> string)
  | Tag of 'a tag
  | List of 'a liste
  | Cond of 'a cond
  | Sub of 'a sub

and 'a tag = 'a Rep_types.tag = {
    mutable tag : string ;
    mutable atts : (string * (unit -> string)) list ;
    mutable tag_subs : 'a report_ele list
  } 
    
and 'a liste = 'a Rep_types.liste =
    { mutable list_subs : ('a -> 'a report_ele list) ;
      mutable f : (unit -> 'a list) ;
    }

and 'a cond = 'a Rep_types.cond = 
    { mutable cond : unit -> bool ;
      mutable subs_then : 'a report_ele list ;
      mutable subs_else : 'a report_ele list ;
    } 

and 'a sub = 'a Rep_types.sub = 
    {
      mutable sub_rep : unit -> 'a report ;
    } 

and 'a report = 'a Rep_types.report =
    {
      mutable rep_eles : 'a report_ele list ;
    } 

let coerce = Rep_gen.coerce

let compute ?(html=false) fmt report =
  Rep_gen.print_fmt ~html: html fmt report

let compute_file ?(html=false) file report = 
  Rep_gen.print_file ~html: html file report

(*

(**********************************)

let liste = ref []
let _ = for i = 0 to 100 do liste := i :: !liste done

let (report : int report_ele)= 
  Tag { tag = "html" ; atts = [] ; tag_subs =
	[Tag { tag = "body" ; atts = [("bgcolor", "white")] ; tag_subs =
	       [
		 Tag { tag = "table" ; atts = [("border", "1")] ; tag_subs =
		       let rec n = { f = (fun () -> !liste) ; current = 0 ; sub_subs =
				     [
				       Tag { tag = "tr" ; atts = [] ; tag_subs = 
					     [
					       Tag { tag = "td" ; atts = [] ; tag_subs =
						     [
						       Leaf (fun () -> string_of_int n.current)
						     ] 
						   } 
					     ] 
					   } 
				     ]
				   }
		       in
		       [ Sub (mag n) ]
		     } 
	       ] 
	     } 
	]
      }	 

let _ = print Format.std_formatter report
*)
