(**************************************************************************)
(*                   Cameleon                                             *)
(*                                                                        *)
(*      Copyright (C) 2002 Institut National de Recherche en Informatique et   *)
(*      en Automatique. All rights reserved.                              *)
(*                                                                        *)
(*      This program is free software; you can redistribute it and/or modify  *)
(*      it under the terms of the GNU General Public License as published by  *)
(*      the Free Software Foundation; either version 2 of the License, or  *)
(*      any later version.                                                *)
(*                                                                        *)
(*      This program is distributed in the hope that it will be useful,   *)
(*      but WITHOUT ANY WARRANTY; without even the implied warranty of    *)
(*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the     *)
(*      GNU General Public License for more details.                      *)
(*                                                                        *)
(*      You should have received a copy of the GNU General Public License  *)
(*      along with this program; if not, write to the Free Software       *)
(*      Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA          *)
(*      02111-1307  USA                                                   *)
(*                                                                        *)
(*      Contact: Maxence.Guesdon@inria.fr                                *)
(**************************************************************************)

(** Translation of Report 0.4 files to new format. *)

module M03 =
  struct
    type report_ele =
      | Leaf of leaf
      | Tag of tag
      | List of liste
      | Cond of cond
      | Sub of sub
      | Mark of mark
      | Then of cond
      | Else of cond

    and leaf = {
	mutable leaf : string ;
      }

    and tag = {
	mutable tag : string ;
	mutable atts : (string * string) list ;
	mutable tag_subs : report_ele list ;
      }

    and sub = {
	mutable sub_code : string ;
      }

    and liste = {
	mutable var : string ;
	mutable f : string ;
	mutable list_subs : report_ele list ;
      }

    and cond = {
	mutable cond : string ;
	mutable subs_then : report_ele list ;
	mutable subs_else : report_ele list ;
      }

    and mark = {
	mutable mark_id : string ; (** the caml id for the mark *)
	mutable mark_name : string ; (** the mark name *)
      }

    type report = {
	mutable rep_params : string list;
	mutable rep_eles : report_ele list ;
      }

 end

module M04 =
  struct
    type report = {
	mutable rep_header : string ;
	mutable rep_params : string list;
	mutable rep_eles : M03.report_ele list ;
      }

 end

let usage = "usage: "^Sys.argv.(0)^" <old_file> <new_file>"

let files = ref []

let args = []

let parse_args () =
  try
    Arg.parse args
      (fun s ->
        match !files with
          [] -> files := [s]
	| [f1] -> files := [f1 ; s]
        | _ -> raise (Failure usage)
      )
      (usage^"Options are:")
  with
    Failure s ->
      prerr_endline s;
      exit 1

let main () =
  parse_args ();
  try
    match !files with
    | [in_file ; out_file] ->
	(
	 let chanin = open_in in_file in
         let tree = IoXML.parse_xml (Stream.of_channel chanin) in
	 close_in chanin;
         let report = Obj.magic (M04.xparse_report tree) in
	 let fmt = Format.formatter_of_out_channel stdout in
	 Rep_io.store_report out_file report
	)
    | _ ->
	assert false
   with
     Sys_error s ->
       prerr_endline s;
       exit 1

    | IoXML.ExcLoc ((bp,ep), e) ->
	prerr_endline ("Error at location "^(string_of_int bp)^" -> "^
		       (string_of_int ep));
	exit 2

let _ = main ()
