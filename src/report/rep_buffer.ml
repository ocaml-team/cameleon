(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open Rep_desc

let buffer = ref (None : report_ele option)

let rec copy_report_ele ele =
  match ele with
    Leaf l -> Leaf { leaf = l.leaf }
  | Tag t -> Tag { t with tag_subs = List.map copy_report_ele t.tag_subs }
  | List l -> List { l with list_subs = List.map copy_report_ele l.list_subs }
  | Cond c -> Cond { c with
		     subs_then = List.map copy_report_ele c.subs_then ;
		     subs_else = List.map copy_report_ele c.subs_else ;
		   }
  | Sub s -> Sub { sub_code = s.sub_code  }
  | Mark m -> Mark { mark_name = m.mark_name ;
		     mark_id = m.mark_id }
  | Then _ | Else _ ->
      (* should not occur *)
      ele

let copy ele = buffer := Some (copy_report_ele ele)

let paste () =
  match !buffer with
    None -> None
  | Some e -> Some (copy_report_ele e)
