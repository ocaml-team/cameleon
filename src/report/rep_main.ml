(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Main module of the generator. *)

let main () =
  Rep_args.parse ();
  match !Rep_args.file with
    None ->
      prerr_endline Rep_messages.usage ;
      exit 1
  | Some file ->
      let report =
	if !Rep_args.binary_input then
	  Rep_misc.value_of_file file
	else
	  Rep_io.load_report file
      in
      if !Rep_args.generate_code then
	(
	 let ml_file =
	   match !Rep_args.output_file with
	     None ->
	       (try Filename.chop_extension file
		 with Invalid_argument _ -> file)^".ml"
	   | Some f -> f
	 in
	 Rep_gen_code.gen_code ml_file report
	)
      else
        (* if the output_file is given, then we must
           save to this file. *)
	match !Rep_args.output_file with
          None ->
	    failwith "No output file given to save."
	| Some f ->
	    if !Rep_args.binary_output then
	      Rep_misc.file_of_value f report
	    else
	      Rep_io.store_report f report

let () = Rep_misc.safe_main main
