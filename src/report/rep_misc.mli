(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Misc functions. *)

(*i==v=[Misc.safe_main]=1.0====*)
(** [safe_main f] calls [f ()] but handles [Sys_error] and [Failure]
   exceptions by exiting with error code 1.
@author Maxence Guesdon
@version 1.0
@cgname Misc.safe_main*)
val safe_main : (unit -> unit) -> unit
(*/i==v=[Misc.safe_main]=1.0====*)

(*i==v=[File.value_of_file]=1.0====*)
(** [value_of_file filename] reads a value from [filename] using
   [input_value].
@author Fabrice Lefessant
@version 1.0
@raise Sys_error if the file could not be opened.
@cgname File.value_of_file*)
val value_of_file : string -> 'b
(*/i==v=[File.value_of_file]=1.0====*)

(*i==v=[File.file_of_value]=1.0====*)
(** [file_of_value filename v] writes a value [v] to [filename] using
   [output_value].
@author Fabrice Lefessant
@version 1.0
@raise Sys_error if the file could not be opened.
@cgname File.file_of_value*)
val file_of_value : string -> 'c -> unit

(*/i==v=[File.file_of_value]=1.0====*)
(*i==v=[String.chop_n_char]=1.0====*)
(** [chop_n_char n s] returns the given string where characters after position n
   are replaced by ["..."].
@version 1.0
@cgname String.chop_n_char*)
val chop_n_char : int -> string -> string
(*/i==v=[String.chop_n_char]=1.0====*)
