(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Analysis of command line arguments. *)

(** The report file. *)
let file = ref None

(** Flag for code generation. *)
let generate_code = ref false

(** Output code file. *)
let output_file = ref None

(** Binary input instead of XML. *)
let binary_output = ref false

(** Binary output instead of XML. *)
let binary_input = ref false

let options  = ref [
  "-gen", Arg.Set generate_code, Rep_messages.opt_gen ;
  "-ibin", Arg.Set binary_input, Rep_messages.opt_bin_in ;
  "-obin", Arg.Set binary_output, Rep_messages.opt_bin_out ;
  "-o", Arg.String (fun s -> output_file := Some s), Rep_messages.opt_out ;
 ]

(** The report files to open in gui. *)
let gui_files = ref []

let options_gui  = ref [
 ]

(** Parse the command line and fill the arguments variables. *)
let parse () =
  try
    let _ = Arg.parse !options
	(fun s ->
	  match !file with
	    None -> file := Some s;
	  | Some f -> raise (Failure Rep_messages.usage)
	)
	(Rep_messages.usage^Rep_messages.options_are)
    in
    ()
  with
    Failure s ->
      prerr_endline s ;
      exit 1

(** Parse the command line and fill the arguments variables for the GUI. *)
let parse_gui () =
  try
    let _ = Arg.parse !options_gui
	(fun s -> gui_files := !gui_files @ [s])
	(Rep_messages.usage_gui^Rep_messages.options_are)
    in
    ()
  with
    Failure s ->
      prerr_endline s ;
      exit 1
