(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** The structure used to describe a report for its generation. *)

(** A report element. *)
type 'a report_ele =
  | Leaf of (unit -> string)
  | Tag of 'a tag
  | List of 'a liste
  | Cond of 'a cond
  | Sub of 'a sub

    (** A tag. *)
and 'a tag = {
    mutable tag : string ;
    mutable atts : (string * (unit -> string)) list ;
    mutable tag_subs : 'a report_ele list
  } 
      
    (** A list of substructures. *)
and 'a liste =
    { mutable list_subs : ('a -> 'a report_ele list) ;
      mutable f : (unit -> 'a list) ;
    }

    (** Conditional *)
and 'a cond =
    { mutable cond : unit -> bool ;
      mutable subs_then : 'a report_ele list ;
      mutable subs_else : 'a report_ele list ;
    } 
  
(** Subreport *)
and 'a sub = 
    { 
      mutable sub_rep : unit -> 'a report ;
    }
    
    (** A report description is a list of report elements. *)
and 'a report = {
    mutable rep_eles : 'a report_ele list ;
  } 



