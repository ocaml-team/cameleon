(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(* $Id: dep_view.ml 758 2011-01-13 07:53:27Z zoggy $ *)

(** Caml modules dependencies view.*)

module V = Cam_plug.View
open Odoc_info

module C = Cam_plug.Commands

let default_node_color = "skyblue";;
let node_shape = "ellipse";;
let top_shape = "rectangle";;

let p_dbg s = ()
(*
let p_dbg = prerr_endline
*)


(*c==v=[Misc.md5sum_of_string]=1.0====*)
let md5sum_of_string s =
  let com = Printf.sprintf "echo %s | md5sum | cut -d\" \" -f 1"
      (Filename.quote s)
  in
  let ic = Unix.open_process_in com in
  let s = input_line ic in
  close_in ic;
  ignore (Unix.wait ());
  s
(*/c==v=[Misc.md5sum_of_string]=1.0====*)

class box view_name ?root_module dir =
  let hash = md5sum_of_string
      (Printf.sprintf "%s%s%s" Cam_plug.login dir
	 (match root_module with None -> "" | Some s -> s)
      )
  in
  let prefix = Printf.sprintf "/tmp/%s" hash in
  object(self)
    inherit Odot_view.box ~tmp_hash: prefix ()
    val mutable modules = []

    method modules_to_handle =
      let mods =
        match (Unix.lstat dir).Unix.st_kind with
          Unix.S_DIR -> Cam_plug.Modules_view.get_modules_from_dir dir
        | _ -> Odoc_info.load_modules dir
      in
      let mods =
	match root_module with
	  None -> mods
	| Some name ->
	    let rec iter acc = function
		[] -> acc
              | m_name :: q ->
		  if List.exists (fun m -> m.Module.m_name = m_name) acc then
                    iter acc q
		  else
                    let m_opt =
                      try Some (List.find (fun m -> m.Module.m_name = m_name) mods)
                      with Not_found -> None
                    in
                    match m_opt with
                      None -> iter acc q
                    | Some m -> iter (m::acc) (m.Module.m_top_deps @ q)
            in
            try iter [] [String.capitalize name]
            with Not_found ->
              Cam_hooks.error_message ("Could not find module "^name);
	      []
      in
      Odoc_info.Dep.kernel_deps_of_modules mods;
      let pred =
	let l = List.map (fun m -> m.Module.m_name) mods in
	fun s -> List.mem s l
      in
      List.iter
	(fun m ->
	  m.Module.m_top_deps <- List.filter pred m.Module.m_top_deps
	)
	mods;
      mods

    method refresh_data =
      modules <- self#modules_to_handle;
      Cam_hooks.display_message (Printf.sprintf "%s#resfresh_data, %d modules" view_name (List.length modules))

    method build_graph =
      let dir_colors = Hashtbl.create 13 in
      let avail_colors = ref !Odoc_info.Args.dot_colors in
      let get_color m =
        let dir = Filename.dirname m.Module.m_file in
        try Hashtbl.find dir_colors  dir
        with Not_found ->
            let c =
              match !avail_colors with
                [] -> default_node_color
              | c :: q -> avail_colors := q; c
            in
            Hashtbl.add dir_colors dir c;
            c
      in
      let mk_attr_list =
	List.map
	  (fun (id1, id2_opt) ->
	    (Odot.Simple_id id1,
	     match id2_opt with
	       None -> None
	     | Some id2 -> Some (Odot.Double_quoted_id id2)))
      in
      let atts =
	List.map
	  (fun (id1, id2) ->
	    Odot.Stmt_equals (Odot.Simple_id id1,Odot.Double_quoted_id id2))
	  [
	    "ratio", "fill" ;
	    "fontsize", "10pt" ;
	    "rankdir", "TB" ;
	  ]
      in
      let t = Hashtbl.create 13 in
      List.iter
	(fun m ->
	  List.iter (fun m -> Hashtbl.replace t m true) m.Module.m_top_deps
	)
	modules;
      let f_node m =
	let shape =
	  try ignore (Hashtbl.find t m.Module.m_name); node_shape
	  with Not_found -> top_shape
	in
	Odot.Stmt_node
	  (Odot.node_id (Odot.Simple_id m.Module.m_name),
	   mk_attr_list
	     [ "shape", Some shape;
	       "style", Some "filled" ;
	       "color", Some (get_color m) ;
	     ]
	  )
      in
      let nodes = List.map f_node modules in

      let links =
	List.flatten
	  (List.map
	     (fun src ->
	       List.map
		 (fun dest -> Odot.Stmt_edge
		     (Odot.Edge_node_id (Odot.node_id (Odot.Double_quoted_id src.Module.m_name)),
		      [Odot.Edge_node_id (Odot.node_id (Odot.Double_quoted_id dest))],
		      [])
		 )
		 src.Module.m_top_deps
	     )
	     modules
	  )
      in
      let stmt_list = atts @ nodes @ links in
      { Odot.id = None ;
	Odot.kind = Odot.Digraph ;
	Odot.strict = false ;
	Odot.stmt_list = stmt_list ;
      }

    method on_button1_press ~x ~y = function
	None -> ()
      |	Some id ->
	  try
	    let m = List.find (fun m -> m.Module.m_name = id) modules in
	    let files =
	      (match m.Module.m_loc.Odoc_info.loc_inter with
		None -> []
	      | Some (f,_) -> [f]
	      ) @
	      (match m.Module.m_loc.Odoc_info.loc_impl with
		None -> []
	      | Some (f,_) -> [f]
	      )
	    in
	    let entries =
	      (`I ("view only this module's dependencies",
		   let res = Printf.sprintf "%s#%s" dir m.Module.m_name in
		   fun () -> ignore(Cam_view.open_ressource res view_name [| |]))
	      ) ::
	      (
	       List.map
		 (fun f ->
		   `M (Cam_misc.escape_menu_label (Filename.basename f),
		       Cam_files.edition_commands_menu_entries f)
		 )
		 files
	      )
	    in
	    GToolbox.popup_menu ~entries ~button: 1 ~time: Int32.zero
	  with
	    Not_found ->
	      Cam_hooks.error_message (Printf.sprintf "%s: %s not found" view_name id)

    initializer
      ()

  end

class view
    (name : V.view_name)
    (dir : V.ressource_name)
    (box : box)
    (close_window_on_close : bool) =
  object (self)
    method changed = false
    method close : bool = close_window_on_close
    method name = name
    method refresh = box#refresh ()
    method ressource = dir
    method ressource_kind : V.ressource_kind = `Dir
  end

class factory : V.view_factory =
  object (self)
    method private dir_and_root_module_of_string s =
      try
	let len = String.length s in
	let p = String.rindex s '#' in
	let dir = String.sub s 0 p in
	let root_module =
	  if len > p + 1 then
	    Some (String.sub s (p+1) (len-p-1))
	  else
	    None
	in
	(dir,root_module)
      with
	Not_found ->
	  (s, None)

    method create res_name args =
      let (dir,root_module) = self#dir_and_root_module_of_string res_name in
      let box = new box self#name ?root_module dir in
      let v = new view (self#name) res_name box true in
      let w = V.create_view_window
          ~title: (Printf.sprintf "%s [%s]" res_name self#name)
          v
      in
      let _ = w#vbox#pack ~expand: true box#box#coerce in
      (v, w#window)

    method create_no_window window res_name args =
      let (dir,root_module) = self#dir_and_root_module_of_string res_name in
      let box = new box self#name ?root_module dir in
      let v = new view (self#name) res_name box false in
      (v, box#box#coerce)

    method known_ressource_kinds = [`Dir ; `File]
    method name = "dependencies"
  end

let _ = V.register_factory (new factory)
