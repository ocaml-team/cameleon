(*********************************************************************************)
(*                Cameleon                                                       *)
(*                                                                               *)
(*    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 2 of the         *)
(*    License, or any later version.                                             *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(* $Id: dep_view.ml 588 2007-09-04 11:55:55Z zoggy $ *)

(** Caml types dependencies view.*)

module V = Cam_plug.View
open Odoc_info

module C = Cam_plug.Commands

let default_node_color = "skyblue";;

let p_dbg s = ()
(*
let p_dbg = prerr_endline
*)

let file_of_type t =
  match t.Type.ty_loc.loc_inter with
    Some (f,_) -> f
  | None -> match t.Type.ty_loc.loc_impl with
        Some (f,_) -> f
      | None -> ""
;;

let md5sum_of_string = Dep_view.md5sum_of_string;;

class box view_name ?root_type dir =
  let hash = md5sum_of_string
      (Printf.sprintf "%s%s%s%s" Cam_plug.login view_name dir
	 (match root_type with None -> "" | Some s -> s)
      )
  in
  let prefix = Printf.sprintf "/tmp/%s" hash in
  object(self)
    inherit Odot_view.box ~tmp_hash: prefix ()
    val mutable types = []

    method types_to_handle =
      let mods =
        match (Unix.lstat dir).Unix.st_kind with
          Unix.S_DIR -> Cam_plug.Modules_view.get_modules_from_dir dir
        | _ -> Odoc_info.load_modules dir
      in
      let types = Odoc_info.Search.types mods in
      let deps = Odoc_info.Dep.deps_of_types types in
      let types =
        match root_type with
          None ->  deps
        | Some name ->
            let rec iter acc = function
              [] -> acc
            | t_name :: q ->
                if List.exists (fun (t,_) -> t.Type.ty_name = t_name) acc then
                  iter acc q
                else
                  let t_opt =
                    try Some (List.find (fun (t,_) -> t.Type.ty_name = t_name) deps)
                    with Not_found -> None
                  in
                  match t_opt with
                    None -> iter acc q
                  | Some (t,t_deps) ->
                      let l =
                        try snd(List.find (fun (t2,_) -> t2.Type.ty_name = t.Type.ty_name) deps)
                        with Not_found -> []
                      in
                      iter ((t,t_deps)::acc) (l @ q)
            in
            try iter [] [String.capitalize name]
            with Not_found ->
                Cam_hooks.error_message ("Could not find type "^name);
                []
      in
      types

    method refresh_data =
      types <- self#types_to_handle

    method build_graph =
      let dir_colors = Hashtbl.create 13 in
      let avail_colors = ref !Odoc_info.Args.dot_colors in
      let get_color t =
        let dir = Filename.dirname (file_of_type t) in
        try Hashtbl.find dir_colors  dir
        with Not_found ->
            let c =
              match !avail_colors with
                [] -> default_node_color
              | c :: q -> avail_colors := q; c
            in
            Hashtbl.add dir_colors dir c;
            c
      in
      let mk_attr_list =
	List.map
	  (fun (id1, id2_opt) ->
	    (Odot.Simple_id id1,
	     match id2_opt with
	       None -> None
	     | Some id2 -> Some (Odot.Double_quoted_id id2)))
      in
      let atts =
	List.map
	  (fun (id1, id2) ->
	    Odot.Stmt_equals (Odot.Simple_id id1,Odot.Double_quoted_id id2))
	  [
	    "ratio", "fill" ;
	    "fontsize", "10pt" ;
	    "rankdir", "TB" ;
	  ]
      in
      let f_node (t,_) =
        let shape = match t.Type.ty_kind with
            Type.Type_abstract when t.Type.ty_manifest = None -> "diamond"
          | Type.Type_abstract -> "house"
          | Type.Type_variant _ -> "ellipse"
          | Type.Type_record _ -> "rectangle"
        in
        Odot.Stmt_node
          (Odot.node_id (Odot.Double_quoted_id t.Type.ty_name),
           mk_attr_list
             [ "shape", Some shape;
               "style", Some "filled" ;
               "color", Some (get_color t) ;
             ]
          )
      in
      let nodes = List.map f_node types in

      let links =
        List.flatten
          (List.map
           (fun (src, dests) ->
              List.map
                (fun dest -> Odot.Stmt_edge
                   (Odot.Edge_node_id (Odot.node_id (Odot.Double_quoted_id src.Type.ty_name)),
                    [Odot.Edge_node_id (Odot.node_id (Odot.Double_quoted_id dest))],
                    [])
                )
                dests
           )
             types
          )
      in
      let stmt_list = atts @ nodes @ links in
      { Odot.id = None ;
	Odot.kind = Odot.Digraph ;
	Odot.strict = false ;
	Odot.stmt_list = stmt_list ;
      }

    method on_button1_press ~x ~y = function
      None -> ()
    | Some id ->
        (* id is double quoted; let's remove the quotes first *)
        let id =
          let len = String.length id in
          String.sub id 1 (len - 2)
        in
        try
          let (t,_) = List.find (fun (t,_) -> t.Type.ty_name = id) types in
          let files =
            (match t.Type.ty_loc.Odoc_info.loc_inter with
               None -> []
             | Some (f,c) -> [f,c]
            ) @
              (match t.Type.ty_loc.Odoc_info.loc_impl with
                 None -> []
               | Some (f,c) -> [f,c]
              )
          in
          let entries =
            (`I ("view only this type's dependencies",
              let res = Printf.sprintf "%s#%s" dir t.Type.ty_name in
              fun () -> ignore(Cam_view.open_ressource res view_name [| |]))
            ) ::
              (
               List.map
                 (fun (f,c) ->
                    `M
                      (Cam_misc.escape_menu_label (Filename.basename f),
                       Cam_files.edition_commands_menu_entries
                         ~line: (Cam_misc.line_of_char f c)
                         f)
                 )
                 files
              )
          in
          GToolbox.popup_menu ~entries ~button: 1 ~time: Int32.zero
        with
          Not_found ->
            Cam_hooks.error_message
              (Printf.sprintf "id \"%s\" not found in %s" id
              (String.concat ", " (List.map (fun (t,_) -> t.Type.ty_name) types))
              )

    initializer
      ()
  end

class view
    (name : V.view_name)
    (dir : V.ressource_name)
    (box : box)
    (close_window_on_close : bool) =
  object (self)
    method changed = false
    method close : bool = close_window_on_close
    method name = name
    method refresh = box#refresh ()
    method ressource = dir
    method ressource_kind : V.ressource_kind = `Dir
  end

class factory : V.view_factory =
  object (self)
    method private dir_and_root_type_of_string s =
      try
	let len = String.length s in
	let p = String.rindex s '#' in
	let dir = String.sub s 0 p in
	let root_type =
	  if len > p + 1 then
	    Some (String.sub s (p+1) (len-p-1))
	  else
	    None
	in
	(dir,root_type)
      with
	Not_found ->
	  (s, None)

    method create res_name args =
      let (dir,root_type) = self#dir_and_root_type_of_string res_name in
      let box = new box self#name ?root_type dir in
      let v = new view (self#name) res_name box true in
      let w = V.create_view_window
          ~title: (Printf.sprintf "%s [%s]" res_name self#name)
          v
      in
      let _ = w#vbox#pack ~expand: true box#box#coerce in
      (v, w#window)

    method create_no_window window res_name args =
      let (dir,root_type) = self#dir_and_root_type_of_string res_name in
      let box = new box self#name ?root_type dir in
      let v = new view (self#name) res_name box false in
      (v, box#box#coerce)

    method known_ressource_kinds = [`Dir ; `File]
    method name = "type_dependencies"
  end

let _ = V.register_factory (new factory)
