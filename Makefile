#################################################################################
#                Cameleon                                                       #
#                                                                               #
#    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     #
#    et en Automatique. All rights reserved.                                    #
#                                                                               #
#    This program is free software; you can redistribute it and/or modify       #
#    it under the terms of the GNU Library General Public License as            #
#    published by the Free Software Foundation; either version 2 of the         #
#    License, or any later version.                                             #
#                                                                               #
#    This program is distributed in the hope that it will be useful,            #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of             #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              #
#    GNU Library General Public License for more details.                       #
#                                                                               #
#    You should have received a copy of the GNU Library General Public          #
#    License along with this program; if not, write to the Free Software        #
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   #
#    02111-1307  USA                                                            #
#                                                                               #
#    Contact: Maxence.Guesdon@inria.fr                                          #
#                                                                               #
#################################################################################

include master.Makefile

SCRIPTS=

IMAGES=\
	images/bookmark.png \
	images/chamo.png \
	images/component.png \
	images/file_component.png \
	images/fun.png \
	images/run.png \
	images/tdl-group.png \
	images/tdl-item.png \
	images/tdl-item-done.png \
	images/tdl-item-high.png \
	images/tdl-item-low.png \
	images/tdl-item-suspended.png \
	images/topcameleon.xpm \
	images/type.png \
	images/value.png

# Compilation
#############

work: utils src

all: work srcdoc doc

src: dummy
	cd src && $(MAKE) all

utils: dummy
#	cd utils && $(MAKE) all

re : depend clean all

perl:
	$(ROOT)/utils/compile.sh

help:
	@echo "work:      utils src"
	@echo "all:       work srcdoc doc"
	@echo "src:       src/all"
	@echo "utils:     utils/all"
	@echo "re:        depend clean all"

# Documentation :
#################
srcdoc: dummy
	cd src && $(MAKE) doc

doc: dummy
	cd doc && $(MAKE) all

# myself

master.Makefile: master.Makefile.in config.status
	./config.status

config.status: configure master.Makefile.in \
	src/cameleon/cam_installation.ml.in \
	src/cameleon/cam_config.ml.in \
	src/custop/custop_installation.ml.in \
	src/report/rep_installation.ml.in \
	src/dbforge/dbf_installation.ml.in
	./config.status --recheck

configure: configure.in
	autoconf

# headers :
###########
HEADFILES= configure.in configure \
	master.Makefile.in Makefile \
	src/*/*.ml src/*/*.mli src/*/*.mll src/*/*.mly src/*/*.in \
	src/Makefile doc/Makefile utils/Makefile utils/checkocaml.ml
headers: dummy
	echo $(HEADFILES)
	headache -h header -c .headache_config `ls $(HEADFILES) | grep -v odot | grep -v cam-mktop2.in`

noheaders: dummy
	headache -r -c .headache_config `ls $(HEADFILES) | grep -v odot | grep -v cam-mktop.in`

# backup, clean and depend :
############################

distclean: clean
	cd src && $(MAKE) distclean
	cd doc && $(MAKE) distclean
	$(RM) config.cache config.log config.status master.Makefile
	$(RM) config_check.log ocaml_config.sh
	$(RM) images/topcameleon.xpm
#	cd utils && $(MAKE) distclean

clean: dummy
	$(RM) *~ \#*\#
	cd src && $(MAKE) clean
	cd doc && $(MAKE) clean
#	cd utils && $(MAKE) clean


depend: dummy
	cd src && $(MAKE) depend
alldepend: dummy
	cd src && $(MAKE) alldepend

dummy:

#################
# code count
#################
codecount:
	@echo `cat src/*/*.ml src/*/*.mli | wc -l` lines


#################
# installation
#################

install: dummy installimages
	cd src && $(MAKE) install
	$(MAKE) installspecsfiles
	$(MAKE) installdoc

installspecsfiles:
	$(MKDIR) $(LANGUAGESSPECSDIR)
	$(CP) utils/ocaml.lang $(LANGUAGESSPECSDIR)

installdoc:
	cd doc && $(MAKE) install

%.gif: %.png
	convert $< $@

%.xpm: %.png
	convert -scale 32x32 $< $@

installimages: dummy $(IMAGES)
	$(MKDIR) $(PIXMAPSDIR)
	$(CP) $(IMAGES) $(PIXMAPSDIR)

installscripts: dummy
#	$(MKDIR) $(DIR_UTILS)
#	$(CP) $(SCRIPTS) $(DIR_UTILS)

###########################
# additional dependencies
###########################

# DO NOT DELETE
