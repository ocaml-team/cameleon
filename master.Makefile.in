#################################################################################
#                Cameleon                                                       #
#                                                                               #
#    Copyright (C) 2004-2011 Institut National de Recherche en Informatique     #
#    et en Automatique. All rights reserved.                                    #
#                                                                               #
#    This program is free software; you can redistribute it and/or modify       #
#    it under the terms of the GNU Library General Public License as            #
#    published by the Free Software Foundation; either version 2 of the         #
#    License, or any later version.                                             #
#                                                                               #
#    This program is distributed in the hope that it will be useful,            #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of             #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              #
#    GNU Library General Public License for more details.                       #
#                                                                               #
#    You should have received a copy of the GNU Library General Public          #
#    License along with this program; if not, write to the Free Software        #
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   #
#    02111-1307  USA                                                            #
#                                                                               #
#    Contact: Maxence.Guesdon@inria.fr                                          #
#                                                                               #
#################################################################################

# Various commands and dir
##########################
OCAMLBIN= @OCAMLBIN@
OCAMLC   = @OCAMLC@
OCAMLOPT =@OCAMLOPT@
OCAMLDEP = @OCAMLDEP@
OCAMLLEX = @OCAMLLEX@
OCAMLYACC= @OCAMLYACC@
OCAMLLIB = @OCAMLLIB@
OCAMLMKTOP = @OCAMLMKTOP@
OCAMLMKLIB = @OCAMLMKLIB@
CAMMKTOP = @CAMMKTOP@
OCAMLDOCDIR= $(OCAMLLIB)/ocamldoc
OCAMLDOC_PLUGINSDIR=$(DESTDIR)@OCAMLDOC_PLUGINSDIR@
OCAMLVERSION = @OCAMLVERSION@
OCAMLFIND= @OCAMLFIND@
EXTRAC_CRC = $(OCAMLLIB)/extract_crc
OCAMLDOC= @OCAMLDOC@
OCAMLDOCOPT= @OCAMLDOC_OPT@
OCAMLPP=
LATEX=TEXINPUTS=$(ROOT)/doc: latex
HEVEA=hevea -fix -francais -I $(ROOT)/doc -exec xxdate.exe
HACHA=hacha
DVIPS=dvips
DVIPDF=dvipdf
BIBTEX=bibtex
HEVEA_CSS_STYLE=@LOC_WWW@@LOC_DOC@style.css
OCAMLCFLAGS= -warn-error FSPUYZ -thread
OCAMLBYTECFLAGS=-annot

OCAML_DRIVER_INCLUDES=@OCAML_DRIVER_INCLUDES@
GTK_INCLUDES=@GTK_INCLUDES@
LABLGLADECC=@LABLGLADECC@
LABLGTK2_INCLUDES=@LABLGTK2_INCLUDES@
XMLLIGHT_INCLUDES=@XMLLIGHT_INCLUDES@
PCRE_INCLUDES=@PCRE_INCLUDES@
LABLGTKEXTRAS_INCLUDES=@LABLGTKEXTRAS_INCLUDES@

# For installation
##############
MKDIR=mkdir -p
CP=cp -f
CPDIR=$(CP) -r
RM=rm -fR
MV=mv

DESTDIR=   # For debian packagers

LIBDIR=$(DESTDIR)@libdir@
BINDIR=$(DESTDIR)@bindir@
MANDIR=$(DESTDIR)@mandir@
TEMPLATESDIR=$(DESTDIR)@TEMPLATESDIR@
PLUGINSDIR=$(DESTDIR)@PLUGINSDIR@
PIXMAPSDIR=$(DESTDIR)@PIXMAPSDIR@
GLADEDIR=$(DESTDIR)@GLADEDIR@
DTDDIR=$(DESTDIR)@DTDDIR@
LANGUAGESSPECSDIR=$(DESTDIR)@LANGUAGESSPECSDIR@

CAMELEON=@CAMELEON@
CAMELEON_BYTE=@CAMELEON_BYTE@

CAM_CONFIG=@CAM_CONFIG@
CAM_CONFIG_BYTE=@CAM_CONFIG_BYTE@

DOCBROWSER=@DOCBROWSER@
DOCBROWSER_BYTE=@DOCBROWSER_BYTE@

EDITOR=@EDITOR@
EDITOR_BYTE=@EDITOR_BYTE@

TDL=@TDL@
TDL_BYTE=@TDL_BYTE@
TDL_EXPORT=@TDL_EXPORT@
TDL_EXPORT_BYTE=@TDL_EXPORT_BYTE@
TDL_HTML=@TDL_HTML@
TDL_HTML_BYTE=@TDL_HTML_BYTE@
TDL_SVN=@TDL_SVN@
TDL_SVN_BYTE=@TDL_SVN_BYTE@

RSS=@RSS@
RSS_BYTE=@RSS_BYTE@
RSS_EXPORT=@RSS_EXPORT@
RSS_EXPORT_BYTE=@RSS_EXPORT_BYTE@

REPORT=@REPORT@
REPORT_BYTE=@REPORT_BYTE@
REPORT_GUI=@REPORT_GUI@
REPORT_GUI_BYTE=@REPORT_GUI_BYTE@

DBFORGE=@DBFORGE@
DBFORGE_BYTE=@DBFORGE_BYTE@
DBFORGE_GUI=@DBFORGE_GUI@
DBFORGE_GUI_BYTE=@DBFORGE_GUI_BYTE@

TMPL_ENGINE=@TMPL_ENGINE@
TMPL_ENGINE_BYTE=@TMPL_ENGINE_BYTE@

OCAMLCVS=@OCAMLCVS@
OCAMLCVS_BYTE=@OCAMLCVS_BYTE@

CAMTOP=@CAMTOP@

#shut up autconf @datarootdir@

# Compilation
#############

ROOT=@ROOT@

# generic rules :
#################
.SUFFIXES: .mli .ml .cmi .cmo .cmx .mll .mly \
	.html .tex .dvi .ps .pdf

%.cmi:%.mli
	$(OCAMLC) $(OCAMLBYTECFLAGS) $(OCAMLCFLAGS) $(OCAMLPP) $(COMPFLAGS) -c $<

%.cmo:%.ml
	if test -f `dirname $<`/`basename $< .ml`.mli && test ! -f `dirname $<`/`basename $< .ml`.cmi ; then \
	$(OCAMLC) $(OCAMLBYTECFLAGS) $(OCAMLCFLAGS) $(OCAMLPP) $(COMPFLAGS) -c `dirname $<`/`basename $< .ml`.mli; fi
	$(OCAMLC) $(OCAMLBYTECFLAGS) $(OCAMLCFLAGS) $(OCAMLPP) $(COMPFLAGS) -c $<

%.cmi %.cmo:%.ml
	if test -f `dirname $<`/`basename $< .ml`.mli && test ! -f `dirname $<`/`basename $< .ml`.cmi ; then \
	$(OCAMLC) $(OCAMLBYTECFLAGS) $(OCAMLCFLAGS) $(OCAMLPP) $(COMPFLAGS) -c `dirname $<`/`basename $< .ml`.mli; fi
	$(OCAMLC) $(OCAMLBYTECFLAGS) $(OCAMLCFLAGS) $(OCAMLPP) $(COMPFLAGS) -c $<

%.cmx %.o:%.ml
	$(OCAMLOPT) $(OCAMLCFLAGS) $(OCAMLPP) $(COMPFLAGS) -c $<

%.o: %.c
	$(OCAMLOPT) $(OCAMLCFLAGS) $(COMPFLAGS) -c $< && $(MV) `basename $@` `dirname $@`

%.ml:%.mll
	$(OCAMLLEX) $<

%.mli %.ml:%.mly
	$(OCAMLYACC) -v $<

%.dvi: %.tex
	$(LATEX) $<
	$(BIBTEX) `basename $< .tex`
	$(LATEX) $<
	$(LATEX) $<

%.ps: %.dvi
	$(DVIPS) -o $@ $<

%.pdf: %.dvi
	$(DVIPDF) $< $@

%.html: %.tex
	$(HEVEA) $<
	$(HACHA) $@
	rpl '<META name="GENERATOR" content="hevea 1.07">' \
	'<link href="$(HEVEA_CSS_STYLE)" rel="stylesheet" type="text/css"> <META name="GENERATOR" content="hevea 1.07">' \
	*.html

# some common targets :
#######################
first:all
cleandoc:
	$(RM) *.ps *.dvi *.html *.pdf *.log *.aux *.toc *.bbl *.blg *.htoc *.haux *~
