#! /bin/zsh

while true; do
  bad_files=$(make ${1:-work} 2>&1 | utils/filter.pl; return $pipestatus[0])
  r=$?
  if [ "$bad_files" = "" ]; then exit $r; fi
  echo $bad_files
  f1=$(echo "$bad_files" | awk '{print $1}')
  f2=$(echo "$bad_files" | awk '{print $2}')
  (cd src; rm -f "$f1" "$f2")
done
