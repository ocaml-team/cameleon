#! /usr/bin/perl -w

use strict;

my ($f1, $f2) = (undef, undef);
my @bad_files;

while (<>) {
    chomp; print STDERR "$_\n";
    if (/^(?:The f|F)iles (\S+) and (\S+)(.*)$/) {
        $f1 = $1; $f2 = $2;
    } elsif (defined $f1 && defined $f2) {
        if (/^(.*)over (?:implementation|interface) (\S+)$/) {
            push (@bad_files, { f1 => $f1, f2 => $f2, interface => $1 });
        }
        $f1 = $f2 = undef;
    } else {
        $f1 = $f2 = undef;
    }
}

foreach my $v (@bad_files) {
    printf "%s %s [%s]\n", $v->{f1}, $v->{f2}, $v->{interface}
}

exit 0;
